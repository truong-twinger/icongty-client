import { HTMLAttributes, PropsWithChildren } from 'react';

import Footer from './Footer';
import Head, { THead } from './Head';
import Header from './Header';
import HeaderWithSearch from './HeaderWithSearch';

export interface ILayoutProps {
  withSearch?: boolean;
}
type PageProps = PropsWithChildren<THead> & HTMLAttributes<HTMLDivElement> & ILayoutProps;
function Layout(props: PageProps) {
  const { children, className, withSearch = false } = props;
  return (
    <>
      <Head {...props} />
      {withSearch ? <HeaderWithSearch /> : <Header />}
      {/* <Header /> */}
      <main className='site' id='page'>
        <div className={`tw-main ${className || ''}`}>{children}</div>
      </main>
      <Footer />
    </>
  );
}

export default Layout;
