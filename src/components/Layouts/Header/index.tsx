import { CloseOutlined } from '@ant-design/icons';
import { Col, Row, Space, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import { urlHeader } from '@/configs/const.config';
import { ELanguage } from '@/configs/interface.config';
import { handleChangeLanguage } from '@/libs/const';

import styles from './style.module.less';

const { Text } = Typography;

function Header() {
  const router = useRouter();
  const { t } = useTranslation();
  const pathname = `/${router.pathname.split('/')[1]}`;

  // const { mutate: signOut } = useMutationSignOut();

  // const [token, setToken] = useState<string>('');
  // useEffect(() => {
  //   const accessTokenCurrent = checkAuth();
  //   setToken(accessTokenCurrent);
  //   window.addEventListener('storage', () => {
  //     const accessToken = checkAuth();
  //     setToken(accessToken);
  //   });
  // }, []);

  const [y, setY] = useState(0);
  const [isScrollDown, setIsScrollDown] = useState(false);
  const [isShowDraw, setIsShowDraw] = useState(false);
  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleScroll = (event: any) => {
      const window = event.currentTarget;
      if (y > 100) {
        if (y > window.scrollY) {
          // Scrolling up
          setIsScrollDown(false);
        } else if (y < window.scrollY) {
          // Scrolling down
          setIsScrollDown(true);
        }
      }
      setY(window.scrollY);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [y]);
  return (
    <header>
      <Col
        className={`${styles.overlay} ${isShowDraw && styles.open}`}
        onClick={() => {
          setIsShowDraw(false);
        }}
      />
      <Row
        className={`${styles.wrapper}  ${isScrollDown && pathname === '/' && styles.hide} ${
          (isScrollDown || y > 400) && styles.scrollDown
        }  ${pathname !== '/' && styles.notHome} ${isShowDraw && styles.open}`}
      >
        <Col lg={{ span: 16 }} span={24}>
          <Row gutter={[62, 0]} className={styles.logoRow}>
            <Col>
              <Link href='/'>
                <Image className={styles.logo} src='/images/logo.svg' alt='logo iCONGTY' width={187} height={48} />
                <Image
                  className={styles.logoHover}
                  src='/images/logo3.svg'
                  alt='logo iCONGTY'
                  width={187}
                  height={48}
                />
              </Link>
            </Col>
            <Col
              className={styles.close}
              onClick={() => {
                setIsShowDraw(false);
              }}
            >
              <CloseOutlined className={styles.icon} />
            </Col>

            <Col className={styles.category}>
              <Row gutter={[32, 38]} className={styles.wrap}>
                <Col>
                  <Link
                    className={`${styles.link} ${pathname === urlHeader.findCompany && styles.active}`}
                    href={urlHeader.findCompany}
                  >
                    {t('find_company')}
                  </Link>
                </Col>
                <Col>
                  <Link
                    className={`${styles.link} ${pathname === urlHeader.news && styles.active}`}
                    href={urlHeader.news}
                  >
                    {t('news')}
                  </Link>
                </Col>
                <Col>
                  <Link
                    className={`${styles.link} ${pathname === urlHeader.contact && styles.active}`}
                    href={urlHeader.contact}
                  >
                    {t('contact')}
                  </Link>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col lg={{ span: 8 }} span={24} className={styles.action}>
          <Row gutter={[16, 0]} className={styles.wrap}>
            {/* {token ? (
              <Col>
                <Button
                  onClick={() => {
                    signOut();
                  }}
                  size='small'
                  className={styles.btnRegister}
                  type='link'
                >
                  {t('sign_out')}
                </Button>
              </Col>
            ) : (
              <>
                <Col>
                  <Button
                    onClick={() => {
                      router.push({ pathname: urlHeader.signIn });
                    }}
                    size='small'
                    className={styles.btnSignIn}
                    ghost
                    type='primary'
                  >
                    {t('sign_in')}
                  </Button>
                </Col>
                <Col>
                  <Button
                    onClick={() => {
                      router.push({ pathname: urlHeader.signUp });
                    }}
                    size='small'
                    className={styles.btnRegister}
                    type='primary'
                  >
                    <Space size={8}>
                      <IconRegister color='#fff' />
                      <Text className={styles.text} strong>
                        {t('sign_up')}
                      </Text>
                    </Space>
                  </Button>
                </Col>
              </>
            )} */}

            <Col onClick={() => handleChangeLanguage(router)} className={styles.language}>
              <Space size={10}>
                {router?.locale === ELanguage.VI ? (
                  <Image src='/images/flagEn.svg' width={22.5} height={17} alt='en' />
                ) : (
                  <Image src='/images/flagVI.svg' width={22.5} height={17} alt='en' />
                )}

                <Text className={styles.text} strong>
                  {router?.locale === ELanguage.VI ? 'EN' : 'VI'}
                </Text>
              </Space>
            </Col>
          </Row>
        </Col>
      </Row>
      <div className='home-container'>
        <Row
          className={`${styles.headerMobile}  ${isScrollDown && pathname === '/' && styles.hide} ${
            pathname !== '/' && styles.notHome
          } ${(isScrollDown || y > 100) && styles.scrollDown} home-wrapper`}
        >
          <Col>
            <Link href='/'>
              <Image
                className={styles.logoHover}
                src='/images/logoMobile.svg'
                alt='logo iCONGTY'
                width={48}
                height={48}
              />
            </Link>
          </Col>
          <Col onClick={() => setIsShowDraw(true)}>
            <Image className={styles.menuIcon} src='/images/menu.svg' alt='menu' width={32} height={32} />
          </Col>
        </Row>
      </div>
    </header>
  );
}

export default Header;
