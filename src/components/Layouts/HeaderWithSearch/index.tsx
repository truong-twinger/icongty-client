/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
import { CloseOutlined } from '@ant-design/icons';
import { AutoComplete, Col, Row, Space, Typography } from 'antd';
import { debounce } from 'lodash';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';

import SearchInput from '@/components/Elements/Search';
import Label from '@/components/Screens/Home/components/Label';
import { baseParams, urlHeader } from '@/configs/const.config';
import { ELanguage, EOrder, EOrderBy, IOption } from '@/configs/interface.config';
import { handleChangeLanguage } from '@/libs/const';
import { TCompany } from '@/modules';
import { queryGetSuggestCompany } from '@/queries/hooks';

import styles from './style.module.less';

const { Text } = Typography;

function HeaderWithSearch() {
  const router = useRouter();
  const pathname = `/${router.pathname.split('/')[1]}`;
  const { t } = useTranslation();
  const [isShowDraw, setIsShowDraw] = useState(false);

  // Auth
  // const { mutate: signOut } = useMutationSignOut();
  // const [token, setToken] = useState<string>('');
  // useEffect(() => {
  //   const accessTokenCurrent = checkAuth();
  //   setToken(accessTokenCurrent);
  //   window.addEventListener('storage', () => {
  //     const accessToken = checkAuth();
  //     setToken(accessToken);
  //   });
  // }, []);

  // Suggest
  const [suggest, setSuggest] = useState('');
  const {
    data: suggestData,
    fetchNextPage: fetchNextPageSuggest,
    hasNextPage: hasNextPageSuggest,
  } = queryGetSuggestCompany({
    ...baseParams,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    s: suggest,
    lang: router.locale as ELanguage,
  });
  const optionsSuggest = useMemo(() => {
    const result: IOption[] = [];
    suggestData?.pages?.forEach((page) =>
      page?.data?.forEach((company: TCompany) =>
        result.push({
          value: company.slug,
          label: Label({ label: company.name, logo: company?.logo?.location }) as unknown as string,
        }),
      ),
    );
    return result;
  }, [suggestData]);
  const onPopupScrollSuggest = (e: any) => {
    if (e.target.scrollHeight === e.target.scrollTop + e.target.offsetHeight) {
      if (hasNextPageSuggest) {
        fetchNextPageSuggest();
      }
    }
  };
  const onSelect = (value: string) => {
    router.push(`/company/${value}`);
  };
  const onSearch = (value: string) => {
    router.push({ pathname: '/search', query: { s: value } });
  };
  return (
    <header>
      <Col
        className={`${styles.overlay} ${isShowDraw && styles.open}`}
        onClick={() => {
          setIsShowDraw(false);
        }}
      />
      <Row className={`${styles.wrapper}  ${isShowDraw && styles.open}`}>
        <Col order={2} lg={{ order: 1 }} span={24} className={styles.category}>
          <Row className={styles.wrap}>
            <Col>
              <Row gutter={[32, 38]}>
                <Col className={styles.linkWrap}>
                  <Link
                    className={`${styles.link} ${pathname === urlHeader.findCompany && styles.active}`}
                    href={urlHeader.findCompany}
                  >
                    {t('find_company')}
                  </Link>
                </Col>
                <Col className={styles.linkWrap}>
                  <Link
                    className={`${styles.link} ${pathname === urlHeader.news && styles.active}`}
                    href={urlHeader.news}
                  >
                    {t('news')}
                  </Link>
                </Col>
                <Col className={styles.linkWrap}>
                  <Link
                    className={`${styles.link} ${pathname === urlHeader.contact && styles.active}`}
                    href={urlHeader.contact}
                  >
                    {t('contact')}
                  </Link>
                </Col>
              </Row>
            </Col>
            <Col onClick={() => handleChangeLanguage(router)} className={styles.language}>
              <Space size={10}>
                {router?.locale === ELanguage.VI ? (
                  <Image src='/images/flagEn.svg' width={22.5} height={17} alt='en' />
                ) : (
                  <Image src='/images/flagVI.svg' width={22.5} height={17} alt='en' />
                )}

                <Text className={styles.text} strong>
                  {router?.locale === ELanguage.VI ? 'EN' : 'VI'}
                </Text>
              </Space>
            </Col>
          </Row>
        </Col>
        <Col order={1} lg={{ order: 2 }} className={styles.search} span={24}>
          <Row className={styles.searchRow}>
            <Col>
              <Link href='/'>
                <Image className={styles.logo} src='/images/logo3.svg' alt='logo iCONGTY' width={187} height={48} />
              </Link>
            </Col>
            <Col
              className={styles.close}
              onClick={() => {
                setIsShowDraw(false);
              }}
            >
              <CloseOutlined className={styles.icon} />
            </Col>
            <Col className={styles.searchCol}>
              <AutoComplete
                className={`${styles.autoComplete} auto-complete-header`}
                popupClassName='auto-complete-header'
                options={optionsSuggest}
                onSelect={onSelect}
                onPopupScroll={onPopupScrollSuggest}
              >
                <SearchInput
                  onSearch={onSearch}
                  onChange={debounce(
                    (event: React.ChangeEvent<HTMLInputElement>) => setSuggest(event.target.value),
                    500,
                  )}
                />
              </AutoComplete>
            </Col>
            <Col className={styles.action}>
              <Row gutter={[16, 0]} className={styles.wrap}>
                {/* {token ? (
                  <Col>
                    <Button
                      onClick={() => {
                        signOut();
                      }}
                      size='small'
                      className={styles.btnRegister}
                      type='link'
                    >
                      <strong>{t('sign_out')}</strong>
                    </Button>
                  </Col>
                ) : (
                  <>
                    <Col>
                      <Button
                        onClick={() => {
                          router.push({ pathname: urlHeader.signIn });
                        }}
                        size='small'
                        className={styles.btnSignIn}
                        ghost
                        type='primary'
                      >
                        <strong>{t('sign_in')}</strong>
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        onClick={() => {
                          router.push({ pathname: urlHeader.signUp });
                        }}
                        size='small'
                        className={styles.btnRegister}
                        type='primary'
                      >
                        <Space size={8}>
                          <IconRegister color='#fff' />
                          <Text className={styles.text} strong>
                            {t('sign_up')}
                          </Text>
                        </Space>
                      </Button>
                    </Col>
                  </>
                )} */}
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
      <div className='home-container'>
        <Row className={`${styles.headerMobile} home-wrapper`}>
          <Col>
            <Link href='/'>
              <Image
                className={styles.logoHover}
                src='/images/logoMobile.svg'
                alt='logo iCONGTY'
                width={48}
                height={48}
              />
            </Link>
          </Col>
          <Col onClick={() => setIsShowDraw(true)}>
            <Image className={styles.menuIcon} src='/images/menu.svg' alt='menu' width={32} height={32} />
          </Col>
        </Row>
      </div>
    </header>
  );
}

export default HeaderWithSearch;
