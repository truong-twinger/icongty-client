import { Button, Col, Form, Input, Row, Typography } from 'antd';
import Link from 'next/link';
import { useState } from 'react';

import { useForgotPassword } from '@/queries/hooks';

import styles from './style.module.less';

const { Title } = Typography;

export default function ForgotPasswordScreen() {
  const { mutate: forgot } = useForgotPassword();
  const [step, setStep] = useState(1);

  const onFinish = (values: { email: string }) => {
    forgot(values.email, {
      onSuccess: () => {
        setStep(2);
      },
    });
  };
  return (
    <Form onFinish={onFinish} layout='vertical' className={styles.wrapper}>
      {step === 1 && (
        <Row gutter={[0, 24]} className={styles.formGroup}>
          <Col span={24}>
            <Title level={1}>Khôi phục mật khẩu</Title>
          </Col>
          <Col span={24}>
            <Form.Item
              label='Email khôi phục'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập email',
                },
              ]}
              name='email'
            >
              <Input placeholder='Nhập email khôi phục' />
            </Form.Item>
            <Form.Item>
              <Button type='primary' htmlType='submit'>
                Gửi
              </Button>
            </Form.Item>
          </Col>
        </Row>
      )}

      {step === 2 && (
        <Row className={styles.formGroup}>
          <Col span={24}>
            Một email đã được gửi đi. Vui lòng kiểm tra hòm thư của bạn.
            <Link href='/'>Về trang chủ</Link>
          </Col>
        </Row>
      )}
    </Form>
  );
}
