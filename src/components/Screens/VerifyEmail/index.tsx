import { Button, Col, Row, Typography } from 'antd';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

import { urlHeader } from '@/configs/const.config';
import { useConfirmEmail } from '@/queries/hooks';

import styles from './style.module.less';

const { Title } = Typography;

export default function VerifyEmail() {
  const router = useRouter();

  const { mutate: mutateVerifyEmail, data } = useConfirmEmail();

  useEffect(() => {
    if (router.query.token) {
      mutateVerifyEmail({ token: router.query.token.toString() });
    }
  }, [router]);
  return (
    <Row className={styles.wrapper}>
      <Col span={24}>
        <Row>
          <Col className={styles.contentWrap} span={24} md={{ span: 12, offset: 2 }}>
            <Row className={styles.contentRow}>
              <Title level={1} className={styles.title}>
                XÁC THỰC EMAIL THÀNH CÔNG
              </Title>
              <Col span={24} lg={{ span: 20 }}>
                {`Email ${data?.email} đã được xác thực.`}
              </Col>
              <Col span={24}>
                <Button onClick={() => router.push({ pathname: urlHeader.signIn })} className={styles.regiterBtn}>
                  Đăng nhập
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
