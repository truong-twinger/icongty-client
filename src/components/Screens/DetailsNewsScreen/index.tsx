/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  ClockCircleFilled,
  FacebookOutlined,
  LinkedinOutlined,
  LinkOutlined,
  RiseOutlined,
  // eslint-disable-next-line prettier/prettier
  TwitterOutlined,
} from '@ant-design/icons';
import { Col, Divider, message, Row, Tag, Tooltip, Typography } from 'antd';
import moment from 'moment-timezone';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import { FacebookShareButton, LinkedinShareButton, TwitterShareButton } from 'react-share';

import AllTag from '@/components/Elements/AllTag';
import BreadcrumbPage from '@/components/Elements/Breadcrumb';
import RelatedPosts from '@/components/Elements/RelatedPosts';
import { BASE_URL } from '@/configs/env.config';
import { ELanguage } from '@/configs/interface.config';
import { getBlurDataUrl } from '@/libs/common';
import { EADS_KEY, TAdvertising, TCategory, TPost } from '@/modules';
import { useQueryAdvertising } from '@/queries/hooks/advertising';
import { useWindowSize } from '@hooks/index';

import 'moment/locale/vi';
import styles from './style.module.less';

const { Text, Title } = Typography;

function DetailNewsScreen({ dataDetail, ListNewsByTax, listAllNewTopView }: any) {
  const { t } = useTranslation('news');
  const router = useRouter();
  moment.locale(router.locale);

  const { data: adsData, isLoading: isLoadingAdsById, isFetching: isFetchingAdsById } = useQueryAdvertising();

  const ads = useMemo(() => adsData?.data.values, [adsData, isLoadingAdsById, isFetchingAdsById]);

  const adsTop = ads?.filter((value: TAdvertising) => value.key === EADS_KEY.NEWS_DETAILS);
  const adsBottom = ads?.filter((value: TAdvertising) => value.key === EADS_KEY.HOME_SIDEBAR);

  const link = `${BASE_URL}${router.asPath.slice(1)}`;
  const handleCopy = () => {
    if (router.asPath) {
      navigator.clipboard
        .writeText(link)
        .then(() => {
          message.info(link);
        })
        .catch(() => {
          message.error('Error!!');
        });
    } else {
      message.error('Error!!');
    }
  };

  const [width] = useWindowSize();
  const srcAdsTop = useMemo(() => {
    if (width < 576) {
      return adsTop?.[0]?.mb?.location;
    }
    if (width < 768) {
      return adsTop?.[0]?.tablet?.location;
    }
    return adsTop?.[0]?.pc?.location;
  }, [width, adsData]);

  const srcAdsBot = useMemo(() => {
    if (width < 576) {
      return adsTop?.[0]?.mb?.location;
    }
    if (width < 768) {
      return adsTop?.[0]?.tablet?.location;
    }
    return adsTop?.[0]?.pc?.location;
  }, [width, adsData]);

  return (
    <div className='home-container'>
      <div className='home-wrapper'>
        <Row className={styles.wrapper} gutter={[0, { xs: 32, sm: 32, md: 56 }]}>
          <Col span={24} className={styles.breadcrumb}>
            <BreadcrumbPage detailName={dataDetail?.name} router={router} />
          </Col>
          <Col span={24}>
            <Row gutter={[{ xs: 0, sm: 0, md: 78 }, 0]}>
              <Col span={24} lg={{ span: 16 }}>
                {/* Heading */}
                <Row className={styles.content}>
                  <Col className={styles.heading} span={24}>
                    <Title level={1}>{dataDetail?.name}</Title>
                  </Col>
                  <Col xs={{ span: 24 }} sm={{ span: 13 }}>
                    {dataDetail?.categories?.map(
                      (category: TCategory, index: number) =>
                        index < 1 && (
                          <Tag color='blue' key={category._id}>
                            {category?.translation?.[router.locale as ELanguage]?.name}
                          </Tag>
                        ),
                    )}
                    {dataDetail?.categories?.length > 1 && (
                      <Tooltip
                        trigger={['hover', 'click']}
                        color='#fff'
                        placement='bottom'
                        title={<AllTag list={dataDetail.categories.slice(1, dataDetail.categories.length)} />}
                      >
                        <Tag color='blue'>+{(dataDetail?.categories?.length || 1) - 1}</Tag>
                      </Tooltip>
                    )}
                  </Col>
                  <Col xs={{ span: 24 }} sm={{ span: 11 }} className={styles.time}>
                    <ClockCircleFilled className={styles.icon} />
                    <Text className={styles.currentNewsTime} type='secondary'>
                      {moment(dataDetail?.createdAt)
                        .format('dddd , DD/MM/YYYY,HH:MM')
                        .replace(/(^|\s)(\w)/g, (match) => match.toUpperCase())}
                      {` (GTM  ${moment(dataDetail?.createdAt).tz('Asia/Ho_Chi_Minh').format('Z')})`}
                    </Text>
                  </Col>
                </Row>

                {/* Content */}
                <Row className={styles.wrappContent}>
                  <Col
                    span={24}
                    className={styles.content}
                    dangerouslySetInnerHTML={{ __html: dataDetail?.content || '' }}
                  />
                </Row>

                {/* Tag and Link */}
                <Row className={styles.tagWrap} gutter={[0, { xs: 30, sm: 30, md: 80, lg: 150 }]}>
                  <Col span={24}>
                    <Row>
                      <Col span={24} sm={{ span: 12 }} md={{ span: 14 }}>
                        <Row className={styles.listTag} gutter={[16, 0]} align='middle'>
                          <Col style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <FacebookShareButton
                              url={link}
                              hashtag='#Icongty'
                              className='Demo__some-network__share-button'
                            >
                              {' '}
                              <Tag className={styles.tag}>
                                <FacebookOutlined style={{ color: '#2F61E6' }} />
                              </Tag>
                            </FacebookShareButton>
                            <LinkedinShareButton title={dataDetail?.title} url={link}>
                              <Tag className={styles.tag}>
                                <LinkedinOutlined style={{ color: '#2F61E6' }} />
                              </Tag>
                            </LinkedinShareButton>
                            <TwitterShareButton title={dataDetail?.title} url={link} hashtags={['Icongty']}>
                              <Tag className={styles.tag}>
                                <TwitterOutlined style={{ color: '#2F61E6' }} />
                              </Tag>
                            </TwitterShareButton>

                            <Tag className={styles.tag} onClick={handleCopy}>
                              <LinkOutlined style={{ color: '#2F61E6' }} />
                            </Tag>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24} sm={{ span: 12 }} md={{ span: 10 }}>
                        <Row gutter={[16, 0]} className={styles._source} justify='end'>
                          {dataDetail?.source?.name && (
                            <>
                              <Col>
                                {t('By')} <strong>{dataDetail?.source?.name}</strong>
                              </Col>
                              <Col> | </Col>
                              <Col>
                                <Link
                                  target='_blank'
                                  href={dataDetail?.source?.url ? dataDetail?.source?.url : '/#'}
                                  passHref
                                >
                                  {t('Visit')}
                                  <RiseOutlined style={{ color: '#2F61E6' }} />
                                </Link>
                              </Col>
                            </>
                          )}
                        </Row>
                      </Col>
                    </Row>
                  </Col>

                  <Col span={24}>
                    <h2 className={styles._title}>{t('RelatedPosts')}</h2>

                    <RelatedPosts ListNewsByTax={ListNewsByTax} />
                  </Col>
                </Row>
              </Col>
              <Col span={24} lg={{ span: 8 }} className={styles.tabRight}>
                <Row gutter={[0, 64]} className={styles._wrappSticky}>
                  <Col span={24} className={styles._noStick}>
                    <Row gutter={[0, 32]}>
                      <Col span={24}>
                        <Title level={5} className={styles.title}>
                          {t('Most_Viewed')}
                          <RiseOutlined />
                        </Title>
                      </Col>

                      <Col span={24}>
                        {listAllNewTopView?.map((value: TPost, key: number) => (
                          <Row gutter={[24, 0]} key={key}>
                            <Col span={24}>
                              <Row>
                                <Col span={24}>
                                  <Link href={`/news/${value.slug}`} className={styles._blockNewsTopViews}>
                                    <Row gutter={[0, 24]} align='middle'>
                                      <Col span={4} className={styles._order}>
                                        {key + 1}
                                      </Col>
                                      <Col span={20}>
                                        <Text className={styles._text}>{value.name}</Text>
                                      </Col>
                                    </Row>
                                  </Link>
                                </Col>

                                <Col span={24}>
                                  <Divider className={styles._customDivider} />
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        ))}
                      </Col>
                    </Row>
                  </Col>
                  <Col span={24} className={styles._sticky}>
                    <Row gutter={[{ md: 12 }, 32]} className={styles.rightCol}>
                      <Col className={styles.bannerCol} md={{ span: 12 }} lg={{ span: 24 }} span={24}>
                        {/* Banner */}
                        <div className={styles.stickWrap}>
                          <Col span={24} className={`${styles.banner}`} lg={{ span: 24 }}>
                            {adsTop && (
                              <Link target='_blank' href={adsTop?.[0]?.guide || '/'} passHref>
                                <div className={`${styles.bannerWrap}`}>
                                  <Image
                                    placeholder='blur'
                                    blurDataURL={getBlurDataUrl(256, 341)}
                                    className='ads'
                                    src={srcAdsTop}
                                    fill
                                    alt='Ads'
                                  />
                                </div>
                              </Link>
                            )}
                          </Col>
                        </div>
                      </Col>

                      <Col className={styles.bannerCol} md={{ span: 12 }} lg={{ span: 24 }} span={24}>
                        {/* Banner */}
                        <div className={styles.stickWrap}>
                          <Col span={24} className={`${styles.banner}`} lg={{ span: 24 }}>
                            {adsBottom && (
                              <Link target='_blank' href={adsBottom?.[0]?.guide || '/'} passHref>
                                <div className={`${styles.bannerWrap}`}>
                                  <Image
                                    placeholder='blur'
                                    blurDataURL={getBlurDataUrl(256, 341)}
                                    className='ads'
                                    src={srcAdsBot}
                                    fill
                                    alt='Ads'
                                  />
                                </div>
                              </Link>
                            )}
                          </Col>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default DetailNewsScreen;
