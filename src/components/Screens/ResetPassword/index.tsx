import { Button, Col, Form, Input, Row, Typography } from 'antd';
import Link from 'next/link';
import { useState } from 'react';

import { urlHeader } from '@/configs/const.config';
import { TResetPassword } from '@/modules';
import { useResetPassword } from '@/queries/hooks';
import { regexPwdStrong } from '@/utils/regex';

import styles from './style.module.less';

const { Title } = Typography;

interface IProps {
  token: string;
}

export default function ResetPasswordScreen({ token }: IProps) {
  const { mutate: reset } = useResetPassword();
  const [step, setStep] = useState(1);

  const onFinish = (values: TResetPassword) => {
    reset(
      { ...values, token },
      {
        onSuccess: () => {
          setStep(2);
        },
      },
    );
  };
  return (
    <Form onFinish={onFinish} layout='vertical' className={styles.wrapper}>
      <Row gutter={[0, 24]} className={styles.formGroup}>
        {step === 1 && (
          <>
            <Col span={24}>
              <Title className={styles.title} level={1}>
                Đặt lại mật khẩu
              </Title>
            </Col>
            <Col span={24}>
              <Form.Item
                label='Mật khẩu'
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập Mật khẩu',
                  },
                  {
                    pattern: regexPwdStrong,
                    message:
                      'Mật khẩu bao gồm chữ hoa, chữ thường, số, kí tự đặc biệt và tối thiểu 8 kí tự, tối đa 15 kí tự',
                  },
                ]}
                name='newPassword'
              >
                <Input.Password placeholder='Mật khẩu' />
              </Form.Item>
              <Form.Item
                label='Nhập lại mật khẩu'
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng xác thực mật khẩu',
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('newPassword') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Mật khẩu không khớp'));
                    },
                  }),
                ]}
                name='confirmPassword'
              >
                <Input.Password placeholder='Mật khẩu' />
              </Form.Item>
              <Form.Item>
                <Button type='primary' htmlType='submit' block>
                  Xác nhận
                </Button>
              </Form.Item>
            </Col>
          </>
        )}
        {step === 2 && (
          <Col span={24}>
            Đặt lại mật khẩu thành công <Link href={urlHeader.signIn}>Đăng nhập</Link>
          </Col>
        )}
      </Row>
    </Form>
  );
}
