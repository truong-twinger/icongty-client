import { Button, Col, Form, Input, Row, Typography } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { urlHeader } from '@/configs/const.config';
import { TSignIn } from '@/modules';
import { useMutationSignIn } from '@/queries/hooks';
import { regexPwdStrong } from '@/utils/regex';

import styles from './style.module.less';

const { Title } = Typography;

export default function SignInScreen() {
  const { mutate: signIn } = useMutationSignIn();
  const router = useRouter();
  const onFinish = (values: TSignIn) => {
    signIn(values, {
      onSuccess: () => {
        router.push({ pathname: '/' });
      },
    });
  };
  return (
    <Form onFinish={onFinish} layout='vertical' className={styles.wrapper}>
      <Row gutter={[0, 24]} className={styles.formGroup}>
        <Col span={24}>
          <Title className={styles.title} level={1}>
            Đăng nhập
          </Title>
        </Col>
        <Col span={24}>
          <Form.Item
            label='Tên đăng nhập'
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập tên đăng nhập',
              },
            ]}
            name='username'
          >
            <Input placeholder='Tên đăng nhập' />
          </Form.Item>
          <Form.Item
            label='Mật khẩu'
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập Mật khẩu',
              },
              {
                pattern: regexPwdStrong,
                message:
                  'Mật khẩu bao gồm chữ hoa, chữ thường, số, kí tự đặc biệt và tối thiểu 8 kí tự, tối đa 15 kí tự',
              },
            ]}
            name='password'
          >
            <Input.Password placeholder='Mật khẩu' />
          </Form.Item>
          <Form.Item>
            <Button type='primary' htmlType='submit' block>
              Đăng nhập
            </Button>
          </Form.Item>
        </Col>
        <Col span={12}>
          Chưa có tài khoản? <Link href={urlHeader.signUp}>Đăng ký</Link>
        </Col>
        <Col span={12} style={{ textAlign: 'right' }}>
          <Link href={urlHeader.forgotPassword}>Quên mật khẩu</Link>
        </Col>
      </Row>
    </Form>
  );
}
