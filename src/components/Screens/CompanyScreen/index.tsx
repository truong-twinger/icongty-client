/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable function-paren-newline */
import { SearchOutlined } from '@ant-design/icons';
import { Button, Col, Input, Pagination, Row, Spin, Select, Space, Tag, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';

import BreadcrumbPage from '@/components/Elements/Breadcrumb';
import NotFound from '@/components/Elements/NotFound';
import TypicalCompany from '@/components/Elements/TypicalCompany';
import TypicalCompanyList from '@/components/Elements/TypicalCompanyList';
import { baseParams, formatNumber, ListOptionCopmanySize } from '@/configs/const.config';
import { ELanguage, IFilter, IOption, TResDataListApi } from '@/configs/interface.config';
import { handleFilter } from '@/libs/const';
import { TCategory, TCity, TOption } from '@/modules';
import { queryAllCompany } from '@/queries/hooks';
import { GridIcon, ListIcon } from '@/utils/icon';
import { GET_LIST_COMPANY } from '@/queries/keys';
import Error from '@/components/Elements/Error';

import styles from './style.module.less';

const { Title, Text } = Typography;

enum ETypeList {
  List = 'list',
  Grid = 'grid',
}

interface IProps {
  fetchListCity: TResDataListApi<TCity[]>;
  fetchListCategory: TResDataListApi<TCategory[]>;
  optionPage: TOption<any>[];
}

function CompanyScreen({ fetchListCategory, fetchListCity, optionPage }: IProps) {
  const { t } = useTranslation();
  const router = useRouter();

  // Category
  const options = useMemo(() => {
    const result: IOption[] = [];
    fetchListCategory.data.forEach((category) => result.push({ value: category._id, label: category.name }));
    return result;
  }, [fetchListCategory]);

  // Infinite City
  const optionsCity = useMemo(() => {
    const result: IOption[] = [];
    fetchListCity.data.forEach((city) => result.push({ value: city._id, label: city.name }));
    return result;
  }, [fetchListCity]);

  const [s, setS] = useState(router.query?.s?.toString());
  const [params, setParams] = useState<IFilter>();

  const [filter, setFilter] = useState<IFilter>({ ...router.query });
  const [isFilter, setIsFilter] = useState(Object.keys(filter).length !== 0);
  const [type, setType] = useState<ETypeList>(ETypeList.List);

  const handleClear = () => {
    if (Object.keys(router.query).length > 0) {
      router.replace(router.pathname, undefined, { shallow: true });
    }
    setFilter({});
    setParams({});
    setS('');
  };

  // Get Data
  const {
    data: listCompany,
    isLoading,
    isError,
  } = queryAllCompany(
    {
      ...baseParams,
      ...router.query,
      ...params,
      limit: 15,
      lang: router.locale as ELanguage,
    },
    GET_LIST_COMPANY,
  );

  const handleSearch = () => {
    let newFilter: IFilter;
    if (isFilter) {
      newFilter = { ...filter, s };
    } else {
      newFilter = { s };
    }

    if (!newFilter.companySizeTo) {
      delete newFilter.companySizeTo;
      delete router.query?.companySizeTo;
    }
    if (s === '') {
      delete newFilter.s;
      delete router.query?.s;
    }
    Object.keys(newFilter).forEach((key) => {
      if (!newFilter[key]) {
        delete newFilter[key];
        delete router.query[key];
      }
    });

    setParams({ ...router.query, ...newFilter });
  };

  return (
    <div className={styles.bg}>
      <div className='home-container'>
        <div className='home-wrapper'>
          <Row className={styles.wrapper}>
            <Col span={24} className='breadcrumb'>
              <BreadcrumbPage router={router} />
            </Col>
            <Col span={24} className={styles.banner}>
              <Image
                className={styles.bannerPc}
                src={optionPage[0]?.value?.location || '/images/category-company/bannerx4.jpg'}
                alt='banner'
                fill
              />
              <Image
                className={styles.bannerMobile}
                src={optionPage[0]?.value?.locationMb || '/images/category-company/bannermobilex4.jpg'}
                alt='banner'
                fill
              />
              <Col span={24} sm={{ span: 14 }} md={{ span: 13 }} lg={{ span: 15 }} className={styles.contentCol}>
                <Row className={styles.content}>
                  <Col span={24}>
                    <Title className={styles.title} level={1}>
                      {optionPage[0]?.value?.title || t('category-company')}
                    </Title>
                  </Col>
                  <Col span={24}>
                    <Text className={styles.subTitle}>
                      {optionPage[0]?.value?.description || t('company:description_carousel')}
                    </Text>
                  </Col>
                </Row>
              </Col>
            </Col>
            <Col span={24}>
              <Row gutter={[{ xs: 12, sm: 24 }, 0]}>
                <Col md={{ span: 19 }} span={15} sm={{ span: 16 }}>
                  <Input
                    allowClear
                    value={s}
                    onChange={(e) => {
                      setS(e.target.value);
                    }}
                    className={styles.input}
                    placeholder={t('home:placeholder_search') || 'Nhập tên công ty, mã số thuế, ngành nghề...'}
                  />
                </Col>
                <Col md={{ span: 5 }} span={9} sm={{ span: 8 }}>
                  <Button
                    disabled={Object.keys(filter).length === 0 && !s}
                    loading={isLoading}
                    icon={<SearchOutlined />}
                    onClick={handleSearch}
                    className={styles.btn}
                    type='primary'
                    block
                  >
                    <Text strong>{t('search')}</Text>
                  </Button>
                </Col>
              </Row>
            </Col>
            <Col span={24} className={styles.filterGroup}>
              <Row gutter={[24, 8]}>
                <Col span={24} sm={{ span: 19 }}>
                  <Row gutter={[24, 12]} className={`${styles.listFilter}`}>
                    <Col className={styles.searchAdvance}>
                      <Button
                        icon={
                          <Image src='/images/category-company/advancesearch.svg' alt='icon' width={14} height={14} />
                        }
                        size='small'
                        type='primary'
                        ghost
                        onClick={() => setIsFilter(!isFilter)}
                        className={`${styles.filter} ${isFilter && styles.active}`}
                      >
                        <Text strong>{t('home:search_advance')}</Text>
                      </Button>
                    </Col>
                    {isFilter && (
                      <Col className={styles.colFilter}>
                        <Row gutter={[24, 8]}>
                          <Col span={24} sm={{ span: 8 }}>
                            <Select
                              value={filter['categoryIds[]'] || undefined}
                              onChange={(value) => {
                                setFilter({ ...filter, 'categoryIds[]': value });
                              }}
                              showSearch
                              placeholder={t('home:career')}
                              className={`${styles.selectItem} home-multiple-select`}
                              showArrow
                              optionFilterProp='label'
                              options={options}
                            />
                          </Col>
                          <Col span={24} sm={{ span: 8 }}>
                            <Select
                              value={filter?.city || undefined}
                              onChange={(value: string) => {
                                setFilter({ ...filter, city: value });
                              }}
                              showSearch
                              placeholder={t('home:location')}
                              className={`${styles.selectItem} home-multiple-select`}
                              optionFilterProp='label'
                              options={optionsCity}
                            />
                          </Col>
                          <Col span={24} sm={{ span: 8 }}>
                            <Select
                              value={
                                filter?.companySizeTo
                                  ? `${filter?.companySizeFrom}-${filter?.companySizeTo}`
                                  : filter?.companySizeFrom?.toString() || undefined
                              }
                              onChange={(value: string) => {
                                const arr = value?.split('-');
                                if (arr) {
                                  setFilter({
                                    ...filter,
                                    companySizeFrom: Number(arr[0]),
                                    companySizeTo: Number(arr[1]),
                                  });
                                } else {
                                  setFilter({
                                    ...filter,
                                    companySizeFrom: undefined,
                                    companySizeTo: undefined,
                                  });
                                }
                              }}
                              placeholder={t('home:scale')}
                              className={`${styles.selectItem} home-select`}
                            >
                              {ListOptionCopmanySize.map((item, index) => (
                                <Select.Option key={index} value={item.value}>
                                  {item.label[router.locale as ELanguage]}
                                </Select.Option>
                              ))}
                            </Select>
                          </Col>
                        </Row>
                      </Col>
                    )}
                  </Row>
                </Col>
                <Col span={24} sm={{ span: 5 }} className={`${!isFilter && styles.hide} ${styles.clearCol}`}>
                  <Button onClick={handleClear} className={styles.clear} size='small' type='primary' ghost>
                    <Text strong>{t('company:clear')}</Text>
                  </Button>
                </Col>
              </Row>
            </Col>
            <Col span={24} className={`${styles.listCompany}`}>
              <Row gutter={[0, { xs: 12, sm: 24 }]} className={styles.titleGroup}>
                <Col sm={{ span: 12 }} md={{ span: 14 }} span={24}>
                  <Space size={16} align='center'>
                    <Title className={styles.title} level={3}>
                      {t('company:all_company')}
                    </Title>
                    {listCompany && listCompany?.total > 0 && (
                      <Tag color='blue' className={styles.tag}>
                        <strong>{formatNumber.format(listCompany && listCompany?.total)}</strong>
                      </Tag>
                    )}
                  </Space>
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 10 }} span={24} className={styles.action}>
                  <Row gutter={[24, 12]}>
                    <Col className={styles.sortGroup}>
                      <Space size={6}>
                        <Text className={styles.sortLabel}>{t('company:sort_by')}:</Text>
                        <Select
                          allowClear
                          value={params?.order === 'ASC' ? `-${params?.orderBy}` : `${params?.orderBy || 'createdAt'}`}
                          onChange={(value: string) => {
                            setParams({
                              ...params,
                              orderBy: handleFilter(value).orderBy,
                              order: handleFilter(value).order,
                            });
                          }}
                          defaultValue='1'
                          className={`${styles.sortSelect} home-select`}
                        >
                          <Select.Option value='createdAt'>{t('latest')}</Select.Option>
                          <Select.Option value='-createdAt'>{t('oldest')}</Select.Option>
                          <Select.Option value='-name'>A-Z</Select.Option>
                          <Select.Option value='name'>Z-A</Select.Option>
                          <Select.Option value='viewer'>{t('most_view')}</Select.Option>
                        </Select>
                      </Space>
                    </Col>
                    <Col className={styles.typeGroup}>
                      <Space size={8}>
                        <Text
                          onClick={() => setType(ETypeList.List)}
                          className={`${styles.icon} ${type === ETypeList.List && styles.active}`}
                        >
                          <ListIcon />
                        </Text>
                        <Text
                          onClick={() => setType(ETypeList.Grid)}
                          className={`${styles.icon} ${type === ETypeList.Grid && styles.active}`}
                        >
                          <GridIcon />
                        </Text>
                      </Space>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row gutter={[0, { lg: 48, md: 32, sm: 20, xs: 20 }]}>
                {isLoading ? (
                  <Spin className='empty' />
                ) : (
                  <>
                    {type === ETypeList.List && (
                      <Col span={24}>
                        <Row>
                          <Col span={24} className={styles.head}>
                            <Row gutter={[{ xs: 12, sm: 24 }, 0]}>
                              <Col span={24} sm={{ span: 16 }} md={{ span: 14 }} lg={{ span: 12 }}>
                                <strong>{t('company:business')}</strong>
                              </Col>
                              <Col span={0} lg={{ span: 4 }}>
                                <strong>{t('home:tax_code')}</strong>
                              </Col>
                              <Col span={0} md={{ span: 5 }} lg={{ span: 4 }}>
                                <strong>{t('home:career')}</strong>
                              </Col>
                              <Col
                                span={0}
                                sm={{ span: 8 }}
                                md={{ span: 5 }}
                                lg={{ span: 4 }}
                                className={styles.location}
                              >
                                <strong>{t('home:location')}</strong>
                              </Col>
                            </Row>
                          </Col>
                          <Col span={24}>
                            {isError ? (
                              <Error />
                            ) : (
                              <Row gutter={[0, { xs: 12, sm: 24 }]}>
                                {listCompany &&
                                  listCompany.data.map((company) => (
                                    <Col key={company._id} span={24}>
                                      <TypicalCompanyList data={company} />
                                    </Col>
                                  ))}
                              </Row>
                            )}
                          </Col>
                        </Row>
                      </Col>
                    )}
                    {type === ETypeList.Grid && (
                      <Col span={24}>
                        {isError ? (
                          <Error />
                        ) : (
                          <Row gutter={[26, 32]}>
                            {listCompany &&
                              listCompany.data.map((company) => (
                                <Col key={company._id} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                                  <TypicalCompany data={company} />
                                </Col>
                              ))}
                          </Row>
                        )}
                      </Col>
                    )}
                    {listCompany && listCompany.data.length === 0 && <NotFound />}
                  </>
                )}

                <Col span={24}>
                  <Pagination
                    responsive
                    className={styles.pagination}
                    size='small'
                    onChange={(page) => {
                      setParams({ ...params, page });
                    }}
                    current={params?.page || 1}
                    hideOnSinglePage
                    defaultCurrent={1}
                    total={Math.ceil(((listCompany?.total || 0) * 10) / 15)}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}

export default CompanyScreen;
