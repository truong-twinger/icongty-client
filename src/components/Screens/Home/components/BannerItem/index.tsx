import { Button, Col, Row, Typography } from 'antd';
import Image from 'next/image';
import { useRouter } from 'next/router';

import { urlHeader } from '@/configs/const.config';

import styles from './style.module.less';

const { Text, Title } = Typography;

interface Props {
  banner: string;
  bannerMobile: string;
  head: string;
  title: string;
  btn: string;
}

export default function BannerItem({ banner, bannerMobile, head, title, btn }: Props) {
  const router = useRouter();
  return (
    <div className={styles.wrapper}>
      <Image alt='carousel' src={banner} fill sizes='100vw' className={styles.banner} />
      <Image alt='carousel' src={bannerMobile} fill sizes='100vw' className={styles.bannerMobile} />
      <Row className={styles.content}>
        <Col lg={{ span: 8 }} md={{ span: 11 }} sm={{ span: 11 }} span={24}>
          <Col span={24}>
            <Text className={styles.head}>{head}</Text>
          </Col>
          <Col span={24}>
            <Row gutter={[0, 32]}>
              <Col span={24}>
                <Title level={3} className={`${styles.title} ellipsis twoline`}>
                  {title}
                </Title>
              </Col>

              <Col span={24}>
                <Button onClick={() => router.push(urlHeader.signUp)} type='link' className={styles.btn}>
                  {btn}
                </Button>
              </Col>
            </Row>
          </Col>
        </Col>
      </Row>
    </div>
  );
}
