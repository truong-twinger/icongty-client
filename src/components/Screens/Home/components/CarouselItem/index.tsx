import { Button, Col, Row, Typography } from 'antd';
import Image from 'next/image';

import styles from './style.module.less';

const { Text, Title } = Typography;

interface Props {
  alt?: string;
  altMobile?: string;
  banner: string;
  bannerMobile: string;
  title: string;
  subTitle: string;
  desc?: string;
  btn: string;
}

export default function CarouselItem({ banner, bannerMobile, subTitle, title, desc, btn, alt, altMobile }: Props) {
  return (
    <div className={styles.wrapper}>
      <Image alt={alt || 'carousel'} src={banner} fill sizes='90vw' className={styles.banner} />
      <Image
        alt={altMobile || 'carousel'}
        src={bannerMobile || '/images/home/carouselMobilex4.jpg'}
        fill
        sizes='90vw'
        className={styles.bannerMobile}
      />
      <Col span={24} className={styles.overlay} />
      <Row className={styles.content}>
        <Col lg={{ span: 8 }} md={{ span: 9 }} span={24}>
          <Col span={24}>
            <Text className={styles.head}>{title}</Text>
          </Col>
          <Col span={24}>
            <Row gutter={[0, 32]}>
              <Col span={24}>
                <Title level={3} className={`${styles.title} ellipsis twoline`}>
                  {subTitle}
                </Title>
              </Col>
              {desc && (
                <Col span={24}>
                  <Text className={`${styles.desc} ellipsis threeline`}>{desc}</Text>
                </Col>
              )}

              <Col span={24}>
                <Button type='link' className={styles.btn}>
                  {btn}
                </Button>
              </Col>
            </Row>
          </Col>
        </Col>
      </Row>
    </div>
  );
}
