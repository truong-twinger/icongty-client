import React from 'react';
import { Space, Typography } from 'antd';
import Image from 'next/image';

import styles from './style.module.less';

const { Text } = Typography;

interface IProps {
  label: string;
  logo: string | undefined;
}

export default function Label({ label, logo }: IProps) {
  return (
    <Space className={styles.wrapper} size={12}>
      {logo ? (
        <Image src={logo} alt={label} width={16} height={16} />
      ) : (
        <svg width='14' height='14' viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <path
            d='M1 4C0.867392 4 0.740215 4.05268 0.646447 4.14645C0.552678 4.24021 0.5 4.36739 0.5 4.5V12.5H3V4H1ZM2.5 11H1.5V10H2.5V11ZM2.5 9H1.5V8H2.5V9ZM2.5 7H1.5V6H2.5V7ZM13 4H11V12.5H13.5V4.5C13.5 4.36739 13.4473 4.24021 13.3536 4.14645C13.2598 4.05268 13.1326 4 13 4ZM12.5 11H11.5V10H12.5V11ZM12.5 9H11.5V8H12.5V9ZM12.5 7H11.5V6H12.5V7ZM10 0H4C3.86739 0 3.74021 0.0526784 3.64645 0.146447C3.55268 0.240215 3.5 0.367392 3.5 0.5V12.5H10.5V0.5C10.5 0.367392 10.4473 0.240215 10.3536 0.146447C10.2598 0.0526784 10.1326 0 10 0ZM6.5 11H5.5V10H6.5V11ZM6.5 9H5.5V8H6.5V9ZM6.5 7H5.5V6H6.5V7ZM6.5 5H5.5V4H6.5V5ZM6.5 3H5.5V2H6.5V3ZM8.5 11H7.5V10H8.5V11ZM8.5 9H7.5V8H8.5V9ZM8.5 7H7.5V6H8.5V7ZM8.5 5H7.5V4H8.5V5ZM8.5 3H7.5V2H8.5V3ZM14 14H0V13H14V14Z'
            fill='#FA8C16'
          />
        </svg>
      )}

      <Text className={styles.text}>{label}</Text>
    </Space>
  );
}
