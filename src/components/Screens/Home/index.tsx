/* eslint-disable prettier/prettier */
/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
import { AutoComplete, Button, Carousel, Col, Empty, Pagination, Row, Select, Space, Typography } from 'antd';
import { debounce } from 'lodash';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';

import CareerCategory from '@/components/Elements/CareerCategory';
import HotNews from '@/components/Elements/HomeHotNews';
import NewEnterprise from '@/components/Elements/NewEnterprise';
import Post from '@/components/Elements/Post';
import SearchInput from '@/components/Elements/Search';
import TypicalCompany from '@/components/Elements/TypicalCompany';
import SlideHotNews from '@/components/Widgets/SlideHotNews';
import { baseParams, ListOptionCopmanySize, urlHeader } from '@/configs/const.config';
import { ELanguage, EOrder, EOrderBy, IFilter, IOption, TResDataListApi } from '@/configs/interface.config';
import { TCategory, TCity, TCompany, TOption, TPost } from '@/modules';
import {
  queryAllCategory,
  queryAllCompany,
  queryGetSuggestCompany,
  useInfiniteAllCategory,
  useInfiniteAllCity,
} from '@/queries/hooks';
import { GET_LIST_COMPANY, GET_LIST_TYPICAL_COMPANY } from '@/queries/keys';
import Error from '@/components/Elements/Error';

import BannerItem from './components/BannerItem';
import CarouselItem from './components/CarouselItem';
import Label from './components/Label';
import styles from './style.module.less';

const { Title, Text } = Typography;

interface IProps {
  fetchNews: TResDataListApi<TPost[]>;
  fetchAllHotNews: TResDataListApi<TPost[]>;
  homeOption: TOption<any>[];
}

function HomeScreen({ fetchNews, fetchAllHotNews, homeOption }: IProps) {
  const { t } = useTranslation();
  const router = useRouter();

  // Get data
  const [pageTypical, setPageTypical] = useState(1);
  const { data: listTypicalCompany, isError: isErrorTypical } = queryAllCompany(
    {
      ...baseParams,
      limit: 6,
      page: pageTypical,
      lang: router.locale as ELanguage,
      typical: 1,
    },
    GET_LIST_TYPICAL_COMPANY,
  );

  const [pageCompany, setPageCompany] = useState(1);
  const { data: listCompany, isError: isErrorCompany } = queryAllCompany(
    {
      ...baseParams,
      page: pageCompany,
      limit: 9,
      lang: router.locale as ELanguage,
      typical: 0,
    },
    GET_LIST_COMPANY,
  );

  const [pageCategory, setPageCategory] = useState(1);
  const { data: listAllCategory } = queryAllCategory({
    ...baseParams,
    page: pageCategory,
    limit: 12,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    lang: router.locale as ELanguage,
  });

  // News
  const listNews = useMemo(() => fetchNews.data, [fetchNews]);
  const listHotNews = useMemo(() => fetchAllHotNews.data, [fetchAllHotNews]);
  const slideElementHotNews = useMemo(
    () => listHotNews.map((v, i) => <HotNews next={listHotNews[i + 1]} data={v} key={i} />),
    [fetchAllHotNews],
  );

  // Filter
  const [filter, setFilter] = useState<IFilter>({ ...router.query });
  const [isFilter, setIsFilter] = useState(true);

  // Infinite Category
  const [sCategory, setSCategory] = useState('');
  const {
    data: listCategory,
    fetchNextPage,
    isLoading,
    hasNextPage,
  } = useInfiniteAllCategory({
    ...baseParams,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    s: sCategory,
    lang: router.locale as ELanguage,
  });
  const options = useMemo(() => {
    const result: IOption[] = [];
    listCategory?.pages?.forEach((page) =>
      page?.data.forEach((category: TCategory) => result.push({ value: category._id, label: category.name })),
    );
    return result;
  }, [listCategory]);
  const onPopupScrollCategory = (e: any) => {
    if (e.target.scrollHeight === e.target.scrollTop + e.target.offsetHeight) {
      if (hasNextPage) {
        fetchNextPage();
      }
    }
  };

  // Infinite City
  const [sCity, setSCity] = useState('');
  const {
    data: listCity,
    fetchNextPage: fetchNextPageCity,
    hasNextPage: hasNextPageCity,
    isLoading: isLoadingCity,
  } = useInfiniteAllCity({
    ...baseParams,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    'countryCodes[]': ['vn'],
    s: sCity,
  });

  const optionsCity = useMemo(() => {
    const result: IOption[] = [];
    listCity?.pages?.forEach((page) =>
      page?.data.forEach((city: TCity) => result.push({ value: city._id, label: city.name })),
    );
    return result;
  }, [listCity]);
  const onPopupScrollCity = (e: any) => {
    if (e.target.scrollHeight === e.target.scrollTop + e.target.offsetHeight) {
      if (hasNextPageCity) {
        fetchNextPageCity();
      }
    }
  };

  // Search
  const handleSearch = (s: string) => {
    let newFilter: IFilter;
    if (isFilter) {
      newFilter = { ...filter, s };
    } else {
      newFilter = { s };
    }

    if (newFilter.companySize) {
      const arr = newFilter.companySize.split('-');
      newFilter.companySizeFrom = Number(arr[0]);
      newFilter.companySizeTo = Number(arr[1]);
      delete newFilter.companySize;
    }
    if (!newFilter.companySizeTo) {
      delete newFilter.companySizeTo;
    }
    if (s === '') {
      delete newFilter.s;
    }
    Object.keys(newFilter).forEach((key) => {
      if (!newFilter[key]) {
        delete newFilter[key];
      }
    });

    router.push({ pathname: '/search', query: { ...router.query, ...newFilter } });
  };

  // Suggest
  const [suggest, setSuggest] = useState('');
  const {
    data: suggestData,
    fetchNextPage: fetchNextPageSuggest,
    hasNextPage: hasNextPageSuggest,
  } = queryGetSuggestCompany({
    ...baseParams,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    s: suggest,
    lang: router.locale as ELanguage,
  });
  const optionsSuggest = useMemo(() => {
    const result: IOption[] = [];
    suggestData?.pages?.forEach((page) =>
      page?.data.forEach((company: TCompany) =>
        result.push({
          value: company.slug,
          label: Label({ label: company.name, logo: company?.logo?.location }) as unknown as string,
        }),
      ),
    );
    return result;
  }, [suggestData]);
  const onPopupScrollSuggest = (e: any) => {
    if (e.target.scrollHeight === e.target.scrollTop + e.target.offsetHeight) {
      if (hasNextPageSuggest) {
        fetchNextPageSuggest();
      }
    }
  };

  const onSelect = (value: string) => {
    router.push(`/company/${value}`);
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.banner}>
        <Image
          sizes='(max-width: 768px) 80vw,
          100vw'
          className={styles.bannerImage}
          priority
          src='/images/home/bannerx4.jpeg'
          fill
          alt='banner'
        />
        <Image
          sizes='(max-width: 768px) 80vw,
          100vw'
          className={styles.bannerImageMobile}
          priority
          src='/images/home/bannerMobilex4.jpg'
          fill
          alt='banner'
        />
        <Row className={styles.filterGroup}>
          <Col span={24}>
            <Title className={styles.title} level={1}>
              {t('home:title')}
            </Title>
          </Col>
          <Col span={24} className={styles.searchWrap}>
            <AutoComplete
              className={styles.autoComplete}
              options={optionsSuggest}
              onSelect={onSelect}
              onPopupScroll={onPopupScrollSuggest}
            >
              <SearchInput
                onSearch={handleSearch}
                onChange={debounce((event: React.ChangeEvent<HTMLInputElement>) => setSuggest(event.target.value), 500)}
              />
            </AutoComplete>
          </Col>
          <Col span={24}>
            <Row gutter={[0, 16]}>
              <Col span={24}>
                <Space className={styles.advanceWrap} onClick={() => setIsFilter(!isFilter)} size={8} align='center'>
                  <Image src='/images/home/dot.svg' className={styles.icon} width={16} height={16} alt='advance' />
                  <Text className={styles.advance} strong>
                    {t('home:search_advance')}
                  </Text>
                </Space>
              </Col>
              <Col className={styles.colFilter} span={24}>
                {isFilter && (
                  <Row
                    gutter={[
                      { xs: 8, sm: 24 },
                      { xs: 8, sm: 12 },
                    ]}
                  >
                    <Col span={12} sm={{ span: 12 }} md={{ span: 8 }}>
                      <Select
                        className={`${styles.selectItem} home-select`}
                        style={{ width: '100%' }}
                        allowClear
                        showSearch
                        value={filter['categoryIds[]'] || undefined}
                        onChange={(value) => {
                          setFilter({ ...filter, 'categoryIds[]': value });
                        }}
                        placeholder={t('home:career')}
                        showArrow
                        optionFilterProp='label'
                        onSearch={debounce((value: string) => setSCategory(value), 500)}
                        onPopupScroll={onPopupScrollCategory}
                        loading={isLoading}
                        options={options}
                      />
                    </Col>
                    <Col span={12} sm={{ span: 12 }} md={{ span: 8 }}>
                      <Select
                        allowClear
                        value={filter?.city || undefined}
                        onChange={(value: string) => {
                          setFilter({ ...filter, city: value });
                        }}
                        showSearch
                        onSearch={debounce((value: string) => setSCity(value), 500)}
                        placeholder={t('home:location')}
                        className={`${styles.selectItem} home-select`}
                        onPopupScroll={onPopupScrollCity}
                        loading={isLoadingCity}
                        optionFilterProp='label'
                        options={optionsCity}
                      />
                    </Col>
                    <Col span={12} sm={{ span: 12 }} md={{ span: 8 }}>
                      <Select
                        allowClear
                        value={filter?.companySize || undefined}
                        onChange={(value: string) => {
                          setFilter({ ...filter, companySize: value });
                        }}
                        placeholder={t('home:scale')}
                        className={`${styles.selectItem} home-select`}
                      >
                        {ListOptionCopmanySize.map((item, index) => (
                          <Select.Option key={index} value={item.value}>
                            {item.label[router.locale as ELanguage]}
                          </Select.Option>
                        ))}
                      </Select>
                    </Col>
                  </Row>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div className={`home-container ${styles.bg}`}>
        <div className='home-wrapper'>
          <Row className={styles.content}>
            <Col span={24} className={styles.section}>
              <Row gutter={[0, { lg: 48, md: 32, sm: 20, xs: 20 }]}>
                <Col span={24} className={styles.typicalCompany}>
                  <Title level={2} className={styles.title}>
                    {t('home:typical_company')}
                  </Title>
                  <Text className={`${styles.subTitle} ellipsis threeline`}>{t('home:sub_title')}</Text>
                </Col>
                <Col span={24}>
                  {isErrorTypical ? (
                    <Error />
                  ) : (
                    <Row gutter={[26, 32]}>
                      {listTypicalCompany &&
                        listTypicalCompany.data.map((company) => (
                          <Col key={company._id} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                            <TypicalCompany data={company} />
                          </Col>
                        ))}
                      {listTypicalCompany && listTypicalCompany.data.length === 0 && (
                        <Empty className='empty' description={t('no_data')} />
                      )}
                    </Row>
                  )}
                </Col>
                <Col span={24}>
                  <Pagination
                    responsive
                    className={styles.pagination}
                    size='small'
                    onChange={(page) => {
                      setPageTypical(page);
                    }}
                    current={pageTypical || 1}
                    hideOnSinglePage
                    defaultCurrent={1}
                    total={Math.ceil(((listTypicalCompany?.total || 0) * 10) / 6)}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={24} className={styles.section}>
              <Row gutter={[0, { lg: 48, md: 32, sm: 20, xs: 20 }]}>
                <Col span={24} className={styles.titleGroup}>
                  <Row align='middle' gutter={[0, 12]}>
                    <Col span={16}>
                      <Title level={2} className={styles.title}>
                        {t('home:new_enterprise')}
                      </Title>
                    </Col>
                    <Col className={styles.btnCol} span={8}>
                      <Button
                        onClick={() => router.push({ pathname: urlHeader.findCompany })}
                        type='primary'
                        className={styles.btn}
                      >
                        {t('view_all')}
                      </Button>
                    </Col>
                  </Row>
                </Col>
                {isErrorCompany ? (
                  <Error />
                ) : (
                  <Col span={24}>
                    <Row gutter={[26, 32]}>
                      {listCompany &&
                        listCompany.data.map((company) => (
                          <Col key={company._id} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                            <NewEnterprise data={company} />
                          </Col>
                        ))}
                      {listCompany && listCompany.data.length === 0 && (
                        <Empty className='empty' description={t('no_data')} />
                      )}
                    </Row>
                  </Col>
                )}

                <Col span={24}>
                  <Pagination
                    responsive
                    className={styles.pagination}
                    size='small'
                    onChange={(page) => setPageCompany(page)}
                    current={pageCompany || 1}
                    hideOnSinglePage
                    defaultCurrent={1}
                    total={Math.ceil(((listCompany?.total || 0) * 10) / 9)}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={24} className={styles.section}>
              <Carousel autoplay className={styles.carousel} effect='fade'>
                {homeOption[0]?.value?.map((slide: any, key: number) => (
                  <CarouselItem
                    key={key}
                    alt={slide.alternativeText}
                    altMobile={slide.alternativeTextMB}
                    banner={slide.location}
                    bannerMobile={slide.locationMb}
                    title={slide.title}
                    subTitle={slide.subTitle}
                    desc={slide.description}
                    btn={t('home:discover_now')}
                  />
                ))}
              </Carousel>
            </Col>
            <Col span={24} className={styles.section}>
              <Row gutter={[0, { lg: 48, md: 32, sm: 20, xs: 20 }]}>
                <Col span={24} className={styles.titleGroup}>
                  <Row align='middle' gutter={[0, 12]}>
                    <Col span={16}>
                      <Title level={2} className={styles.title}>
                        {t('home:list_of_occupations')}
                      </Title>
                    </Col>
                  </Row>
                </Col>
                <Col span={24}>
                  <Row gutter={[24, 24]}>
                    {listAllCategory &&
                      listAllCategory.data.map((category) => (
                        <Col key={category._id} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 6 }} span={24}>
                          <CareerCategory data={category} />
                        </Col>
                      ))}
                    {listAllCategory && listAllCategory.data.length === 0 && (
                      <Empty className='empty' description={t('no_data')} />
                    )}
                  </Row>
                </Col>
                <Col span={24}>
                  <Pagination
                    responsive
                    className={styles.pagination}
                    size='small'
                    onChange={(page) => setPageCategory(page)}
                    current={pageCategory || 1}
                    hideOnSinglePage
                    defaultCurrent={1}
                    total={Math.ceil(((listAllCategory?.total || 0) * 10) / 12)}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={24} className={styles.section}>
              <Row>
                <Col span={24} className={styles.outstanding}>
                  <Image
                    sizes='(max-width: 768px) 80vw,
                    100vw'
                    className={styles.img}
                    fill
                    src='/images/home/outstanding1.png'
                    alt='outstanding'
                  />
                  <Image
                    sizes='(max-width: 768px) 80vw,
                    100vw'
                    className={styles.imgMobile}
                    fill
                    src='/images/home/outstandingMobile.png'
                    alt='outstanding'
                  />
                  <Row className={styles.content}>
                    <Col span={24}>
                      <Text className={styles.head}>{t('home:grow_enterprise')}</Text>
                      <Title className={styles.title} level={4}>
                        {t('home:outstanding_numbers')}
                      </Title>
                      <Col className={styles.lineWrap}>
                        <Row>
                          <Col span={24}>
                            <Row className={styles.statistical} gutter={[{ xs: 16, sm: 24, md: 48 }, 48]}>
                              <Col lg={{ span: 12 }} xl={{ span: 6 }} span={12}>
                                <div className={styles.line}>
                                  <Image
                                    className={styles.lineIcon}
                                    src='/images/home/icon1.svg'
                                    width={32}
                                    height={32}
                                    alt='icon'
                                  />
                                </div>
                                <Title className={`${styles.title} ellipsis oneline`} level={3}>
                                  {t('home:milion')}
                                </Title>
                                <Text className={`${styles.desc} ellipsis twoline`}>{t('home:verify_number')}</Text>
                              </Col>
                              <Col lg={{ span: 12 }} xl={{ span: 6 }} span={12}>
                                <div className={styles.line}>
                                  <Image
                                    className={styles.lineIcon}
                                    src='/images/home/icon2.svg'
                                    width={32}
                                    height={32}
                                    alt='icon'
                                  />
                                </div>
                                <Title className={`${styles.title} ellipsis oneline`} level={3}>
                                  50k+
                                </Title>
                                <Text className={`${styles.desc} ellipsis twoline`}>
                                  {t('home:verify_account_number')}
                                </Text>
                              </Col>
                              <Col lg={{ span: 12 }} xl={{ span: 6 }} span={12}>
                                <div className={styles.line}>
                                  <Image
                                    className={styles.lineIcon}
                                    src='/images/home/icon3.svg'
                                    width={32}
                                    height={32}
                                    alt='icon'
                                  />
                                </div>
                                <Title className={`${styles.title} ellipsis oneline`} level={3}>
                                  800%
                                </Title>
                                <Text className={`${styles.desc} ellipsis twoline`}>{t('home:growth_number')}</Text>
                              </Col>
                              <Col lg={{ span: 12 }} xl={{ span: 6 }} span={12}>
                                <div className={`${styles.line} ${styles.last}`}>
                                  <Image
                                    className={styles.lineIcon}
                                    src='/images/home/icon4.svg'
                                    width={32}
                                    height={32}
                                    alt='icon'
                                  />
                                </div>
                                <Title className={`${styles.title} ellipsis oneline`} level={3}>
                                  100%
                                </Title>
                                <Text className={`${styles.desc} ellipsis twoline`}>
                                  {t('home:probability_number')}
                                </Text>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col span={24} className={styles.section}>
              <Row gutter={[58, 30]}>
                <Col span={24} lg={{ span: 15 }}>
                  <SlideHotNews
                    slideElement={slideElementHotNews}
                    isNavigation
                    isPagination
                    slidesPerView={1}
                    slideKey='news'
                  />
                </Col>
                <Col className={styles.newsCol} span={24} lg={{ span: 9 }}>
                  <Row>
                    <Col span={24} className={`${styles.newsTitle} ${styles.titleGroup}`}>
                      <Row align='middle' gutter={[0, 12]}>
                        <Col span={16}>
                          <Title level={2} className={styles.title}>
                            {t('home:news')}
                          </Title>
                        </Col>
                        <Col className={styles.btnCol} span={8}>
                          <Button
                            onClick={() => router.push({ pathname: urlHeader.news })}
                            type='primary'
                            size='small'
                            ghost
                            className={styles.btn}
                          >
                            {t('view_all')}
                          </Button>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  {listNews.length > 0 && (
                    <Row gutter={[24, 24]}>
                      {listNews.map((value, key) => {
                        if (key < listNews.length - 1) {
                          return (
                            <Col key={value._id} span={24}>
                              <Post data={value} />
                            </Col>
                          );
                        }
                        if (key === listNews.length - 1) {
                          return (
                            <Col key={value._id} span={24}>
                              <Post data={value} isLast />
                            </Col>
                          );
                        }
                        return false;
                      })}
                    </Row>
                  )}
                  <Row>{listNews.length === 0 && <Empty className='empty' description={t('no_data')} />}</Row>
                </Col>
              </Row>
            </Col>
            <Col span={24} className={styles.section}>
              <Row>
                <Col span={24}>
                  <BannerItem
                    banner='/images/home/lastBannerx4.jpg'
                    bannerMobile='/images/home/lastBannerMobilex4.jpg'
                    head={t('home:head_banner')}
                    title={t('home:title_banner')}
                    btn={t('home:register_now')}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}

export default HomeScreen;
