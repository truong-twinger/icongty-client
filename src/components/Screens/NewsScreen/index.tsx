import { Col, Empty, Row } from 'antd';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import React from 'react';

import BreadcrumbPage from '@/components/Elements/Breadcrumb';
import HotNews from '@/components/Elements/HotNews';
import NewsCategory from '@/components/Elements/NewsCategory';
import { TPost, TTaxonomy } from '@/modules';

import styles from './style.module.less';

export interface NewsProps {
  listAllNew: TPost[];
  listAllTax: TTaxonomy[];
}
function NewsScreen({ listAllNew, listAllTax }: NewsProps) {
  const router = useRouter();
  const { t } = useTranslation();

  return (
    <>
      <div className={styles.bg}>
        <div className='home-container'>
          <div className='home-wrapper'>
            <Row className={styles.wrapper}>
              <Col span={24} className={styles.breadcrumb}>
                <BreadcrumbPage router={router} />
              </Col>
              {listAllNew && listAllNew.length > 0 && <HotNews listAllNew={listAllNew} />}
              {listAllNew.length === 0 && <Empty description={t('no_data')} className='empty' />}
            </Row>
          </div>
        </div>
      </div>

      <div className={styles.bgGrey}>
        <div className='home-container'>
          <div className='home-wrapper'>
            <Row className={styles.wrapper2}>
              {listAllTax && listAllTax.length > 0 && <NewsCategory listAllTax={listAllTax} />}
            </Row>
          </div>
        </div>
      </div>
    </>
  );
}

export default NewsScreen;
