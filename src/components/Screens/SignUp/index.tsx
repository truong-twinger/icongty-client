import { Button, Col, Form, Input, Row, Typography } from 'antd';
import Link from 'next/link';
import { useState } from 'react';

import { TRegister } from '@/modules';
import { useRegister } from '@/queries/hooks';
import { regexEmail, regexPwdStrong } from '@/utils/regex';

import styles from './style.module.less';

const { Title } = Typography;

export default function SignUpScreen() {
  const [step, setStep] = useState(1);
  const { mutate: register } = useRegister();
  const onFinish = (values: TRegister) => {
    register(values, {
      onSuccess: () => {
        setStep(2);
      },
    });
  };
  return (
    <Form onFinish={onFinish} layout='vertical' className={styles.wrapper}>
      {step === 1 && (
        <Row gutter={[0, 24]} className={styles.formGroup}>
          <Col span={24}>
            <Title className={styles.title} level={1}>
              Đăng ký
            </Title>
          </Col>
          <Col span={24}>
            <Form.Item
              label='Tên đăng nhập'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên đăng nhập',
                },
              ]}
              name='username'
            >
              <Input placeholder='Tên đăng nhập' />
            </Form.Item>
            <Form.Item
              label='Email'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập email',
                },
                { pattern: regexEmail, message: 'Email không đúng định dạng' },
              ]}
              name='email'
            >
              <Input placeholder='Email' />
            </Form.Item>
            <Form.Item
              label='Tên'
              name='firstName'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên của bạn',
                },
              ]}
            >
              <Input placeholder='Tên' />
            </Form.Item>
            <Form.Item
              label='Họ'
              name='lastName'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập họ của bạn',
                },
              ]}
            >
              <Input placeholder='Họ' />
            </Form.Item>
            <Form.Item
              label='Mật khẩu'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập Mật khẩu',
                },
                {
                  pattern: regexPwdStrong,
                  message:
                    'Mật khẩu bao gồm chữ hoa, chữ thường, số, kí tự đặc biệt và tối thiểu 8 kí tự, tối đa 15 kí tự',
                },
              ]}
              name='password'
            >
              <Input.Password placeholder='Mật khẩu' />
            </Form.Item>
            <Form.Item
              label='Nhập lại mật khẩu'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng xác thực mật khẩu',
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Mật khẩu không khớp'));
                  },
                }),
              ]}
              name='confirmPassword'
            >
              <Input.Password placeholder='Mật khẩu' />
            </Form.Item>
            <Form.Item>
              <Button type='primary' htmlType='submit' block>
                Đăng ký
              </Button>
            </Form.Item>
          </Col>
        </Row>
      )}

      {step === 2 && (
        <Row className={styles.formGroup}>
          <Col span={24}>
            Một email đã được gửi đi. Vui lòng kiểm tra hòm thư của bạn.
            <Link href='/'>Về trang chủ</Link>
          </Col>
        </Row>
      )}
    </Form>
  );
}
