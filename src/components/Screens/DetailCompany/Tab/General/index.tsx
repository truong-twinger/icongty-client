import { CopyOutlined, VerticalAlignTopOutlined } from '@ant-design/icons';
import { Col, message, Row, Space, Typography } from 'antd';
import moment from 'moment';
import { useTranslation } from 'next-i18next';
import React, { useMemo, useRef, useState } from 'react';

import ProductItem from '@/components/Elements/ProductItem';
import ProfileItem from '@/components/Elements/ProfileItem';
import ServiceItem from '@/components/Elements/ServiceItem';
import SlideCustomize from '@/components/Widgets/SlideHotNews';
import { EStatusDoc } from '@/configs/interface.config';
import { chunkArray } from '@/libs/const';
import { ETab, TCompany, TPortfolio, TProduct } from '@/modules';
import { TService } from '@/modules/services';

import styles from './style.module.less';

const { Title, Text } = Typography;

interface Props {
  detailCompany: TCompany;
  ListPortfolio?: TPortfolio[];
  ListService?: TService[];
  ListProduct?: TProduct[];
  handleChangeTab: (tab: ETab) => void;
}

export default function General({ handleChangeTab, detailCompany, ListPortfolio, ListProduct, ListService }: Props) {
  const { t } = useTranslation();
  // console.log('🚀 ~ file: index.tsx:43 ~ DetailCompany ~ dataProduct:', dataProduct);

  const slideElementProfile = useMemo(() => {
    const array = [];
    if (ListPortfolio) {
      array.push(
        ...chunkArray(
          ListPortfolio?.filter((item) => item.status === EStatusDoc.ACTIVE),
          3,
        ),
      );
    }
    return array.map((value) => (
      <React.Fragment>
        {value.map((item) => (
          <ProfileItem data={item} />
        ))}
      </React.Fragment>
    ));
  }, []);

  const slideElementProduct = useMemo(() => {
    const array = [];
    if (ListProduct) {
      array.push(
        ...chunkArray(
          ListProduct?.filter((item) => item.status === EStatusDoc.ACTIVE),
          3,
        ),
      );
    }
    return array.map((value) => (
      <React.Fragment>
        {value.map((item, i) => (
          <ProductItem index={i + 1} data={item} />
        ))}
      </React.Fragment>
    ));
  }, []);

  const slideElementService = useMemo(() => {
    const array = [];
    if (ListService) {
      array.push(
        ...chunkArray(
          ListService.filter((item) => item.status === EStatusDoc.ACTIVE),
          3,
        ),
      );
    }
    return array.map((value) => (
      <React.Fragment>
        {value.map((item, i) => (
          <ServiceItem index={i + 1} data={item} />
        ))}
      </React.Fragment>
    ));
  }, []);

  const handleCopy = (e: { preventDefault: () => void }, code: string) => {
    e.preventDefault();
    navigator.clipboard
      .writeText(code)
      .then(() => {
        message.info(`Copied ${code}`);
      })
      .catch(() => {
        message.error('Error!!');
      });
  };

  const [isReadmore, setIsReadmore] = useState(false);
  const myRef = useRef<null | HTMLDivElement>(null);

  return (
    <Row gutter={[0, 24]}>
      <Col span={24} className={styles.intro}>
        <div className='card-company'>
          <div className='title-group'>
            <Title level={2} className='title'>
              {t('company:introduce')}
            </Title>
          </div>
          <Row gutter={[{ xs: 12, sm: 12, lg: 18, xl: 24 }, 16]} className='content'>
            <Col span={24} sm={{ span: 12 }} md={{ span: 24 }} lg={{ span: 12 }}>
              <Space size={8} direction='vertical'>
                <Text className={styles.label}>{t('company:full_name')}:</Text>
                <Text strong className={styles.content}>
                  {detailCompany?.name}
                </Text>
              </Space>
            </Col>
            <Col span={24} sm={{ span: 12 }} md={{ span: 24 }} lg={{ span: 12 }}>
              <Space size={8} direction='vertical'>
                <Text className={styles.label}>{t('company:international_name')}:</Text>
                <Text strong className={styles.content}>
                  {detailCompany?.internationalName || t('updating')}
                </Text>
              </Space>
            </Col>
            <Col span={24} sm={{ span: 12 }} md={{ span: 24 }} lg={{ span: 12 }}>
              <Space size={8} direction='vertical'>
                <Text className={styles.label}>{t('company:tax_code')}:</Text>
                <Space size={5} align='center'>
                  <Text strong className={styles.content}>
                    {detailCompany?.taxCode || t('updating')}
                  </Text>
                  {detailCompany?.taxCode && <CopyOutlined onClick={(e) => handleCopy(e, detailCompany.taxCode)} />}
                </Space>
              </Space>
            </Col>
            <Col span={24} sm={{ span: 12 }} md={{ span: 24 }} lg={{ span: 12 }}>
              <Space size={8} direction='vertical'>
                <Text className={styles.label}>{t('company:address')}:</Text>
                <Text strong className={styles.content}>
                  {detailCompany?.address || t('updating')}
                </Text>
              </Space>
            </Col>
            <Col span={24} sm={{ span: 12 }} md={{ span: 24 }} lg={{ span: 12 }}>
              <Space size={8} direction='vertical'>
                <Text className={styles.label}>{t('company:founding')}:</Text>
                {detailCompany?.foundationDate ? (
                  <Text strong className={styles.content}>
                    {moment(detailCompany?.foundationDate).format('DD/MM/YYYY')}
                  </Text>
                ) : (
                  <Text strong className={styles.content}>
                    {t('updating')}
                  </Text>
                )}
              </Space>
            </Col>
            <Col span={24} sm={{ span: 12 }} md={{ span: 24 }} lg={{ span: 12 }}>
              <Space size={8} direction='vertical'>
                <Text className={styles.label}>{t('company:personnel_size')}:</Text>
                {detailCompany?.companySize ? (
                  <Text strong className={styles.content}>
                    {detailCompany.companySize} {t('employees')}
                  </Text>
                ) : (
                  <Text strong className={styles.content}>
                    {t('updating')}
                  </Text>
                )}
              </Space>
            </Col>
          </Row>
        </div>
      </Col>
      {detailCompany?.description && (
        <Col span={24} className={styles.aboutUs}>
          <div className='card-company'>
            <div className='title-group'>
              <Title level={2} className='title'>
                {t('company:about_us')}
              </Title>
            </div>
            <Row className='content'>
              <Col
                ref={myRef}
                span={24}
                className={`${styles.content} ${!isReadmore && styles.hide}`}
                dangerouslySetInnerHTML={{ __html: detailCompany?.description || '' }}
              />
            </Row>

            <Row>
              <Col className={styles.readMore} span={24}>
                <Row gutter={[24, 0]}>
                  <Col className={styles.line} />
                  <Col>
                    {isReadmore ? (
                      <Text strong onClick={() => setIsReadmore(false)}>
                        <Space size={9} align='center'>
                          <VerticalAlignTopOutlined /> {t('company:collapse')}
                        </Space>
                      </Text>
                    ) : (
                      <Text strong onClick={() => setIsReadmore(true)}>
                        {t('company:read_more')}
                      </Text>
                    )}
                  </Col>
                </Row>
              </Col>
            </Row>
            {/* {myRef && myRef.current && myRef.current?.clientHeight > 88 && (
            )} */}
          </div>
        </Col>
      )}

      <Col span={24} className={styles.profile}>
        <div className='card-company'>
          <div className='title-group'>
            <Title level={2} className='title'>
              {t('company:capacity_profile')}
            </Title>
            <Text onClick={() => handleChangeTab(ETab.PORTFOLIO)} className='view-detail' strong>
              {t('view_more')}
            </Text>
          </div>
          <div className='content'>
            {slideElementProfile ? (
              <SlideCustomize
                slideElement={slideElementProfile}
                isNavigation
                isPagination
                slidesPerView={1}
                slideKey='profile'
              />
            ) : (
              'loading'
            )}
          </div>
        </div>
      </Col>
      <Col span={24} className={styles.profile}>
        <div className='card-company'>
          <div className='title-group'>
            <Title level={2} className='title'>
              {t('company:product')}
            </Title>
            <Text onClick={() => handleChangeTab(ETab.PRODUCT)} className='view-detail' strong>
              {t('view_more')}
            </Text>
          </div>
          <div className='content'>
            {slideElementProduct ? (
              <SlideCustomize
                slideElement={slideElementProduct}
                isNavigation
                isPagination
                slidesPerView={1}
                slideKey='product'
              />
            ) : (
              'loading'
            )}
          </div>
        </div>
      </Col>
      <Col span={24} className={styles.profile}>
        <div className='card-company'>
          <div className='title-group'>
            <Title level={2} className='title'>
              {t('company:service')}
            </Title>
            <Text onClick={() => handleChangeTab(ETab.SERVICE)} className='view-detail' strong>
              {t('view_more')}
            </Text>
          </div>
          <div className='content'>
            <SlideCustomize
              slideElement={slideElementService}
              isNavigation
              isPagination
              slidesPerView={1}
              slideKey='service'
            />
          </div>
        </div>
      </Col>
    </Row>
  );
}
