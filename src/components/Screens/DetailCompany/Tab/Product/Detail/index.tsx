import { LeftOutlined } from '@ant-design/icons';
import { Col, Row, Typography } from 'antd';

import styles from './style.module.less';

const { Text, Title } = Typography;

export default function DetailProduct() {
  return (
    <Row gutter={[0, 24]} className={`${styles.wrapper}`}>
      <Col span={24}>
        <Row className={`${styles.wrapper} card-company`}>
          <Col className={styles.backLink} span={24}>
            <Text strong>
              <LeftOutlined /> Trở lại danh mục
            </Text>
          </Col>
          <Col
            className={styles.content}
            span={24}
            // dangerouslySetInnerHTML={{ __html: data?.data?.description || '' }}
          />
        </Row>
      </Col>
      <Col span={24} className={`card-company ${styles.list}`}>
        <Title level={2}>Các sản phẩm khác</Title>
        {/* <Row className={styles.listItem} gutter={[24.5, 0]}>
          {listItem?.map((item) => (
            <Col key={item._id} span={8}>
              <div className={styles.thumbnailWrap}>
                {data?.data?.logo ? (
                  <div className={styles.logoWrap}>
                    <Image
                      placeholder='blur'
                      blurDataURL={getBlurDataUrl(data.data.logo.width || 256, data.data.logo.height || 341)}
                      alt={data?.data.name}
                      src={data?.data?.logo?.location}
                      fill
                      sizes='30vw'
                    />
                  </div>
                ) : (
                  <Image
                    alt='logo image company'
                    className={styles.thumbnail}
                    src='/images/category-company/logocompany.jpg'
                    width={210}
                    height={140}
                    sizes='30vw'
                  />
                )}
              </div>
              <Title level={3}>Sản phẩm 1</Title>
            </Col>
          ))}
          {listItem?.length === 0 && <Empty className='empty' description={t('no_data')} />}
        </Row> */}
      </Col>
    </Row>
  );
}
