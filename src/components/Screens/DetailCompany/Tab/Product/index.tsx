import { Col, Empty, Pagination, Row } from 'antd';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import ProductItem from '@/components/Elements/ProductItem';
import { baseParams } from '@/configs/const.config';
import { ELanguage, EStatusDoc } from '@/configs/interface.config';
import { useQueryProductByCompanyID } from '@/queries/hooks';

import styles from './style.module.less';

interface Props {
  idCompany: string;
}

export default function Product({ idCompany }: Props) {
  const { t } = useTranslation();
  const router = useRouter();

  const [page, setPage] = useState(1);
  const { data: dataProduct } = useQueryProductByCompanyID(
    { ...baseParams, page, limit: 10, companyId: idCompany },
    router.locale as ELanguage,
  );

  useEffect(() => {
    window.scrollTo(0, 400);
  }, [page]);
  return (
    <Row gutter={[0, { lg: 32, xs: 12, md: 24 }]}>
      <Col span={24} className={styles.profile}>
        {dataProduct?.data
          ?.filter((item) => item.status === EStatusDoc.ACTIVE)
          ?.map((item, index) => (
            <div key={item._id} className={`card-company ${styles.item}`}>
              <div>
                <ProductItem index={index + 1} data={item} />
              </div>
            </div>
          ))}
      </Col>
      {dataProduct?.data?.filter((item) => item.status === EStatusDoc.ACTIVE)?.length === 0 && (
        <Col span={24}>
          <Empty className='empty' description={t('no_data')} />
        </Col>
      )}
      <Col span={24}>
        {dataProduct && dataProduct?.total > 0 ? (
          <Pagination
            responsive
            className={styles.pagination}
            size='small'
            onChange={(page) => setPage(page)}
            current={Number(page) || 1}
            hideOnSinglePage
            defaultCurrent={1}
            total={Math.ceil((dataProduct?.total * 10) / 10)}
          />
        ) : (
          ''
        )}
      </Col>
    </Row>
  );
}
