import { Col, Empty, Pagination, Row } from 'antd';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import ServiceItem from '@/components/Elements/ServiceItem';
import { baseParams } from '@/configs/const.config';
import { ELanguage, EStatusDoc } from '@/configs/interface.config';
import { useQueryServiceByCompanyID } from '@/queries/hooks';

import styles from './style.module.less';

interface Props {
  idCompany?: string;
}

export default function Service({ idCompany }: Props) {
  const { t } = useTranslation();
  const router = useRouter();

  const [page, setPage] = useState(1);
  const { data: dataService } = useQueryServiceByCompanyID(
    { ...baseParams, page, limit: 10, companyId: idCompany },
    router.locale as ELanguage,
  );

  useEffect(() => {
    window.scrollTo(0, 400);
  }, [page]);
  return (
    <Row gutter={[0, { lg: 32, xs: 12, md: 24 }]}>
      <Col span={24} className={styles.profile}>
        {dataService?.data
          ?.filter((item) => item.status === EStatusDoc.ACTIVE)
          ?.map((item, index) => (
            <div key={item._id} className={`card-company ${styles.item}`}>
              <div>
                <ServiceItem index={index + 1} data={item} />
              </div>
            </div>
          ))}
      </Col>
      {dataService?.data?.filter((item) => item.status === EStatusDoc.ACTIVE)?.length === 0 && (
        <Col span={24}>
          <Empty className='empty' description={t('no_data')} />
        </Col>
      )}
      <Col span={24}>
        {dataService?.total && dataService?.total > 0 ? (
          <Pagination
            responsive
            className={styles.pagination}
            size='small'
            onChange={(page) => setPage(page)}
            current={Number(page) || 1}
            hideOnSinglePage
            defaultCurrent={1}
            total={Math.ceil((dataService?.total * 10) / 10)}
          />
        ) : (
          ''
        )}
      </Col>
    </Row>
  );
}
