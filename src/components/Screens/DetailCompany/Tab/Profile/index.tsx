import { Col, Empty, Pagination, Row } from 'antd';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import ProfileItem from '@/components/Elements/ProfileItem';
import { baseParams } from '@/configs/const.config';
import { ELanguage, EStatusDoc } from '@/configs/interface.config';
import { useQueryPortfolioByCompanyID } from '@/queries/hooks';

import styles from './style.module.less';

interface Props {
  idCompany: string;
}

export default function Product({ idCompany }: Props) {
  const { t } = useTranslation();
  const router = useRouter();

  const [page, setPage] = useState(1);
  const { data: dataPortfolio } = useQueryPortfolioByCompanyID(
    { ...baseParams, page, limit: 10, companyId: idCompany },
    router.locale as ELanguage,
  );
  useEffect(() => {
    window.scrollTo(0, 400);
  }, [page]);
  return (
    <Row gutter={[0, { lg: 32, xs: 12, md: 24 }]}>
      <Col span={24} className={styles.profile}>
        {dataPortfolio?.data
          ?.filter((item) => item.status === EStatusDoc.ACTIVE)
          ?.map((item) => (
            <div key={item._id} className={`card-company ${styles.item}`}>
              <div className='content'>
                <ProfileItem data={item} />
              </div>
            </div>
          ))}
      </Col>
      {dataPortfolio?.data.filter((item) => item.status === EStatusDoc.ACTIVE)?.length === 0 && (
        <Col span={24}>
          <Empty className='empty' description={t('no_data')} />
        </Col>
      )}
      <Col span={24}>
        {dataPortfolio && dataPortfolio?.total > 0 ? (
          <Pagination
            responsive
            className={styles.pagination}
            size='small'
            onChange={(page) => setPage(page)}
            current={Number(page) || 1}
            hideOnSinglePage
            defaultCurrent={1}
            total={Math.ceil(((dataPortfolio?.total || 0) * 10) / 10)}
          />
        ) : (
          ''
        )}
      </Col>
    </Row>
  );
}
