/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line react/jsx-wrap-multilines
import { Button, Col, Row, Space, Tooltip, Typography } from 'antd';
import moment from 'moment';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useState } from 'react';
import { FacebookShareButton, LinkedinShareButton, TwitterShareButton } from 'react-share';

import BreadcrumbPage from '@/components/Elements/Breadcrumb';
import RelatedCompany from '@/components/Elements/RelatedCompany';
import { BASE_URL } from '@/configs/env.config';
import { ELanguage } from '@/configs/interface.config';
import { useWindowSize } from '@/hooks';
import { EADS_KEY, ETab, TAdvertising, TCompany, TPortfolio, TProduct, TService } from '@/modules';
import { useQueryAdvertising } from '@/queries/hooks';
import AllTag from '@/components/Elements/AllTag';

import { ContactIcon, DiscoverIcon, GeneralIcon, ProductIcon, ProfileIcon, ServiceIcon } from './Icon';
import styles from './style.module.less';
import General from './Tab/General';
import Product from './Tab/Product';
import Profile from './Tab/Profile';
import Service from './Tab/Service';

const { Title, Text } = Typography;

interface IInfo {
  phone: boolean;
  email: boolean;
  website: boolean;
}

interface IProps {
  detailCompany: TCompany;
  listRelatedCompany: TCompany[];
  listPortfolio: TPortfolio[];
  listService: TService[];
  listProduct: TProduct[];
}

export default function DetailCompany({
  detailCompany,
  listRelatedCompany,
  listPortfolio,
  listService,
  listProduct,
}: IProps) {
  const router = useRouter();

  const { t } = useTranslation();
  const link = `${BASE_URL}${router.asPath.slice(1)}`;

  const [active, setActive] = useState<ETab>((router.query?.tab as ETab) || ETab.GENERAL);
  useEffect(() => {
    setActive((router.query?.tab as ETab) || ETab.GENERAL);
    if (router.query?.tab) {
      window.scrollTo(0, 400);
    }
  }, [router.query?.tab]);

  const name = useMemo(
    () =>
      router.locale === ELanguage.EN ? detailCompany?.internationalName || detailCompany?.name : detailCompany?.name,
    [router],
  );

  // ads
  const [width] = useWindowSize();
  const { data: adsData } = useQueryAdvertising();
  const srcAds = useMemo(() => {
    const dataAds = adsData?.data?.values?.find((value: TAdvertising) => value.key === EADS_KEY.NEWS_LIST);
    if (width < 576) {
      return dataAds?.mb?.location;
    }
    if (width < 768) {
      return dataAds?.tablet?.location;
    }
    return dataAds?.pc?.location;
  }, [adsData]);

  const [y, setY] = useState(0);
  useEffect(() => {
    const handleScroll = (event: any) => {
      const window = event.currentTarget;
      setY(window.scrollY);
    };
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [y]);

  const handleChangeTab = (tab: ETab) => {
    delete router.query?.page;
    router.replace(
      {
        pathname: router.pathname,
        query: { ...router.query, tab },
      },
      undefined,
      { shallow: true },
    );
  };

  const [showInfo, setShowInfo] = useState<IInfo>({
    phone: false,
    email: false,
    website: false,
  });
  useEffect(() => {
    setShowInfo({
      phone: false,
      email: false,
      website: false,
    });
  }, [router]);

  return (
    <div className={styles.bg}>
      <div className='home-container'>
        <div className='home-wrapper'>
          <Row className={styles.wrapper}>
            <Col span={24} className='breadcrumb'>
              <BreadcrumbPage
                detailName={
                  router.locale === ELanguage.VI
                    ? detailCompany?.name
                    : detailCompany?.internationalName || detailCompany?.name
                }
                router={router}
              />
            </Col>
            <Col span={24} className={`${styles.topCard}`}>
              <Row>
                <Col span={24} className={styles.coverImage}>
                  <Image
                    className={styles.img}
                    src={detailCompany?.coverPhoto?.location || '/images/category-company/covercompany.jpg'}
                    fill
                    alt={name}
                  />
                </Col>
                <Col span={24} className={`${styles.content} ${styles.paddingCard}`}>
                  <Row gutter={[{ xs: 12, lg: 24, xl: 56 }, 12]} className={styles.avatarWrap} align='bottom'>
                    <Col className={styles.avatarCol}>
                      <Image
                        className={styles.avatar}
                        src={detailCompany?.logo?.location || '/images/category-company/logocompany.jpg'}
                        width={150}
                        height={150}
                        alt={name}
                      />
                    </Col>
                    <Col className={styles.contactCol}>
                      <Row gutter={[0, { xs: 8, sm: 11 }]} className={styles.contact}>
                        <Col className={styles.colLeft} span={24} lg={{ span: 24 }} xl={{ span: 14 }}>
                          <Title className={`${styles.title} ellipsis twoline`} level={1}>
                            {name}
                          </Title>
                        </Col>
                        <Col
                          order={4}
                          className={styles.colCenter}
                          span={24}
                          md={{ span: 12 }}
                          xl={{ span: 10, order: 0 }}
                        >
                          <Row gutter={[19, 0]} className={styles.btnGroup}>
                            <Col>
                              <a target='_blank' rel='noreferrer' href={detailCompany?.website || '/'}>
                                <Button
                                  disabled={!detailCompany?.website}
                                  icon={<DiscoverIcon styles={styles} />}
                                  className={`${styles.btn} ${!detailCompany?.website && styles.disabled} btn-company`}
                                  size='small'
                                  type='primary'
                                  ghost
                                >
                                  <strong>{t('company:find_out_more')}</strong>
                                </Button>
                              </a>
                            </Col>
                            <Col>
                              <a target='_blank' rel='noreferrer' href={`tel:${detailCompany?.phone}`}>
                                <Button
                                  disabled={!detailCompany?.phone}
                                  icon={<ContactIcon styles={styles} />}
                                  className={`${styles.btn} ${!detailCompany?.website && styles.disabled} btn-company`}
                                  size='small'
                                  type='primary'
                                >
                                  <strong>{t('company:contact')}</strong>
                                </Button>
                              </a>
                            </Col>
                          </Row>
                        </Col>
                        <Col className={styles.colLeft} span={24} lg={{ span: 24 }} xl={{ span: 15 }}>
                          <Space size={8}>
                            {detailCompany?.categories?.map(
                              (category, index) =>
                                index < 2 && (
                                  <div key={category._id} className='tag ellipsis oneline'>
                                    {category?.translation?.[router.locale as ELanguage]?.name}
                                  </div>
                                ),
                            )}
                            {detailCompany?.categories?.length > 2 ? (
                              <Tooltip
                                trigger={['hover', 'click']}
                                color='#fff'
                                placement='bottom'
                                title={
                                  <AllTag list={detailCompany.categories.slice(2, detailCompany.categories.length)} />
                                }
                              >
                                <div className='tag ellipsis oneline'>
                                  +{(detailCompany?.categories?.length || 2) - 2}
                                </div>
                              </Tooltip>
                            ) : (
                              <div className={styles.empty} />
                            )}
                          </Space>
                        </Col>
                        <Col
                          className={styles.colCenter}
                          span={24}
                          order={3}
                          md={{ span: 12 }}
                          xl={{ span: 9, order: 0 }}
                        >
                          <Space size={10}>
                            <Text className={`ellipsis oneline ${styles.join}`}>
                              {t('company:join_from')} {moment(detailCompany?.createdAt).format('MM/YYYY')}
                            </Text>
                            {/* <Image src='/images/category-company/dot.svg' width={4} height={4} alt='dot' /> */}
                            {/* <Space size={4} align='center'>
                              <Image
                                src='/images/category-company/doublecheck.svg'
                                width={16}
                                height={10}
                                alt='doublecheck'
                              />
                              <Text className={`ellipsis oneline ${styles.confirm}`} strong>
                                {t('company:verified')}
                              </Text>
                            </Space> */}
                          </Space>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
                <Col span={24} className={`${styles.tab} ${styles.paddingCard} ${y > 750 && styles.upTop}`}>
                  <Row gutter={[{ xs: 40, sm: 24, md: 24, lg: 12 }, 0]} className={styles.tabRow}>
                    <Col onClick={() => handleChangeTab(ETab.GENERAL)}>
                      <Space
                        align='center'
                        className={`${styles.tabItem} ${active === ETab.GENERAL && styles.active}`}
                        size={10}
                      >
                        <GeneralIcon />
                        <Text strong>{t('company:general')}</Text>
                      </Space>
                    </Col>
                    <Col onClick={() => handleChangeTab(ETab.PORTFOLIO)}>
                      <Space
                        align='center'
                        className={`${styles.tabItem} ${active === ETab.PORTFOLIO && styles.active}`}
                        size={10}
                      >
                        <ProfileIcon />
                        <Text strong>{t('company:capacity_profile')}</Text>
                      </Space>
                    </Col>
                    <Col onClick={() => handleChangeTab(ETab.PRODUCT)}>
                      <Space
                        align='center'
                        className={`${styles.tabItem} ${active === ETab.PRODUCT && styles.active}`}
                        size={10}
                      >
                        <ProductIcon />
                        <Text strong>{t('company:product')}</Text>
                      </Space>
                    </Col>
                    <Col onClick={() => handleChangeTab(ETab.SERVICE)}>
                      <Space
                        align='center'
                        className={`${styles.tabItem} ${active === ETab.SERVICE && styles.active}`}
                        size={10}
                      >
                        <ServiceIcon />
                        <Text strong>{t('company:service')}</Text>
                      </Space>
                    </Col>

                    <Col className={`${styles.subTitle} ${y > 750 && styles.show} ellipsis oneline`}>
                      <Title level={2}>{name}</Title>
                    </Col>

                    {/* <Col onClick={() => setActive(4)}>
                      <Space align='center' className={`${styles.tabItem} ${active === 4 && styles.active}`} size={10}>
                        <CompanyTeam />
                        <Text strong>{t('company:company_team')}</Text>
                      </Space>
                    </Col>
                    <Col onClick={() => setActive(5)}>
                      <Space align='center' className={`${styles.tabItem} ${active === 5 && styles.active}`} size={10}>
                        <PostIcon />
                        <Text strong>{t('company:post')}</Text>
                      </Space>
                    </Col> */}
                  </Row>
                </Col>
                <Col span={24} className={`${styles.subTab} ${y > 750 && styles.show}`} />
              </Row>
            </Col>

            <Col span={24} className={styles.contentActive}>
              <Row gutter={[24, 24]} className={styles.wrapperActive}>
                <Col className='col-company left' span={24} md={{ span: 17 }}>
                  {active === ETab.GENERAL && (
                    <General
                      handleChangeTab={handleChangeTab}
                      detailCompany={detailCompany}
                      ListPortfolio={listPortfolio}
                      ListProduct={listProduct}
                      ListService={listService}
                    />
                  )}

                  {active === ETab.PORTFOLIO && <Profile idCompany={detailCompany._id} />}
                  {active === ETab.PRODUCT && <Product idCompany={detailCompany._id} />}
                  {active === ETab.SERVICE && <Service idCompany={detailCompany._id} />}
                </Col>

                <Col className='col-company right' span={24} md={{ span: 7 }}>
                  <Row gutter={[0, 24]}>
                    <Col span={24} className={styles.contact}>
                      <div className='card-company small'>
                        <div className='title-group'>
                          <Title className='title' level={4}>
                            {t('company:contact_info')}
                          </Title>
                        </div>
                        <Row gutter={[0, 12]} className={styles.content}>
                          <Col span={24}>
                            <Space align='center' size={16}>
                              <Image
                                src='/images/category-company/phoneicon.svg'
                                alt='phone icon'
                                width={32}
                                height={32}
                              />
                              {showInfo.phone ? (
                                <Text strong className={styles.text}>
                                  {detailCompany?.phone || t('updating')}
                                </Text>
                              ) : (
                                <Text
                                  onClick={() => setShowInfo({ ...showInfo, phone: true })}
                                  strong
                                  className={styles.link}
                                >
                                  {t('company:view_phone_number')}
                                </Text>
                              )}
                            </Space>
                          </Col>
                          <Col span={24}>
                            <Space align='center' size={16}>
                              <Image
                                src='/images/category-company/emailicon.svg'
                                alt='email icon'
                                width={32}
                                height={32}
                              />
                              {showInfo.email ? (
                                <Text strong className={styles.text}>
                                  {detailCompany?.email || t('updating')}
                                </Text>
                              ) : (
                                <Text
                                  onClick={() => setShowInfo({ ...showInfo, email: true })}
                                  strong
                                  className={styles.link}
                                >
                                  {t('company:view_email')}
                                </Text>
                              )}
                            </Space>
                          </Col>
                          <Col span={24}>
                            <Space align='center' size={16}>
                              <Image
                                src='/images/category-company/globalicon.svg'
                                alt='website icon'
                                width={32}
                                height={32}
                              />
                              {showInfo.website ? (
                                <Text strong className={styles.text}>
                                  {detailCompany?.website || t('updating')}
                                </Text>
                              ) : (
                                <Text
                                  onClick={() => setShowInfo({ ...showInfo, website: true })}
                                  strong
                                  className={styles.link}
                                >
                                  {t('company:view_website')}
                                </Text>
                              )}
                            </Space>
                          </Col>
                        </Row>
                        <Row gutter={[0, 8]} className={styles.share}>
                          <Col span={24}>
                            <Title level={4} className='title'>
                              {t('company:business_sharing')}
                            </Title>
                          </Col>
                          <Col span={24}>
                            <Space size={12}>
                              <FacebookShareButton
                                className={`${styles.linkShare} Demo__some-network__share-button`}
                                url={link}
                                hashtag='#Icongty'
                              >
                                <svg
                                  width='32'
                                  height='32'
                                  viewBox='0 0 32 32'
                                  fill='none'
                                  xmlns='http://www.w3.org/2000/svg'
                                >
                                  <path
                                    d='M13.868 17.384C13.817 17.384 12.695 17.384 12.185 17.384C11.913 17.384 11.828 17.282 11.828 17.027C11.828 16.347 11.828 15.65 11.828 14.97C11.828 14.698 11.93 14.613 12.185 14.613H13.868C13.868 14.562 13.868 13.576 13.868 13.117C13.868 12.437 13.987 11.791 14.327 11.196C14.684 10.584 15.194 10.176 15.84 9.93804C16.265 9.78504 16.69 9.71704 17.149 9.71704H18.815C19.053 9.71704 19.155 9.81904 19.155 10.057V11.995C19.155 12.233 19.053 12.335 18.815 12.335C18.356 12.335 17.897 12.335 17.438 12.352C16.979 12.352 16.741 12.573 16.741 13.049C16.724 13.559 16.741 14.052 16.741 14.579H18.713C18.985 14.579 19.087 14.681 19.087 14.953V17.01C19.087 17.282 19.002 17.367 18.713 17.367C18.101 17.367 16.792 17.367 16.741 17.367V22.909C16.741 23.198 16.656 23.3 16.35 23.3C15.636 23.3 14.939 23.3 14.225 23.3C13.97 23.3 13.868 23.198 13.868 22.943C13.868 21.158 13.868 17.435 13.868 17.384Z'
                                    fill='#2F61E6'
                                  />
                                  <rect x='0.5' y='0.5' width='31' height='31' rx='3.5' stroke='#2F61E6' />
                                </svg>
                              </FacebookShareButton>
                              <LinkedinShareButton title={name} className={styles.linkShare} url={link}>
                                <svg
                                  width='32'
                                  height='32'
                                  viewBox='0 0 32 32'
                                  fill='none'
                                  xmlns='http://www.w3.org/2000/svg'
                                >
                                  <path
                                    d='M22.3999 22.3995V17.7115C22.3999 15.4075 21.9039 13.6475 19.2159 13.6475C17.9199 13.6475 17.0559 14.3515 16.7039 15.0235H16.6719V13.8555H14.1279V22.3995H16.7839V18.1595C16.7839 17.0395 16.9919 15.9675 18.3679 15.9675C19.7279 15.9675 19.7439 17.2315 19.7439 18.2235V22.3835H22.3999V22.3995Z'
                                    fill='#2F61E6'
                                  />
                                  <path d='M9.80762 13.8557H12.4636V22.3997H9.80762V13.8557Z' fill='#2F61E6' />
                                  <path
                                    d='M11.1356 9.59961C10.2876 9.59961 9.59961 10.2876 9.59961 11.1356C9.59961 11.9836 10.2876 12.6876 11.1356 12.6876C11.9836 12.6876 12.6716 11.9836 12.6716 11.1356C12.6716 10.2876 11.9836 9.59961 11.1356 9.59961Z'
                                    fill='#2F61E6'
                                  />
                                  <rect x='0.5' y='0.5' width='31' height='31' rx='3.5' stroke='#2F61E6' />
                                </svg>
                              </LinkedinShareButton>
                              <TwitterShareButton
                                title={name}
                                className={styles.linkShare}
                                url={link}
                                hashtags={['Icongty']}
                              >
                                <svg
                                  width='32'
                                  height='32'
                                  viewBox='0 0 32 32'
                                  fill='none'
                                  xmlns='http://www.w3.org/2000/svg'
                                >
                                  <path
                                    d='M23.4283 11.3988C22.8818 11.6345 22.2872 11.8041 21.6747 11.8702C22.3106 11.4924 22.7868 10.8956 23.014 10.1916C22.4173 10.5466 21.7635 10.7955 21.0818 10.9274C20.7969 10.6228 20.4523 10.3801 20.0695 10.2145C19.6867 10.0489 19.2739 9.96393 18.8568 9.96485C17.1693 9.96485 15.8122 11.3327 15.8122 13.0113C15.8122 13.247 15.8408 13.4827 15.8872 13.7095C13.3604 13.5774 11.1068 12.3702 9.60862 10.522C9.33563 10.9883 9.19257 11.5192 9.19434 12.0595C9.19434 13.1166 9.73184 14.0488 10.5515 14.597C10.0685 14.578 9.59673 14.4452 9.17469 14.2095V14.247C9.17469 15.7274 10.2211 16.9541 11.6158 17.2363C11.3539 17.3043 11.0845 17.3391 10.814 17.3399C10.6158 17.3399 10.4283 17.3202 10.239 17.2934C10.6247 18.5006 11.7479 19.3774 13.0854 19.4059C12.039 20.2256 10.7283 20.7077 9.30505 20.7077C9.04969 20.7077 8.81398 20.6988 8.56934 20.6702C9.91934 21.5363 11.5211 22.0363 13.2461 22.0363C18.8461 22.0363 21.9104 17.397 21.9104 13.3702C21.9104 13.2381 21.9104 13.1059 21.9015 12.9738C22.4943 12.5399 23.014 12.0024 23.4283 11.3988Z'
                                    fill='#2F61E6'
                                  />
                                  <rect x='0.5' y='0.5' width='31' height='31' rx='3.5' stroke='#2F61E6' />
                                </svg>
                              </TwitterShareButton>
                            </Space>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    {listRelatedCompany?.length > 0 && (
                      <Col className={styles.company} span={24}>
                        <div className='card-company small'>
                          <div className='title-group'>
                            <Title className='title' level={4}>
                              {t('company:related_company')}
                            </Title>
                          </div>
                          <Row className={styles.content}>
                            {listRelatedCompany?.map((company) => (
                              <Col key={company._id} className={styles.companyItem} span={24}>
                                <RelatedCompany data={company} />
                              </Col>
                            ))}
                          </Row>
                        </div>
                      </Col>
                    )}

                    {srcAds && (
                      <Col span={24} md={{ span: 24 }} className='adsWrapCompany'>
                        <Image src={srcAds} alt='ads' className='ads' fill />
                      </Col>
                    )}
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}
