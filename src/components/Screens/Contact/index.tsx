/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/no-unescaped-entities */
import { Button, Col, Form, Input, Row, Space, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { useRouter } from 'next/router';
import PhoneInput from 'react-phone-input-2';

import BreadcrumbPage from '@/components/Elements/Breadcrumb';
import { TOption, TPostContact } from '@/modules';
import { useMutationPostContact } from '@/queries/hooks';
import { regexEmail, regexPhoneNumber } from '@/utils/regex';

import styles from './style.module.less';

const { Title, Text } = Typography;
const { TextArea } = Input;

interface IProps {
  optionContact: TOption<any>;
  optionGeneral: TOption<any>;
}

function ContactScreen({ optionContact, optionGeneral }: IProps) {
  const { t } = useTranslation('contact');
  const router = useRouter();
  const { mutate: postContact } = useMutationPostContact();
  const [form] = Form.useForm();

  const onFinish = (values: TPostContact) => {
    postContact(
      { ...values, phone: `+${values.phone}` },
      {
        onSuccess: () => {
          form.resetFields();
        },
      },
    );
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.background}>
        <Image className={styles.img} src='/images/contact/backgroundx4.jpg' alt='background' fill />
        <Image className={styles.imgTablet} src='/images/contact/backgroundtabletx4.jpg' alt='background' fill />
        <div className='home-container'>
          <div className='home-wrapper'>
            <Row gutter={[24, 0]} className={styles.bgWrap}>
              <Col span={24} className={styles.breadcrumb}>
                <BreadcrumbPage router={router} />
              </Col>
              <Col span={24}>
                <Row>
                  <Col span={24} className={styles.title}>
                    <Title level={1}>
                      <Text>{t('we_can')}</Text>
                      <Text>{t('help_you_quickly')}</Text>
                    </Title>
                  </Col>
                  <Col span={24}>
                    <Row gutter={[24, 18]} className={styles.statistical}>
                      <Col span={24} sm={{ span: 8 }} lg={{ span: 7 }} xl={{ span: 6 }}>
                        <div className={styles.line}>
                          <Image
                            className={styles.lineIcon}
                            src='/images/contact/location.svg'
                            width={40}
                            height={40}
                            alt='icon'
                          />
                        </div>
                        <Title className={`${styles.title} ellipsis oneline`} level={3}>
                          {t('address')}
                        </Title>
                        <Text className={`${styles.desc} ellipsis threeline`}>{optionGeneral?.contact?.address}</Text>
                      </Col>
                      <Col span={24} sm={{ span: 8 }} lg={{ span: 7 }} xl={{ span: 6 }}>
                        <div className={styles.line}>
                          <Image
                            className={styles.lineIcon}
                            src='/images/contact/phone.svg'
                            width={40}
                            height={40}
                            alt='icon'
                          />
                        </div>
                        <Title className={`${styles.title} ellipsis oneline`} level={3}>
                          {t('phone_number')}
                        </Title>
                        <Text className={`${styles.desc} ellipsis threeline`}>
                          <Space wrap className={styles.space} size={8}>
                            <Text>{optionGeneral?.contact?.phone}</Text>
                            <a target='_blank' rel='noreferrer' href={`tel:${optionGeneral?.contact?.phone}`}>
                              <strong>{t('call')}</strong>
                            </a>
                          </Space>
                        </Text>
                      </Col>
                      <Col span={24} sm={{ span: 6 }} lg={{ span: 5 }} xl={{ span: 5 }}>
                        <div className={`${styles.line} ${styles.last}`}>
                          <Image
                            className={styles.lineIcon}
                            src='/images/contact/email.svg'
                            width={40}
                            height={40}
                            alt='icon'
                          />
                        </div>
                        <Title className={`${styles.title} ellipsis oneline`} level={3}>
                          Email
                        </Title>
                        <Text className={`${styles.desc} ellipsis threeline`}>
                          <Space wrap className={styles.space} size={8}>
                            <Text>{optionGeneral?.contact?.email}</Text>
                            <a target='_blank' rel='noreferrer' href={`mailto:${optionGeneral?.contact?.email}`}>
                              <strong>{t('send_mail')}</strong>
                            </a>
                          </Space>
                        </Text>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>
      </div>
      <div className='home-container'>
        <div className='home-wrapper'>
          <Row gutter={[12, 0]} className={styles.content}>
            <Col span={24} lg={{ span: 12 }} className={styles.formGroup}>
              <Col span={24} xl={{ span: 20 }} lg={{ span: 22 }}>
                <Title className={styles.title} level={2}>
                  {t('connect_with_us')}
                </Title>
                <Form form={form} onFinish={onFinish} className={styles.form} layout='vertical'>
                  <Row gutter={[24, 0]}>
                    <Col span={12}>
                      <Form.Item
                        rules={[{ required: true, message: t('please_enter_your_name') || 'Vui lòng nhập họ tên' }]}
                        label={t('full_name')}
                        name='name'
                      >
                        <Input placeholder={t('enter_your_full_name') || ''} />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        rules={[
                          {
                            required: true,
                            message: t('please_enter_your_phone_number') || 'Vui lòng nhập số điện thoại',
                          },
                          { pattern: regexPhoneNumber, message: t('invalid_phone') || 'Số điện thoại không hợp lệ' },
                        ]}
                        label={t('phone_number')}
                        name='phone'
                      >
                        <PhoneInput
                          inputClass='phone-input'
                          buttonClass='phone-input-button'
                          country='vn'
                          enableSearch
                          containerStyle={{ width: '100%' }}
                          inputStyle={{ width: '100%' }}
                          placeholder={t('enter_your_phone') || ''}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        rules={[
                          {
                            required: true,
                            message: t('please_enter_your_email') || 'Vui lòng nhập email',
                          },
                          { pattern: regexEmail, message: t('invalid_email') || 'Email không hợp lệ' },
                        ]}
                        label='Email'
                        name='email'
                      >
                        <Input placeholder={t('enter_your_email') || ''} />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item label='Doanh nghiệp' name='companyName'>
                        <Input placeholder={t('enter_enterprise') || ''} />
                      </Form.Item>
                    </Col>
                    <Col className={styles.note} span={24}>
                      <Form.Item
                        rules={[
                          {
                            required: true,
                            message: t('please_enter_message') || 'Vui lòng nhập lời nhắn',
                          },
                        ]}
                        label={t('message')}
                        name='message'
                      >
                        <TextArea rows={4} placeholder={t('message_textarea') || ''} />
                      </Form.Item>
                    </Col>
                    <Col span={24}>
                      <Form.Item>
                        <Button htmlType='submit' className={styles.btn} type='primary'>
                          {t('send_message')}
                        </Button>
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Col>
            <Col span={24} lg={{ span: 12 }} className={styles.map}>
              <iframe
                title='map'
                width='100%'
                height='600'
                src={`https://maps.google.com/maps?q=${optionContact?.value?.latitude},${optionContact?.value?.longitude}&hl=es;z=14&amp&output=embed`}
              />
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}

export default ContactScreen;
