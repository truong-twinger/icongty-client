import { Col, Row, Space, Typography } from 'antd';
import Image from 'next/image';
import Link from 'next/link';
import { useMemo } from 'react';
import { useRouter } from 'next/router';

import { TCompany } from '@/modules';
import { ELanguage } from '@/configs/interface.config';

import styles from './style.module.less';

const { Title, Text } = Typography;

interface IProps {
  data: TCompany;
}

export default function RelatedCompany({ data }: IProps) {
  const router = useRouter();
  const name = useMemo(
    () => (router.locale === ELanguage.EN ? data?.internationalName || data?.name : data?.name),
    [router],
  );
  return (
    <Link passHref href={`/company/${data.slug}`}>
      <Row align='top' gutter={[16, 0]} className={`${styles.wrapper} hoverItem`}>
        <Col>
          <Col className={styles.logoWrap}>
            {data?.logo ? (
              <Image alt={name} src={data?.logo?.location} className={styles.logo} fill sizes='30vw' />
            ) : (
              <Image
                alt='logo image company'
                className={styles.logo}
                src='/images/category-company/logocompany.jpg'
                fill
                sizes='30vw'
              />
            )}
          </Col>
        </Col>
        <Col>
          <Row className={styles.content} gutter={[0, 8]}>
            <Col span={24}>
              <Title className={`${styles.title} ellipsis twoline`} level={3}>
                {name}
              </Title>
            </Col>
            <Col span={24}>
              <Space size={8} className={styles.tagWrap}>
                {data?.categories?.map(
                  (category, index) =>
                    index < 1 && (
                      <Col key={category._id} className={`tag ellipsis oneline ${styles.tag}`}>
                        {category?.translation?.[router.locale as ELanguage]?.name}
                      </Col>
                    ),
                )}
                {data?.categories?.length > 1 && (
                  <Col className={`tag ellipsis oneline ${styles.tag}`}>+{(data?.categories?.length || 2) - 1}</Col>
                )}
                {data?.categories?.length === 0 && <Text>--</Text>}
              </Space>
            </Col>
          </Row>
        </Col>
      </Row>
    </Link>
  );
}
