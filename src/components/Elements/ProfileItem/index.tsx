/* eslint-disable @typescript-eslint/no-explicit-any */
import { Badge, Button, Col, Row, Typography, Tooltip, Space } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { FacebookShareButton, LinkedinShareButton, TwitterShareButton } from 'react-share';
import { useRouter } from 'next/router';

import { getBlurDataUrl } from '@/libs/common';
import { BASE_URL } from '@/configs/env.config';
import { TPortfolio } from '@/modules';

import styles from './style.module.less';

const { Title, Text } = Typography;

interface IProps {
  data: TPortfolio;
}

function ToolTipp({ linkk, name }: { linkk: string; name: string }) {
  return (
    <Space size={12}>
      <FacebookShareButton
        className={`${styles.linkShare} Demo__some-network__share-button`}
        url={linkk}
        hashtag='#Icongty'
      >
        <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <path
            d='M13.868 17.384C13.817 17.384 12.695 17.384 12.185 17.384C11.913 17.384 11.828 17.282 11.828 17.027C11.828 16.347 11.828 15.65 11.828 14.97C11.828 14.698 11.93 14.613 12.185 14.613H13.868C13.868 14.562 13.868 13.576 13.868 13.117C13.868 12.437 13.987 11.791 14.327 11.196C14.684 10.584 15.194 10.176 15.84 9.93804C16.265 9.78504 16.69 9.71704 17.149 9.71704H18.815C19.053 9.71704 19.155 9.81904 19.155 10.057V11.995C19.155 12.233 19.053 12.335 18.815 12.335C18.356 12.335 17.897 12.335 17.438 12.352C16.979 12.352 16.741 12.573 16.741 13.049C16.724 13.559 16.741 14.052 16.741 14.579H18.713C18.985 14.579 19.087 14.681 19.087 14.953V17.01C19.087 17.282 19.002 17.367 18.713 17.367C18.101 17.367 16.792 17.367 16.741 17.367V22.909C16.741 23.198 16.656 23.3 16.35 23.3C15.636 23.3 14.939 23.3 14.225 23.3C13.97 23.3 13.868 23.198 13.868 22.943C13.868 21.158 13.868 17.435 13.868 17.384Z'
            fill='#2F61E6'
          />
          <rect x='0.5' y='0.5' width='31' height='31' rx='3.5' stroke='#2F61E6' />
        </svg>
      </FacebookShareButton>
      <LinkedinShareButton title={name} className={styles.linkShare} url={linkk}>
        <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <path
            d='M22.3999 22.3995V17.7115C22.3999 15.4075 21.9039 13.6475 19.2159 13.6475C17.9199 13.6475 17.0559 14.3515 16.7039 15.0235H16.6719V13.8555H14.1279V22.3995H16.7839V18.1595C16.7839 17.0395 16.9919 15.9675 18.3679 15.9675C19.7279 15.9675 19.7439 17.2315 19.7439 18.2235V22.3835H22.3999V22.3995Z'
            fill='#2F61E6'
          />
          <path d='M9.80762 13.8557H12.4636V22.3997H9.80762V13.8557Z' fill='#2F61E6' />
          <path
            d='M11.1356 9.59961C10.2876 9.59961 9.59961 10.2876 9.59961 11.1356C9.59961 11.9836 10.2876 12.6876 11.1356 12.6876C11.9836 12.6876 12.6716 11.9836 12.6716 11.1356C12.6716 10.2876 11.9836 9.59961 11.1356 9.59961Z'
            fill='#2F61E6'
          />
          <rect x='0.5' y='0.5' width='31' height='31' rx='3.5' stroke='#2F61E6' />
        </svg>
      </LinkedinShareButton>
      <TwitterShareButton title={name} className={styles.linkShare} url={linkk} hashtags={['Icongty']}>
        <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <path
            d='M23.4283 11.3988C22.8818 11.6345 22.2872 11.8041 21.6747 11.8702C22.3106 11.4924 22.7868 10.8956 23.014 10.1916C22.4173 10.5466 21.7635 10.7955 21.0818 10.9274C20.7969 10.6228 20.4523 10.3801 20.0695 10.2145C19.6867 10.0489 19.2739 9.96393 18.8568 9.96485C17.1693 9.96485 15.8122 11.3327 15.8122 13.0113C15.8122 13.247 15.8408 13.4827 15.8872 13.7095C13.3604 13.5774 11.1068 12.3702 9.60862 10.522C9.33563 10.9883 9.19257 11.5192 9.19434 12.0595C9.19434 13.1166 9.73184 14.0488 10.5515 14.597C10.0685 14.578 9.59673 14.4452 9.17469 14.2095V14.247C9.17469 15.7274 10.2211 16.9541 11.6158 17.2363C11.3539 17.3043 11.0845 17.3391 10.814 17.3399C10.6158 17.3399 10.4283 17.3202 10.239 17.2934C10.6247 18.5006 11.7479 19.3774 13.0854 19.4059C12.039 20.2256 10.7283 20.7077 9.30505 20.7077C9.04969 20.7077 8.81398 20.6988 8.56934 20.6702C9.91934 21.5363 11.5211 22.0363 13.2461 22.0363C18.8461 22.0363 21.9104 17.397 21.9104 13.3702C21.9104 13.2381 21.9104 13.1059 21.9015 12.9738C22.4943 12.5399 23.014 12.0024 23.4283 11.3988Z'
            fill='#2F61E6'
          />
          <rect x='0.5' y='0.5' width='31' height='31' rx='3.5' stroke='#2F61E6' />
        </svg>
      </TwitterShareButton>
    </Space>
  );
}

export default function ProfileItem({ data }: IProps) {
  const { t } = useTranslation();
  const router = useRouter();
  const link = `${BASE_URL}${router.asPath.slice(1)}`;
  return (
    <Row className={styles.wrapper}>
      <Col span={24} xl={{ span: 20 }} className={styles.contentRow}>
        <Row gutter={[{ xs: 12, sm: 24 }, 0]} className={styles.thumbnailWrap}>
          <Col className={styles.thumbnail}>
            <Badge.Ribbon
              className={`${!data?.document?.location && styles.hide}`}
              color='red'
              placement='start'
              text={data?.document?.extension?.slice(1).toUpperCase()}
            >
              {data?.thumbnail ? (
                <div className={styles.logoWrap}>
                  <Image
                    placeholder='blur'
                    blurDataURL={getBlurDataUrl(data.thumbnail.width || 256, data.thumbnail.height || 341)}
                    alt={data.name}
                    src={data?.thumbnail?.location}
                    className={styles.thumbnail}
                    width={93}
                    height={130}
                    sizes='30vw'
                  />
                </div>
              ) : (
                <Image
                  alt='logo image company'
                  className={styles.logo}
                  src='/images/category-company/logocompany.jpg'
                  width={93}
                  height={130}
                  sizes='30vw'
                />
              )}
            </Badge.Ribbon>
          </Col>
          <Col>
            <Row>
              <Col span={24}>
                <Title level={3} className={`${styles.title} ellipsis twoline`}>
                  {data?.name}
                </Title>
              </Col>
              <Col span={24}>
                <Text className={`${styles.desc} ellipsis twoline`}>{data?.excerpt || '--'}</Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
      <Col span={24} xl={{ span: 4 }} className={styles.action}>
        <Row gutter={[0, 4]}>
          <Col xl={{ span: 24 }}>
            <a target='_blank' rel='noreferrer' href={data?.document?.location || '/'}>
              <Button disabled={!data?.document?.location} className='btn-company' size='small' type='primary' ghost>
                <strong>{t('company:download')}</strong>
              </Button>
            </a>
          </Col>
          <Col xl={{ span: 24 }}>
            <Tooltip
              trigger='click'
              color='#fff'
              placement='bottom'
              title={<ToolTipp name={data?.name || ''} linkk={link} />}
            >
              <Button className={`btn-company ${styles.btnShare}`} size='small' type='link' ghost>
                <strong>{t('company:share')}</strong>
              </Button>
            </Tooltip>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
