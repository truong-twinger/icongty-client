export default function Loading() {
  return (
    <div className='wrapperLoading'>
      <span className='loaderLoading' />
    </div>
  );
}
