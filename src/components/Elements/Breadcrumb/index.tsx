/* eslint-disable @typescript-eslint/no-explicit-any */
import { Breadcrumb } from 'antd';
import Link from 'next/link';
import { NextRouter } from 'next/router';
import { useTranslation } from 'next-i18next';

import logger from '@libs/logger';

interface Props {
  router: NextRouter;
  detailName?: string;
}
interface Route {
  path: string;
  breadcrumbName: string;
  children?: Omit<Route, 'children'>[];
}

function BreadcrumbPage({ router, detailName }: Props) {
  // Create mảng breadcrumb
  const { t } = useTranslation();
  const pathWithoutQuery = router.pathname.split('?')[0];
  let pathArray = pathWithoutQuery.split('/');
  pathArray = pathArray.filter((path: string) => path !== '');
  const _breadcrumbs = pathArray
    .map((path: string, index: number) => {
      const href = `/${pathArray.slice(0, index + 1).join('/')}`;

      return {
        path: href,
        breadcrumbName: path,
        prePath: path,
      };
    })
    .filter((d: any) => d.prePath.indexOf('[') === -1);

  // Trang detail
  if (detailName) {
    _breadcrumbs.push({ path: '/', breadcrumbName: detailName, prePath: '/' });
  }

  // Thêm home page
  _breadcrumbs.unshift({ path: '/', breadcrumbName: 'home_page', prePath: '/' });

  // Render
  const itemRender = (route: Route, params: any, routes: Route[]) => {
    const last = routes?.indexOf(route) === routes.length - 1;
    logger.debug('🚀 ~ Params BreadcrumbPage ~ ', params);

    return last ? <span>{t(route.breadcrumbName)}</span> : <Link href={route.path}>{t(route.breadcrumbName)}</Link>;
  };

  return <Breadcrumb itemRender={itemRender} routes={_breadcrumbs} />;
}

export default BreadcrumbPage;
