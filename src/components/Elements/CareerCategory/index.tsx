import { Col, Row, Typography } from 'antd';
import Link from 'next/link';
import React from 'react';
import { useRouter } from 'next/router';

import { TCategory } from '@/modules';

import styles from './style.module.less';

const { Title } = Typography;

interface IProps {
  data: TCategory;
}

export default function CareerCategory({ data }: IProps) {
  const router = useRouter();
  return (
    <Link
      passHref
      href={{
        pathname: '/company',
        query: {
          ...router.query,
          'categoryIds[]': data._id,
        },
      }}
    >
      <Row className={`${styles.wrapper} hoverItem`}>
        <Col span={24} className={styles.content}>
          <Title className={`${styles.title} ellipsis oneline`} level={3}>
            {data.name}
          </Title>
          {/* <Text className={styles.amount}>{formatNumber.format(amount)}</Text> */}
        </Col>
      </Row>
    </Link>
  );
}
