import { Col, Row, Space, Tag, Typography } from 'antd';
import moment from 'moment';
import Image from 'next/image';
// eslint-disable-next-line import/order
import Link from 'next/link';

// eslint-disable-next-line import/no-cycle

import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';

import { getBlurDataUrl } from '@/libs/common';
import { TPost } from '@/modules';
import { ELanguage } from '@/configs/interface.config';

import styles from './style.module.less';

const { Title, Text } = Typography;
export interface IHotNewsProps {
  listAllNew: TPost[];
}
function HotNews({ listAllNew }: IHotNewsProps) {
  const router = useRouter();
  const { t } = useTranslation();

  return (
    <Col span={24} className={`${styles.listNews}`}>
      <Row gutter={[0, { xs: 35, sm: 35, md: 60, lg: 60, xl: 80 }]}>
        <Col span={24}>
          <Link href={`/news/${listAllNew[0]?.slug ? listAllNew[0]?.slug : '/#'}`}>
            <Row
              gutter={[
                { xs: 0, sm: 30, lg: 30, xl: 80 },
                { xs: 20, lg: 0 },
              ]}
              align='middle'
            >
              <Col span={14} xs={{ span: 24 }} md={{ span: 14 }}>
                <Row>
                  <Col className={styles.thumbnailWrap}>
                    {listAllNew[0] && (
                      <Image
                        className={styles.thumbnail}
                        placeholder='blur'
                        blurDataURL={getBlurDataUrl(256, 341)}
                        alt={listAllNew[0]?.name}
                        src={listAllNew[0].thumbnail?.location}
                        fill
                        sizes='(max-width: 768px) 100vw,(max-width: 1200px) 50vw, 50vw'
                      />
                    )}
                  </Col>
                </Row>
              </Col>

              <Col span={10} xs={{ span: 24 }} md={{ span: 10 }}>
                <Row className={styles.content} gutter={[0, 16]}>
                  <Col span={24}>
                    {listAllNew[0] && (
                      <Space wrap size={2}>
                        {listAllNew[0].categories.map((category) => (
                          <Tag color='blue' style={{ fontWeight: '400', fontSize: '14px' }} key={category._id}>
                            {category?.translation?.[router.locale as ELanguage]?.name}
                          </Tag>
                        ))}
                      </Space>
                    )}
                  </Col>
                  <Col>
                    <Row gutter={[0, 16]}>
                      <Col span={24}>
                        <Row gutter={[0, { xs: 13, sm: 20, xl: 32 }]}>
                          <Col span={24}>
                            <Title className={`${styles.title} ellipsis threeline`} level={2}>
                              {listAllNew[0].name}
                            </Title>
                          </Col>
                          <Col span={24}>
                            <Text className={`${styles.text} ellipsis threeline`}>{listAllNew[0].excerpt}</Text>
                          </Col>
                        </Row>
                      </Col>
                      <Col span={24}>
                        <Row gutter={[16, 0]} className={styles._source}>
                          {listAllNew[0]?.source?.name && (
                            <>
                              <Col>
                                {t('according_to')} <strong>{listAllNew[0]?.source?.name}</strong>
                              </Col>
                              <Col> | </Col>
                            </>
                          )}

                          <Col>{moment(listAllNew[0].createdAt).format('DD/MM/YYYY')}</Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Link>
        </Col>
        {listAllNew.length > 1 && (
          <Col span={24}>
            <Row gutter={[{ xs: 20, sm: 30, md: 40, xl: 80 }, 60]}>
              {listAllNew.map((value, key) => {
                if (key > 0) {
                  return (
                    <Col
                      span={8}
                      xs={{ span: 24 }}
                      sm={{ span: 12 }}
                      lg={{ span: 8 }}
                      className={styles._wrappBlock}
                      key={value._id}
                    >
                      <Link href={`/news/${value?.slug ? value?.slug : '/#'}`} className={styles._block}>
                        <Row gutter={[0, { xs: 20, sm: 30, md: 30, xl: 32 }]}>
                          <Col className={styles.BlockWrap}>
                            <Image
                              className={styles.thumbnail}
                              placeholder='blur'
                              blurDataURL={getBlurDataUrl(256, 341)}
                              alt='Tin nổi bật'
                              src={value?.thumbnail?.location || '/images/category-company/logocompany.jpg'}
                              fill
                              sizes='(max-width: 768px) 100vw'
                            />
                          </Col>
                          <Col span={24}>
                            <Row gutter={[0, 8]}>
                              <Col span={24}>
                                <Space wrap size={2}>
                                  {value?.categories?.map(
                                    (category, index) =>
                                      index < 1 && (
                                        <Tag color='blue' key={category._id}>
                                          {category?.translation?.[router.locale as ELanguage]?.name}
                                        </Tag>
                                      ),
                                  )}
                                  {value?.categories?.length > 1 && (
                                    <Tag color='blue'>+{(value?.categories?.length || 1) - 1}</Tag>
                                  )}
                                </Space>
                              </Col>
                              <Col span={24}>
                                <Row>
                                  <Col span={24}>
                                    <Title className={`${styles.titleBlock} ellipsis twoline`} level={3}>
                                      {value.name}
                                    </Title>
                                  </Col>
                                  <Col span={24}>
                                    <Row gutter={[16, 0]} className={styles._sourceBottom}>
                                      {value?.source?.name && (
                                        <>
                                          <Col>
                                            {t('according_to')} <strong>{value?.source?.name}</strong>
                                          </Col>
                                          <Col> | </Col>
                                        </>
                                      )}

                                      <Col>{moment(value.createdAt).format('DD/MM/YYYY')}</Col>
                                    </Row>
                                  </Col>
                                  <Col span={24}>
                                    <Text className={`${styles.desBlock} ellipsis threeline`}>{value.excerpt}</Text>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Link>
                    </Col>
                  );
                }
                return false;
              })}
            </Row>
          </Col>
        )}
      </Row>
    </Col>
  );
}

export default HotNews;
