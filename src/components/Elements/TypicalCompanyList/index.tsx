/* eslint-disable react/jsx-one-expression-per-line */
import { CopyOutlined } from '@ant-design/icons';
import { Badge, Col, message, Row, Space, Typography } from 'antd';
import Image from 'next/image';
import Link from 'next/link';
import { useMemo } from 'react';
import { useRouter } from 'next/router';

import { TCompany } from '@/modules';
import { ELanguage } from '@/configs/interface.config';

import styles from './style.module.less';

const { Text, Title } = Typography;

interface IProps {
  data: TCompany;
}

function Tag() {
  return (
    <Space size={6} align='center'>
      <svg width='14' height='14' viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
        <path
          d='M6.33965 10.3076L5.10149 11.5412C4.58965 12.0531 3.75396 12.0531 3.24212 11.5412L2.00396 10.3031C1.49639 9.79572 1.49639 8.96002 2.00396 8.44818L3.24212 7.21002L6.33965 10.3076ZM12.2151 6.89941L6.64577 1.33432C6.54964 1.2337 6.40951 1.18564 6.27386 1.20743C6.13393 1.22943 6.0158 1.31253 5.95022 1.43494L3.44335 6.1731L7.38512 10.1151L12.11 7.59497C12.2327 7.53387 12.316 7.41125 12.3378 7.2756C12.3596 7.13568 12.3113 7.00003 12.2151 6.89941ZM2.62518 12.1582L1.38702 10.92C1.21206 10.7451 1.07641 10.5526 0.975791 10.3426C0.757041 10.955 0.896965 11.6681 1.38702 12.1582C1.71963 12.4906 2.15713 12.6745 2.62518 12.6745C2.82641 12.6745 3.02337 12.6395 3.20708 12.5739C2.99261 12.4731 2.79586 12.3331 2.62518 12.1582ZM7.73077 11.6944L6.96087 10.9245L5.71822 12.1624C5.61333 12.2675 5.49947 12.3594 5.37707 12.4382L6.182 13.2432C6.38772 13.4489 6.66329 13.5625 6.95638 13.5625C7.24947 13.5625 7.52526 13.4489 7.73077 13.2432C8.15502 12.8144 8.15502 12.1231 7.73077 11.6944ZM9.18768 1.75024V0.875244C9.18768 0.633636 8.992 0.437744 8.75018 0.437744C8.50835 0.437744 8.31268 0.633636 8.31268 0.875244V1.75024C8.31268 1.99185 8.50835 2.18774 8.75018 2.18774C8.992 2.18774 9.18768 1.99185 9.18768 1.75024ZM13.1252 4.81253C13.1252 4.57092 12.9295 4.37503 12.6877 4.37503H11.8127C11.5709 4.37503 11.3752 4.57092 11.3752 4.81253C11.3752 5.05414 11.5709 5.25003 11.8127 5.25003H12.6877C12.9295 5.25003 13.1252 5.05414 13.1252 4.81253ZM10.8095 3.37186L11.6845 2.49686C11.8554 2.32596 11.8554 2.0491 11.6845 1.8782C11.5136 1.70731 11.2367 1.70731 11.0658 1.8782L10.1908 2.7532C10.02 2.9241 10.02 3.20096 10.1908 3.37186C10.2763 3.45731 10.3882 3.50003 10.5002 3.50003C10.6121 3.50003 10.7241 3.45731 10.8095 3.37186Z'
          fill='white'
        />
      </svg>
      <Text>Được tài trợ</Text>
    </Space>
  );
}

export default function TypicalCompanyList({ data }: IProps) {
  const handleCopy = (e: { preventDefault: () => void }, code: string) => {
    e.preventDefault();
    navigator.clipboard
      .writeText(code)
      .then(() => {
        message.info(`Copied ${code}`);
      })
      .catch(() => {
        message.error('Error!!');
      });
  };

  const router = useRouter();
  const name = useMemo(
    () => (router.locale === ELanguage.EN ? data?.internationalName || data?.name : data?.name),
    [router],
  );

  return (
    <Link passHref href={`/company/${data.slug}`}>
      <Badge.Ribbon className={`${styles.tag} ${!data?.isSupported && styles.hide}`} color='#FA8C16' text={<Tag />}>
        <Col span={24} className={`${styles.wrapper} hoverItem`}>
          <Row gutter={[{ xs: 12, sm: 24 }, 0]} style={{ alignItems: 'center' }}>
            <Col span={24} sm={{ span: 16 }} md={{ span: 14 }} lg={{ span: 12 }}>
              <Row className={styles.titleGroup}>
                <Col className={styles.logoWrap}>
                  {data?.logo ? (
                    <Image
                      alt={name}
                      src={data?.logo?.location}
                      className={styles.logo}
                      width={68}
                      height={68}
                      sizes='30vw'
                    />
                  ) : (
                    <Image
                      alt='logo image company'
                      className={styles.logo}
                      src='/images/category-company/logocompany.jpg'
                      fill
                      sizes='30vw'
                    />
                  )}
                </Col>
                <Col className={styles.col}>
                  <Title level={3} className={`${styles.title} ellipsis oneline`}>
                    {name}
                  </Title>
                </Col>
              </Row>
            </Col>
            <Col span={0} lg={{ span: 4 }} className={styles.col}>
              {data?.taxCode ? (
                <Space size={4} align='center'>
                  <Text className={styles.text}>{data.taxCode}</Text>
                  <CopyOutlined onClick={(event) => handleCopy(event, data.taxCode)} className={styles.copy} />
                </Space>
              ) : (
                <Text className={styles.empty}>--</Text>
              )}
            </Col>
            <Col span={0} md={{ span: 5 }} lg={{ span: 4 }} className={styles.col}>
              {data?.categories?.length > 0 ? (
                <Space size={8}>
                  {data?.categories?.map(
                    (category, index) =>
                      index < 1 && (
                        <div key={category._id} className='tag ellipsis oneline'>
                          {category?.translation?.[router.locale as ELanguage]?.name}
                        </div>
                      ),
                  )}
                  {data?.categories?.length > 1 && (
                    <div className='tag ellipsis oneline'>+{(data?.categories?.length || 1) - 1}</div>
                  )}
                </Space>
              ) : (
                <Text className={styles.empty}>--</Text>
              )}
            </Col>
            <Col span={0} sm={{ span: 8 }} md={{ span: 5 }} lg={{ span: 4 }} className={styles.col}>
              {data?.city ? (
                <Space size={8} className={styles.location}>
                  <Image alt='icon' className={styles.icon} src='/images/home/position.svg' width={12} height={11.25} />
                  <Text className={`${styles.content} ellipsis oneline`}>{data.city.name}</Text>
                </Space>
              ) : (
                <Text className={`${styles.emptyLocation} ${styles.location}`}>--</Text>
              )}
            </Col>
          </Row>
        </Col>
      </Badge.Ribbon>
    </Link>
  );
}
