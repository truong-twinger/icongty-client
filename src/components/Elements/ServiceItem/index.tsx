/* eslint-disable @typescript-eslint/no-explicit-any */
import { Col, Row, Typography } from 'antd';
import Image from 'next/image';

import { getBlurDataUrl } from '@/libs/common';

import styles from './style.module.less';

const { Title, Text } = Typography;

interface IProps {
  data: any;
  index: number;
}

export default function ServiceItem({ index, data }: IProps) {
  return (
    <Row className={styles.wrapper}>
      <Col lg={{ order: 0 }} order={1} className={styles.index}>
        <Text strong>{index}.</Text>
      </Col>
      <Col lg={{ order: 0 }} order={3} className={styles.content}>
        <Row>
          <Col span={24} className={styles.title}>
            <Title className='ellipsis twoline' level={3}>
              {data?.name}
            </Title>
          </Col>
          <Col span={24} className={styles.description}>
            <Text className='ellipsis fourline'>{data?.excerpt}</Text>
          </Col>
          {/* <Col span={24} className={styles.discover}>
            <Link href='/'>
              <strong>{t('company:find_out_more')}</strong>
            </Link>
          </Col> */}
        </Row>
      </Col>
      <Col lg={{ order: 0 }} order={2} className={styles.thumbnailWrap}>
        {data?.logo ? (
          <div className={styles.logoWrap}>
            <Image
              placeholder='blur'
              blurDataURL={getBlurDataUrl(data.logo.width || 256, data.logo.height || 341)}
              alt={data.name}
              src={data?.logo?.location}
              className={styles.thumbnail}
              width={210}
              height={140}
              sizes='30vw'
            />
          </div>
        ) : (
          <Image
            alt='logo image company'
            className={styles.thumbnail}
            src='/images/category-company/logocompany.jpg'
            width={210}
            height={140}
            sizes='30vw'
          />
        )}
      </Col>
    </Row>
  );
}
