/* eslint-disable @typescript-eslint/no-explicit-any */
import { SearchOutlined } from '@ant-design/icons';
import { Col, Input, Row } from 'antd';
import { useTranslation } from 'next-i18next';
import React from 'react';

import styles from './style.module.less';

const { Search } = Input;

interface Props {
  onSearch: (value: string) => void;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  placeholder?: string;
}

const SearchInput = React.forwardRef<HTMLInputElement, Props>(({ onSearch, onChange, placeholder }, ref: any) => {
  const { t } = useTranslation();
  return (
    <Row>
      <Col className='home-search'>
        <Search
          ref={ref}
          placeholder={placeholder || t('home:placeholder_search') || 'Nhập tên công ty, mã số thuế, ngành nghề...'}
          enterButton={<SearchOutlined />}
          className={styles.search}
          onSearch={onSearch}
          onChange={onChange}
        />
      </Col>
    </Row>
  );
});

export default SearchInput;
