import { DownOutlined } from '@ant-design/icons';
import { Col, Row, Select } from 'antd';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';

import { EOrder, EOrderBy } from '@/configs/interface.config';

import styles from './style.module.less';

function ShoterComponent() {
  const { t } = useTranslation('news');
  const router = useRouter();
  const handleChange = (value: string) => {
    switch (value) {
      case 'case1':
        // code block

        router.replace({
          pathname: router.pathname,
          query: {
            ...router.query,

            order: EOrder.DESC,
            orderBy: EOrderBy.CREATED_DATE,
          },
        });
        break;
      case 'case2':
        router.replace({
          pathname: router.pathname,
          query: {
            ...router.query,

            order: EOrder.ASC,
            orderBy: EOrderBy.CREATED_DATE,
          },
        });
        break;
      case 'case3':
        router.replace({
          pathname: router.pathname,
          query: {
            ...router.query,

            order: EOrder.DESC,
            orderBy: EOrderBy.NAME,
          },
        });
        break;
      case 'case4':
        router.replace({
          pathname: router.pathname,
          query: {
            ...router.query,

            order: EOrder.ASC,
            orderBy: EOrderBy.NAME,
          },
        });
        break;

      case 'case5':
        router.replace({
          pathname: router.pathname,
          query: {
            ...router.query,

            order: EOrder.DESC,
            orderBy: EOrderBy.VIEWER,
          },
        });
        break;

      default:
        router.replace({
          pathname: router.pathname,
          query: {
            ...router.query,

            order: EOrder.DESC,
            orderBy: 'createdAt',
          },
        });
    }
  };

  return (
    <Row gutter={[16, 0]} align='middle' justify='center'>
      <Col
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          fontSize: '12px',
          fontWeight: '12px',
        }}
      >
        {t('Sort_by')}:
      </Col>

      <Col>
        <Select
          defaultValue='case1'
          style={{ width: 100 }}
          onChange={handleChange}
          suffixIcon={<DownOutlined style={{ color: '#2F61E6', pointerEvents: 'none' }} />}
          className={styles._customSelect}
          options={[
            {
              value: 'case1',
              label: `${t('Latest')}`,
            },
            {
              value: 'case2',
              label: `${t('Oldest')}`,
            },
            {
              value: 'case3',
              label: 'A-Z',
            },
            {
              value: 'case4',
              label: 'Z-A',
            },
            {
              value: 'case5',
              label: `${t('View')}`,
            },
          ]}
        />
      </Col>
    </Row>
  );
}
export default ShoterComponent;
