import { useRouter } from 'next/router';
import { Space } from 'antd';
import React from 'react';

import { TCategory } from '@/modules';
import { ELanguage } from '@/configs/interface.config';

function AllTag({ list }: { list: TCategory[] }) {
  const router = useRouter();
  return (
    <Space wrap size={8}>
      {list.map((category) => (
        <div key={category._id} className='tag ellipsis oneline'>
          {category?.translation?.[router.locale as ELanguage]?.name}
        </div>
      ))}
    </Space>
  );
}

export default AllTag;
