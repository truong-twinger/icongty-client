/* eslint-disable react/jsx-one-expression-per-line */
import { Badge, Col, Row, Space, Typography } from 'antd';
import moment from 'moment';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMemo } from 'react';

import { getBlurDataUrl } from '@/libs/common';
import { TCompany } from '@/modules';
import { ELanguage } from '@/configs/interface.config';

import Tag from './components/Tag';
import styles from './style.module.less';

const { Text, Title } = Typography;

interface IProps {
  data: TCompany;
}

export default function TypicalCompany({ data }: IProps) {
  const { t } = useTranslation();
  const router = useRouter();
  const name = useMemo(
    () => (router.locale === ELanguage.EN ? data?.internationalName || data?.name : data?.name),
    [router],
  );
  return (
    <Link passHref href={`/company/${data.slug}`}>
      <Badge.Ribbon className={`${styles.tag} ${!data?.isSupported && styles.hide}`} color='#FA8C16' text={<Tag />}>
        <Row className={`${styles.wrapper} hoverItem`}>
          <Col className={styles.thumbnailWrap} span={24}>
            {data?.coverPhoto ? (
              <Image
                placeholder='blur'
                blurDataURL={getBlurDataUrl(data.coverPhoto.width || 256, data.coverPhoto.height || 341)}
                alt={name}
                className={styles.thumbnail}
                src={data.coverPhoto.location}
                fill
                sizes='30vw'
              />
            ) : (
              <Image
                alt='cover image company'
                className={styles.thumbnail}
                src='/images/category-company/covercompany.jpg'
                fill
                sizes='30vw'
              />
            )}
            <Col className={styles.overlay}>
              <Col>
                <Col className={styles.logoWrap}>
                  {data?.logo ? (
                    <Image
                      alt={name}
                      src={data?.logo?.location}
                      className={styles.logo}
                      width={72}
                      height={72}
                      sizes='30vw'
                    />
                  ) : (
                    <Image
                      alt='logo image company'
                      className={styles.logo}
                      src='/images/category-company/logocompany.jpg'
                      fill
                      sizes='30vw'
                    />
                  )}
                </Col>
                {data?.taxCode && (
                  <Space size={2} className={styles.textWrap}>
                    <Text className={styles.text}>
                      {t('home:mst')}: {data?.taxCode}
                    </Text>
                  </Space>
                )}
              </Col>
            </Col>
          </Col>
          <Col span={24}>
            <Title level={3} className={`${styles.title} ellipsis twoline`}>
              {name}
            </Title>
          </Col>
          <Col span={24} className={styles.categoryCol}>
            <Space size={8}>
              {data?.categories?.map(
                (category, index) =>
                  index < 2 && (
                    <Col key={category._id} className='tag ellipsis oneline'>
                      {category?.translation?.[router.locale as ELanguage]?.name}
                    </Col>
                  ),
              )}
              {data?.categories?.length > 2 && (
                <Col className='tag ellipsis oneline'>+{(data?.categories?.length || 2) - 2}</Col>
              )}
            </Space>
          </Col>

          <Col span={24} className={styles.separation} />

          <Col span={24}>
            <Row gutter={[0, 6]}>
              <Col span={24}>
                <Row gutter={[2, 0]}>
                  <Col span={12} sm={{ span: 14 }} md={{ span: 12 }}>
                    <Space size={10}>
                      <Image
                        alt='icon'
                        className={styles.icon}
                        src='/images/home/position.svg'
                        width={12}
                        height={11.25}
                      />
                      <Text className={styles.label} strong>
                        {t('home:location')}
                      </Text>
                    </Space>
                  </Col>
                  <Col span={12} sm={{ span: 10 }} md={{ span: 12 }} className={styles.contentWrap}>
                    {data?.city ? (
                      <Text className={`${styles.content} ellipsis oneline`}>{data.city.name}</Text>
                    ) : (
                      <Text>--</Text>
                    )}
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row gutter={[2, 0]}>
                  <Col span={12} sm={{ span: 14 }} md={{ span: 12 }}>
                    <Space size={10}>
                      <Image alt='icon' className={styles.icon} src='/images/home/founded.svg' width={12} height={12} />
                      <Text className={`${styles.label}`} strong>
                        {t('home:founded_year')}
                      </Text>
                    </Space>
                  </Col>
                  <Col span={12} sm={{ span: 10 }} md={{ span: 12 }} className={styles.contentWrap}>
                    {data?.foundationDate ? (
                      <Text className={`${styles.content} ellipsis oneline`}>
                        {moment(data.foundationDate).format('y')}
                      </Text>
                    ) : (
                      <Text>--</Text>
                    )}
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row gutter={[2, 0]}>
                  <Col span={12} sm={{ span: 14 }} md={{ span: 12 }}>
                    <Space size={10}>
                      <Image alt='icon' className={styles.icon} src='/images/home/scale.svg' width={12} height={12} />
                      <Text className={styles.label} strong>
                        {t('home:scale')}
                      </Text>
                    </Space>
                  </Col>
                  <Col span={12} sm={{ span: 10 }} md={{ span: 12 }} className={styles.contentWrap}>
                    {data?.companySize ? (
                      <Text className={`${styles.content} ellipsis oneline`}>
                        {data.companySize} {t('employees')}
                      </Text>
                    ) : (
                      <Text>--</Text>
                    )}
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Badge.Ribbon>
    </Link>
  );
}
