/* eslint-disable @typescript-eslint/no-explicit-any */
import { Col, Row, Space, Tag, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { getBlurDataUrl } from '@/libs/common';
import { TPost } from '@/modules';
import { ELanguage } from '@/configs/interface.config';

import styles from './style.module.less';

const { Title, Text } = Typography;

interface Props {
  data: TPost;
  next: TPost;
}

export default function HomeHotNews({ data, next }: Props) {
  const { t } = useTranslation();
  const router = useRouter();
  return (
    <Row gutter={[0, 32]} className={styles.wrapper}>
      <Col span={24}>
        <Row gutter={[{ lg: 48, xl: 72, md: 24, sm: 16, xs: 12 }, 12]}>
          <Col sm={{ span: 14 }} span={24}>
            <Space className={styles.taxGroup} size={13} direction='vertical'>
              <Space className={styles.tax} size={0}>
                {data?.categories?.map(
                  (category, index) =>
                    index < 2 && (
                      <Tag color='blue' key={category._id}>
                        {category?.translation?.[router.locale as ELanguage]?.name}
                      </Tag>
                    ),
                )}
                {data?.categories?.length > 2 && <Tag color='blue'>+{(data?.categories?.length || 2) - 2}</Tag>}
              </Space>
              <Link href={`/news/${data?.slug}` || '/#'}>
                <Title className={`${styles.title} ellipsis twoline`} level={3}>
                  {data.name}
                </Title>
              </Link>
            </Space>
          </Col>
          {next && (
            <Col sm={{ span: 10 }} className={styles.nextWrap} span={24}>
              <Space style={{ marginTop: 'auto' }} direction='vertical' size={8}>
                <Text className={styles.label}>{t('home:next')}:</Text>
                <Text className={`${styles.next} ellipsis twoline`} strong>
                  {next.name}
                </Text>
              </Space>
            </Col>
          )}
        </Row>
      </Col>
      <Col span={24} className={styles.thumbnailWrap}>
        <Link href={`/news/${data?.slug}` || '/#'}>
          <Image
            className={styles.thumbnail}
            placeholder='blur'
            blurDataURL={getBlurDataUrl(256, 341)}
            alt='Tin nổi bật'
            src={data?.thumbnail?.location}
            fill
            sizes='(max-width: 768px) 100vw,
            (max-width: 1200px) 50vw,
            50vw'
          />
        </Link>
      </Col>
    </Row>
  );
}
