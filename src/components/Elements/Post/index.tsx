import { Col, Row, Space, Tag, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { TPost } from '@/modules';
import { getBlurDataUrl } from '@libs/common';
import { ELanguage } from '@/configs/interface.config';

import styles from './style.module.less';

const { Title, Text } = Typography;

interface Props {
  isLast?: boolean;
  data: TPost;
}

export default function Post({ isLast, data }: Props) {
  const { t } = useTranslation();
  const router = useRouter();
  return (
    <Link passHref href={`/news/${data.slug}`}>
      <Row>
        <Col span={24}>
          <Space align='start' className={`${styles.wrapper} ${isLast && styles.noBorder}`} size={24}>
            <Row className={styles.left}>
              <Col className={styles.logoWrap}>
                <Image
                  className={styles.thumbnail}
                  placeholder='blur'
                  blurDataURL={getBlurDataUrl(256, 341)}
                  alt='news'
                  src={data.thumbnail?.location}
                  fill
                  sizes='20vw'
                />
              </Col>
              {data.source?.name && (
                <Text className={styles.caption}>
                  {t('home:by')}: {data.source?.name}
                </Text>
              )}
            </Row>
            <Row className={styles.content}>
              <Col span={24}>
                {data?.categories?.length > 0 && (
                  <Space wrap size={2}>
                    {data?.categories?.map(
                      (category, index) =>
                        index < 1 && (
                          <Tag color='blue' key={category._id}>
                            {category?.translation?.[router.locale as ELanguage]?.name}
                          </Tag>
                        ),
                    )}
                    {data?.categories?.length > 1 && <Tag color='blue'>+{(data?.categories?.length || 1) - 1}</Tag>}
                  </Space>
                )}
              </Col>
              <Col span={24}>
                <Title className={`${styles.title} ellipsis twoline`} level={3}>
                  {data.name}
                </Title>
              </Col>
              <Col span={24}>
                <Text className={`${styles.text} ellipsis threeline`}>{data.excerpt}</Text>
              </Col>
            </Row>
          </Space>
        </Col>
      </Row>
    </Link>
  );
}
