/* eslint-disable @typescript-eslint/no-explicit-any */
import { Col, Divider, Row, Space, Tag, Typography } from 'antd';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

import { getBlurDataUrl } from '@/libs/common';
import { TPost } from '@/modules';
import { ELanguage } from '@/configs/interface.config';

import styles from './style.module.less';

const { Text, Title } = Typography;

function RelatedPosts({ ListNewsByTax }: any) {
  const router = useRouter();
  return (
    <Row gutter={[0, 32]}>
      <Col span={24}>
        <Row gutter={[0, 32]}>
          <Col span={24}>
            <Row gutter={[0, 32]}>
              {ListNewsByTax?.map((value: TPost) => (
                <React.Fragment key={value._id}>
                  <Col span={24}>
                    <Divider style={{ margin: '0' }} />
                  </Col>

                  <Col span={24} className={styles._wrappBlock} key={value._id}>
                    <Link href={`/news/${value.slug}`} className={styles._block}>
                      <Row gutter={[{ xs: 16, sm: 16, md: 24 }, 0]} align='middle'>
                        <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }}>
                          <Row>
                            <Col span={24} className={styles.BlockWrap}>
                              <Image
                                className={styles.thumbnail}
                                placeholder='blur'
                                blurDataURL={getBlurDataUrl(256, 341)}
                                alt='Tin nổi bật'
                                src={value.thumbnail?.location}
                                fill
                                sizes='(max-width: 768px) 100vw'
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col
                          span={16}
                          xs={{ span: 24 }}
                          sm={{ span: 24 }}
                          md={{ span: 16 }}
                          className={styles._newsSynthesisText}
                        >
                          <Row gutter={[0, 8]}>
                            <Col span={24}>
                              <Space wrap size={2}>
                                {value?.categories?.map(
                                  (category, index) =>
                                    index < 1 && (
                                      <Tag color='blue' key={category._id}>
                                        {category?.translation?.[router.locale as ELanguage]?.name}
                                      </Tag>
                                    ),
                                )}
                                {value?.categories?.length > 1 && (
                                  <Tag color='blue'>+{(value?.categories?.length || 1) - 1}</Tag>
                                )}
                              </Space>
                            </Col>
                            <Col span={24}>
                              <Row gutter={[0, 16]}>
                                <Col span={24}>
                                  <Title className={`${styles.titleBlock} ellipsis twoline`} level={3}>
                                    {value.name}
                                  </Title>
                                </Col>
                                <Col span={24}>
                                  <Text className={`${styles.desBlock} ellipsis threeline`}>{value.excerpt}</Text>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Link>
                  </Col>
                </React.Fragment>
              ))}
            </Row>
          </Col>
          <Col span={24}>
            <Divider style={{ margin: '0' }} />
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
export default RelatedPosts;
