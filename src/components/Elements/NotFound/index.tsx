import { Col, Row, Typography } from 'antd';
import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';

import styles from './style.module.less';

const { Text } = Typography;

export default function NotFound() {
  const { t } = useTranslation();
  return (
    <Col span={24} className={styles.notFound}>
      <Row>
        <Col span={24}>
          <Image
            src='/images/category-company/notfound.png'
            width={684}
            height={480}
            className={styles.img}
            alt='not found'
          />
        </Col>
        <Col span={24}>
          <Text strong className={styles.note}>
            {t('not_found')}
          </Text>
        </Col>
        <Col span={24}>
          <Text className={styles.subNote}>{t('not_found_note')}</Text>
        </Col>
      </Row>
    </Col>
  );
}
