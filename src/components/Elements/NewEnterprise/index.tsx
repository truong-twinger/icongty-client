/* eslint-disable react/jsx-one-expression-per-line */
import { CopyOutlined } from '@ant-design/icons';
import { Col, message, Row, Space, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';

import { ELanguage } from '@/configs/interface.config';
import { getBlurDataUrl } from '@/libs/common';
import { TCompany } from '@/modules';

import styles from './style.module.less';

const { Title, Text } = Typography;

interface IProps {
  data: TCompany;
}

export default function NewEnterprise({ data }: IProps) {
  const [isCopied, setIsCopied] = useState(false);
  const handleCopy = (e: { preventDefault: () => void }, code: string) => {
    e.preventDefault();
    setIsCopied(true);
    navigator.clipboard
      .writeText(code)
      .then(() => {
        message.info(`Copied ${code}`);
      })
      .catch(() => {
        message.error('Error!!');
      });
  };

  const router = useRouter();
  const name = useMemo(
    () => (router.locale === ELanguage.EN ? data?.internationalName || data?.name : data?.name),
    [router],
  );

  const { t } = useTranslation();

  return (
    <Link passHref href={`/company/${data.slug}`}>
      <Space align='start' size={15} className={`${styles.wrapper} hoverItem`}>
        <Row>
          <Col className={styles.logoWrap}>
            {data?.logo ? (
              <Image
                placeholder='blur'
                blurDataURL={getBlurDataUrl(data.logo.width || 256, data.logo.height || 341)}
                alt={name}
                className={styles.logo}
                src={data.logo.location}
                fill
                sizes='30vw'
              />
            ) : (
              <Image
                alt='logo image company'
                className={styles.logo}
                src='/images/category-company/logocompany.jpg'
                fill
                sizes='30vw'
              />
            )}
          </Col>
        </Row>
        <Row className={styles.content} gutter={[0, 8]}>
          <Col span={24}>
            <Title className={`${styles.title} ellipsis twoline`} level={3}>
              {name}
            </Title>
          </Col>
          <Col span={24}>
            <Space direction='vertical' size={17}>
              <Text className={styles.codeWrap}>
                <Text className={`${styles.code} ellipsis oneline`}>
                  {t('home:tax_code')}: {data?.taxCode || '--'}
                </Text>
                {data?.taxCode && (
                  <Text className={styles.iconWrap}>
                    <CopyOutlined
                      onClick={(e) => handleCopy(e, data.taxCode)}
                      className={`${styles.icon} ${isCopied && styles.copied}`}
                    />
                  </Text>
                )}
              </Text>
              <Row className={styles.tagWrap} gutter={[8, 0]}>
                {data?.categories?.map(
                  (category, index) =>
                    index < 2 && (
                      <div key={category._id} className={`tag ellipsis oneline ${styles.tag}`}>
                        {category?.translation?.[router.locale as ELanguage]?.name}
                      </div>
                    ),
                )}
                {data?.categories?.length > 2 && (
                  <div className={`tag ellipsis oneline ${styles.tag}`}>+{(data?.categories?.length || 2) - 2}</div>
                )}
              </Row>
            </Space>
          </Col>
        </Row>
      </Space>
    </Link>
  );
}
