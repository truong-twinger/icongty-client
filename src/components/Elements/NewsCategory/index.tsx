/* eslint-disable @typescript-eslint/no-explicit-any */
import { DownOutlined } from '@ant-design/icons';
import { Empty, Col, Spin, Collapse, Divider, Input, Pagination, Row, Select, Space, Tag, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useMemo, useState } from 'react';
import moment from 'moment';

import { baseParams } from '@/configs/const.config';
import { ELanguage, EOrderBy } from '@/configs/interface.config';
import { useWindowSize } from '@/hooks';
import { getBlurDataUrl } from '@/libs/common';
import { handleFilter } from '@/libs/const';
import { EADS_KEY, TAdvertising, TQueryPost, TTaxonomy } from '@/modules';
import { queryAllPost } from '@/queries/hooks';
import { useQueryAdvertising } from '@/queries/hooks/advertising';

import Error from '../Error';

import styles from './style.module.less';

const { Title, Text } = Typography;
const { Search } = Input;

const { Panel } = Collapse;
export interface IPropsCate {
  listAllTax: TTaxonomy[];
}
export default function NewsCategory({ listAllTax }: IPropsCate) {
  const { t } = useTranslation(['news', 'common']);
  const router = useRouter();

  const [params, setParams] = useState<TQueryPost>();

  const {
    data: listPost,
    isError,
    isLoading,
  } = queryAllPost({ ...baseParams, ...params, limit: 6, isHot: 0 }, router.locale as ELanguage);

  const onSearch = (s: string) => {
    setParams({ ...params, s });
  };
  const handleCLickTAx = (tax: string) => {
    if (tax && tax !== '') {
      setParams({ ...params, 'taxonomyIds[]': tax });
    }
  };
  const handleClearTAx = () => {
    setParams({ ...params, 'taxonomyIds[]': undefined });
  };

  /// ads
  const { data: adsData, isLoading: isLoadingAdsById, isFetching: isFetchingAdsById } = useQueryAdvertising();

  const ads = useMemo(() => adsData?.data.values, [adsData, isLoadingAdsById, isFetchingAdsById]);

  // Value ads
  const valueADS = ads?.filter((value: TAdvertising) => value.key === EADS_KEY.NEWS_LIST);

  const [width] = useWindowSize();
  const srcAds = useMemo(() => {
    if (width < 576) {
      return valueADS?.[0]?.mb?.location;
    }
    if (width < 768) {
      return valueADS?.[0]?.tablet?.location;
    }
    return valueADS?.[0]?.pc?.location;
  }, [width, adsData]);

  return (
    <Col span={24} className={styles._cate}>
      <Row
        gutter={[
          { xs: 30, sm: 30, xl: 83 },
          { xs: 30, sm: 30, md: 0 },
        ]}
      >
        <Col span={8} xs={{ span: 24 }} md={{ span: 8 }}>
          <Row
            className={styles._wrappSticky}
            gutter={[
              0,
              {
                xs: 30,
                sm: 30,
                md: 64,
              },
            ]}
          >
            <Col md={{ order: 1 }} order={2} span={24} className={styles._noStick}>
              <Row gutter={[0, { xs: 30, sm: 30, md: 48 }]}>
                <Col span={24}>
                  <Search
                    placeholder={t('Article_Search') || 'Tìm kiếm bài viết'}
                    onSearch={onSearch}
                    enterButton
                    allowClear
                    className={styles._customSeach}
                  />
                </Col>
                <Col span={24}>
                  <Row gutter={[0, 24]}>
                    <Col className={styles._titleCate} span={24}>
                      {t('Category')}
                    </Col>
                    <Col span={24}>
                      {listAllTax && (
                        <Row gutter={[0, 8]} className='pc'>
                          <Col
                            span={24}
                            className={`${styles._nameCate}
                                ${(params && params['taxonomyIds[]']) || styles.active}
                                `}
                            onClick={() => handleClearTAx()}
                          >
                            {t('common:synthetic')}
                          </Col>
                          {listAllTax.map((value) => {
                            if (value.postCount > 0) {
                              return (
                                <Col
                                  span={24}
                                  key={value._id}
                                  className={`${styles._nameCate}
                                ${params && params['taxonomyIds[]'] === value._id && styles.active}
                                `}
                                  onClick={() => handleCLickTAx(value._id)}
                                >
                                  {value.name}
                                </Col>
                              );
                            }
                            return false;
                          })}
                        </Row>
                      )}
                      <Collapse className='_newsColabMobile'>
                        {listAllTax && (
                          <Panel header={<strong>{t('Select_a_category')}</strong>} key='1'>
                            <Col
                              span={24}
                              className={`${styles._nameCate} ${(params && params['taxonomyIds[]']) || styles.active}`}
                              onClick={() => handleClearTAx()}
                            >
                              {t('common:synthetic')}
                            </Col>
                            {listAllTax.map((value) => {
                              if (value.postCount > 0) {
                                return (
                                  <Col
                                    onClick={() => handleCLickTAx(value._id)}
                                    span={24}
                                    className={`${styles._nameCate} ${
                                      params && params['taxonomyIds[]'] === value._id && styles.active
                                    }`}
                                    key={value._id}
                                  >
                                    {value.name}
                                  </Col>
                                );
                              }
                              return false;
                            })}
                          </Panel>
                        )}
                      </Collapse>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            {valueADS && (
              <Col md={{ order: 2 }} order={1} span={24} className={styles._sticky}>
                <Link href={valueADS?.[0]?.guide || '/'}>
                  <Image className='ads' src={srcAds} alt='ads' fill sizes='100vw' />
                </Link>
              </Col>
            )}
          </Row>
        </Col>
        <Col span={16} xs={{ span: 24 }} md={{ span: 16 }}>
          <Row gutter={[0, 48]}>
            <Col span={24}>
              <Row gutter={[0, 16]}>
                <Col span={24}>
                  <Row justify='space-between' align='top'>
                    <Col>
                      <Title className={`${styles.title} ellipsis threeline`} level={2}>
                        {params?.s && params?.s !== '' ? t('Search_Results') : t('General_News')}
                      </Title>
                      {params?.s && params.s !== '' && (
                        <Text className={styles._textSeach}>
                          {t('Find')} <strong>{listPost?.total}</strong> {t('results_for')}{' '}
                          <strong>“{params.s}”</strong>
                        </Text>
                      )}
                    </Col>
                    <Col>
                      <Row gutter={[16, 0]} align='middle' justify='center'>
                        <Col
                          style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            fontSize: '12px',
                            fontWeight: '12px',
                          }}
                        >
                          {t('Sort_by')}:
                        </Col>

                        <Col>
                          <Select
                            value={
                              params?.order === 'ASC' ? `-${params?.orderBy}` : `${params?.orderBy || 'createdAt'}`
                            }
                            style={{ width: 100 }}
                            onChange={(value: string) => {
                              setParams({
                                ...params,
                                orderBy: handleFilter(value).orderBy as EOrderBy,
                                order: handleFilter(value).order,
                              });
                            }}
                            suffixIcon={<DownOutlined style={{ color: '#2F61E6', pointerEvents: 'none' }} />}
                            className={styles._customSelect}
                          >
                            <Select.Option value='createdAt'>{t('common:latest')}</Select.Option>
                            <Select.Option value='-createdAt'>{t('common:oldest')}</Select.Option>
                            <Select.Option value='-name'>A-Z</Select.Option>
                            <Select.Option value='name'>Z-A</Select.Option>
                            <Select.Option value='viewer'>{t('common:most_view')}</Select.Option>
                          </Select>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
                <Col span={24}>
                  <Row gutter={[0, 32]}>
                    <Col span={24}>
                      {isError ? (
                        <Error />
                      ) : (
                        <Row gutter={[0, 32]}>
                          {isLoading ? (
                            <Spin className='empty' />
                          ) : (
                            listPost?.data?.map((value) => (
                              <React.Fragment key={value._id}>
                                <Col span={24}>
                                  <Divider style={{ margin: '0' }} />
                                </Col>

                                <Col span={24} className={styles._wrappBlock} key={value._id}>
                                  <Link href={`/news/${value.slug}`}>
                                    <Row gutter={[{ xs: 16, sm: 16, md: 24 }, 8]} align='middle'>
                                      <Col
                                        span={16}
                                        xs={{ span: 24 }}
                                        sm={{ span: 24 }}
                                        md={{ span: 16 }}
                                        className={styles._newsSynthesisText}
                                      >
                                        <Row gutter={[0, 8]}>
                                          <Col span={24}>
                                            <Space wrap size={2}>
                                              {value?.categories?.map(
                                                (category, index) =>
                                                  index < 2 && (
                                                    <Tag color='blue' key={category._id}>
                                                      {category?.translation?.[router.locale as ELanguage]?.name}
                                                    </Tag>
                                                  ),
                                              )}
                                              {value?.categories?.length > 2 && (
                                                <Tag color='blue'>+{(value?.categories?.length || 2) - 2}</Tag>
                                              )}
                                            </Space>
                                          </Col>
                                          <Col span={24}>
                                            <Row>
                                              <Col span={24}>
                                                <Title className={`${styles.titleBlock} ellipsis twoline`} level={3}>
                                                  {value.name}
                                                </Title>
                                              </Col>
                                              <Col span={24}>
                                                <Row gutter={[16, 0]} className={styles._source}>
                                                  {value?.source?.name && (
                                                    <>
                                                      <Col>
                                                        {t('common:according_to')}{' '}
                                                        <strong>{value?.source?.name}</strong>
                                                      </Col>
                                                      <Col> | </Col>
                                                    </>
                                                  )}

                                                  <Col>{moment(value.createdAt).format('DD/MM/YYYY')}</Col>
                                                </Row>
                                              </Col>
                                              <Col span={24}>
                                                <Text className={`${styles.desBlock} ellipsis threeline`}>
                                                  {value.excerpt}
                                                </Text>
                                              </Col>
                                            </Row>
                                          </Col>
                                        </Row>
                                      </Col>
                                      <Col span={8} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }}>
                                        <Row>
                                          <Col span={24} className={styles.BlockWrap}>
                                            <Image
                                              className={styles.thumbnail}
                                              placeholder='blur'
                                              blurDataURL={getBlurDataUrl(256, 341)}
                                              alt='Tin nổi bật'
                                              src={
                                                value?.thumbnail?.location || '/images/category-company/logocompany.jpg'
                                              }
                                              fill
                                              sizes='(max-width: 768px) 100vw'
                                            />
                                          </Col>
                                        </Row>
                                      </Col>
                                    </Row>
                                  </Link>
                                </Col>
                              </React.Fragment>
                            ))
                          )}
                          {listPost?.data?.length === 0 && (
                            <Empty className='empty' description={t('common:no_data')} />
                          )}
                        </Row>
                      )}
                    </Col>
                    <Col span={24}>
                      <Divider style={{ margin: '0' }} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <Pagination
                responsive
                style={{ textAlign: 'right' }}
                size='small'
                total={Math.ceil(((listPost?.total || 0) * 10) / 6)}
                onChange={(page) => setParams({ ...params, page })}
                hideOnSinglePage
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </Col>
  );
}
