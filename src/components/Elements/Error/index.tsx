import React from 'react';
import { useTranslation } from 'next-i18next';

export default function Error() {
  const { t } = useTranslation();
  return <div>{t('error')}</div>;
}
