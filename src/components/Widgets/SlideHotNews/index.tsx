/* eslint-disable indent */
import React from 'react';
import { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { SwiperOptions } from 'swiper/types';
import { Button, Col, Empty, Progress, Row, Typography } from 'antd';
import { RightOutlined, LeftOutlined } from '@ant-design/icons';
import ReactDOMServer from 'react-dom/server';
import { useTranslation } from 'next-i18next';

import logger from '@libs/logger';

import styles from './style.module.less';

const { Text } = Typography;

interface ISlideShowCustom extends SwiperOptions {
  isPagination?: boolean;
  isNavigation?: boolean;
  slideElement?: JSX.Element[];
  slideKey: string;
}

function SlideCustomize({
  isPagination = false,
  isNavigation = false,
  slideElement,
  slideKey,
  ...props
}: ISlideShowCustom) {
  const { t } = useTranslation();
  return (
    <Row gutter={[0, 40]} className={styles.wrapper}>
      {slideElement && slideElement?.length > 0 ? (
        <>
          <Col span={24} className={styles.swiper}>
            <Swiper
              autoplay
              modules={[Navigation, Pagination]}
              spaceBetween={30}
              slidesPerView={1}
              navigation={
                isNavigation
                  ? {
                      nextEl: `.button-next-${slideKey}`,
                      prevEl: `.button-prev-${slideKey}`,
                    }
                  : false
              }
              pagination={
                isPagination
                  ? {
                      type: 'custom',
                      clickable: true,
                      el: `.pagination-${slideKey}`,
                      renderCustom: (swiper, current, total) => {
                        logger.debug('swiper:', swiper);
                        return ReactDOMServer.renderToString(
                          <Col>
                            <Text className={styles.current}>{`${current < 10 ? 0 : ''}${current}`}</Text>
                            <Progress
                              className={styles.progress}
                              percent={(current / total) * 100}
                              size='small'
                              showInfo={false}
                            />
                            <Text className={styles.total}>{`${total < 10 ? 0 : ''}${total}`}</Text>
                          </Col>,
                        );
                      },
                    }
                  : false
              }
              onSwiper={(swiper) => logger.debug('🚀 ~ SlideShowCustom ~ swiper', swiper)}
              onSlideChange={(swiper) => logger.debug('🚀 ~ SlideShowCustom - slide change', swiper)}
              {...props}
            >
              {slideElement?.map((e, index) => (
                // eslint-disable-next-line react/no-array-index-key
                <SwiperSlide key={index}>{e}</SwiperSlide>
              ))}
            </Swiper>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={12}>
                <Button
                  type='ghost'
                  className={`${styles.navigation} ${styles.navigationPrev} button-prev-${slideKey}`}
                  icon={<LeftOutlined className={styles.iconNavigation} />}
                />
                <Button
                  type='ghost'
                  className={`${styles.navigation} ${styles.navigationNext} button-next-${slideKey}`}
                  icon={<RightOutlined className={styles.iconNavigation} />}
                />
              </Col>
              <Col span={12} className={`${styles.pagination} pagination-${slideKey}`} />
            </Row>
          </Col>
        </>
      ) : (
        <Col span={24}>
          <Empty className='empty' description={t('no_data')} />
        </Col>
      )}
    </Row>
  );
}

export default SlideCustomize;
