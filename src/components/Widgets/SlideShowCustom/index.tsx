/* eslint-disable indent */
import React from 'react';
import { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { SwiperOptions } from 'swiper/types';
import { Button, Col, Empty, Row } from 'antd';
import { RightOutlined, LeftOutlined } from '@ant-design/icons';

import logger from '@libs/logger';

import styles from './style.module.less';

interface ISlideShowCustom extends SwiperOptions {
  isPagination?: boolean;
  isNavigation?: boolean;
  slideElement?: JSX.Element[];
  slideKey: string;
}

function SlideShowCustom({
  isPagination = false,
  isNavigation = false,
  slideElement,
  slideKey,
  ...props
}: ISlideShowCustom) {
  return (
    <Row gutter={[0, 40]} className={styles.wrapper}>
      {slideElement && slideElement?.length > 0 ? (
        <>
          <Col span={24} className={styles.swiper}>
            <Swiper
              modules={[Navigation, Pagination]}
              spaceBetween={0}
              slidesPerView={1}
              navigation={
                isNavigation
                  ? {
                      nextEl: `.button-next-${slideKey}`,
                      prevEl: `.button-prev-${slideKey}`,
                    }
                  : false
              }
              pagination={isPagination ? { clickable: true, el: `.pagination-${slideKey}` } : false}
              onSwiper={(swiper) => logger.debug('🚀 ~ SlideShowCustom ~ swiper', swiper)}
              onSlideChange={(swiper) => logger.debug('🚀 ~ SlideShowCustom - slide change', swiper)}
              {...props}
            >
              {slideElement?.map((e, index) => (
                // eslint-disable-next-line react/no-array-index-key
                <SwiperSlide key={index}>{e}</SwiperSlide>
              ))}
            </Swiper>
            {isNavigation && (
              <>
                <Button
                  className={`tw-button-navigation ${styles.navigation} ${styles.navigationPrev} button-prev-${slideKey}`}
                  shape='circle'
                >
                  <LeftOutlined className={styles.iconNavigation} />
                </Button>
                <Button
                  className={`tw-button-navigation ${styles.navigation} ${styles.navigationNext} button-next-${slideKey}`}
                  shape='circle'
                >
                  <RightOutlined className={styles.iconNavigation} />
                </Button>
              </>
            )}
          </Col>
          {isPagination && (
            // eslint-disable-next-line react/self-closing-comp
            <Col span={24} className={`tw-pagination ${styles.pagination} pagination-${slideKey}`}></Col>
          )}
        </>
      ) : (
        <Col span={24}>
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='no data' />
        </Col>
      )}
    </Row>
  );
}

export default SlideShowCustom;
