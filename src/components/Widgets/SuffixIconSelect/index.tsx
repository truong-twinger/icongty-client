import { CaretDownOutlined } from '@ant-design/icons';

import style from './style.module.less';

export default function SuffixIconSelect() {
  return <CaretDownOutlined className={style.wrapper} />;
}
