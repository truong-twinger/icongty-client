import { GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';
import { useTranslation } from 'next-i18next';

import Loading from '@/components/Elements/Loading';
import { LANGUAGE_DEFAULT } from '@/configs/const.config';
import { EKeyOption, ELanguage } from '@/configs/interface.config';
import { getOptionByKey } from '@/queries/apis';

const ContactScreen = dynamic(() => import('@components/Screens/Contact'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

export async function getServerSideProps({ locale }: GetServerSidePropsContext) {
  const fetchOption = await getOptionByKey([EKeyOption.CONTACTOPTON, EKeyOption.GENERALOPTION], locale as ELanguage);

  const optionContact = fetchOption.data[0];
  const optionGeneral = fetchOption.data[1];
  return {
    props: {
      ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'contact'])),
      optionContact,
      optionGeneral,
    },
  };
}

function Contact(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { t } = useTranslation();
  const { optionContact, optionGeneral } = props;

  return (
    <Layout
      title={t('contact') || 'Liên hệ'}
      description={optionGeneral?.dataSEO?.metaDesciption}
      keywords={optionGeneral?.dataSEO?.metaKeyword}
      withSearch
    >
      <ContactScreen optionContact={optionContact} optionGeneral={optionGeneral} />
    </Layout>
  );
}

export default Contact;
