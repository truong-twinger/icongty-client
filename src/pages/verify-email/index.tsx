import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

import Loading from '@/components/Elements/Loading';

const VerifyEmailSreen = dynamic(() => import('@/components/Screens/VerifyEmail'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

function VerifyEmail() {
  const router = useRouter();

  useEffect(() => {
    if (!router.query?.token && router.isReady) {
      router.replace('/');
    }
  }, [router]);

  return (
    <Layout title='Xác thực email' description='Trang xác thực email'>
      <VerifyEmailSreen />
    </Layout>
  );
}

export async function getStaticProps() {
  // return {
  //   props: {
  //     ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'auth'])),
  //   },
  // };
  return {
    notFound: true,
  };
}

export default VerifyEmail;
