import dynamic from 'next/dynamic';

import Loading from '@/components/Elements/Loading';

const SignInScreen = dynamic(() => import('@/components/Screens/SignIn'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

function SignIn() {
  return (
    <Layout title='Đăng nhập'>
      <SignInScreen />
    </Layout>
  );
}

export async function getStaticProps() {
  // return {
  //   props: {
  //     ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'auth'])),
  //   },
  // };
  return {
    notFound: true,
  };
}

export default SignIn;
