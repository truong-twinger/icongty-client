import { GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';
import { dehydrate, QueryClient } from 'react-query';

import Loading from '@/components/Elements/Loading';
import { baseParams, LANGUAGE_DEFAULT } from '@/configs/const.config';
import { EKeyOption, ELanguage, EOrder, EOrderBy } from '@/configs/interface.config';
import { TPost } from '@/modules';
import { getListCategory, getListCompanyFromDatabase, getListPostFromDatabase, getOptionByKey } from '@/queries/apis';
import { GET_ALL_CATEGORY } from '@/queries/keys';
import { GET_LIST_COMPANY, GET_LIST_TYPICAL_COMPANY } from '@/queries/keys/company';
import { getDateNow } from '@/libs/common';

const HomeScreen = dynamic(() => import('@components/Screens/Home'), { loading: () => <Loading /> });
const Layout = dynamic(() => import('@/components/Layouts'));

export async function getServerSideProps({ locale }: GetServerSidePropsContext) {
  const queryClient = new QueryClient();
  const fetchAllHotNews = await getListPostFromDatabase(
    {
      ...baseParams,
      limit: -1,
      isHot: 1,
    },
    locale as ELanguage,
  );
  const notInIdsNews = fetchAllHotNews?.data?.map((item: TPost) => item._id) || [];
  const fetchNews = await getListPostFromDatabase(
    {
      ...baseParams,
      limit: 3,
      'notInIds[]': notInIdsNews,
    },
    locale as ELanguage,
  );

  const paramsTypical = {
    ...baseParams,
    limit: 6,
    lang: (locale as ELanguage) || ELanguage.VI,
    typical: 1,
  };
  await queryClient.prefetchQuery([GET_LIST_TYPICAL_COMPANY, JSON.stringify(paramsTypical)], () =>
    getListCompanyFromDatabase(paramsTypical),
  );

  const paramsListCompany = {
    ...baseParams,
    limit: 9,
    lang: (locale as ELanguage) || ELanguage.VI,
    typical: 0,
  };
  await queryClient.prefetchQuery([GET_LIST_COMPANY, JSON.stringify(paramsListCompany)], () =>
    getListCompanyFromDatabase(paramsListCompany),
  );

  const paramsListCategory = {
    ...baseParams,
    limit: 12,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    lang: (locale as ELanguage) || ELanguage.VI,
  };
  await queryClient.prefetchQuery([GET_ALL_CATEGORY, JSON.stringify(paramsListCategory)], () =>
    getListCategory(paramsListCategory),
  );

  const fetchHomeOption = await getOptionByKey([EKeyOption.HOMEOPTION], locale as ELanguage);
  const fetchGeneralOption = await getOptionByKey([EKeyOption.GENERALOPTION], locale as ELanguage);

  const homeOption = fetchHomeOption.data;
  const generalOption = fetchGeneralOption.data;

  return {
    props: {
      ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home'])),
      dehydratedState: dehydrate(queryClient),
      homeOption,
      generalOption,
      fetchNews,
      fetchAllHotNews,
    },
  };
}

function Home(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { fetchNews, fetchAllHotNews, homeOption, generalOption } = props;

  return (
    <Layout
      description={generalOption[0]?.dataSEO?.metaDesciption}
      keywords={generalOption[0]?.dataSEO?.metaKeyword}
      title={generalOption[0]?.dataSEO?.title}
      ogImage={`${generalOption[0]?.value?.location}?v=${getDateNow()}`}
    >
      <HomeScreen homeOption={homeOption} fetchNews={fetchNews} fetchAllHotNews={fetchAllHotNews} />
    </Layout>
  );
}

export default Home;
