import dynamic from 'next/dynamic';

import Loading from '@/components/Elements/Loading';

const ForgotPasswordScreen = dynamic(() => import('@/components/Screens/ForgotPassword'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

function SignIn() {
  return (
    <Layout title='Quên mật khẩu'>
      <ForgotPasswordScreen />
    </Layout>
  );
}

export async function getStaticProps() {
  // return {
  //   props: {
  //     ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'auth'])),
  //   },
  // };
  return {
    notFound: true,
  };
}

export default SignIn;
