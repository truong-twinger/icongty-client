/* eslint-disable @typescript-eslint/no-explicit-any */
import { GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';

import Loading from '@/components/Elements/Loading';
import { baseParams, LANGUAGE_DEFAULT } from '@/configs/const.config';
import { ELanguage, EOrder, EOrderBy, TPaths } from '@/configs/interface.config';
import { getDateNow } from '@/libs/common';
import { TPost } from '@/modules';
import { getListPostFromDatabase, getPostBySlug } from '@/queries/apis';

const DetailNewsScreen = dynamic(() => import('@/components/Screens/DetailsNewsScreen'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

export const getStaticPaths = async () => {
  const fetchAllNew = await getListPostFromDatabase({ ...baseParams });

  const { data } = fetchAllNew;
  const paths: TPaths = [];
  data?.forEach((post: TPost) => {
    const result = [];
    if (post?.publishedLanguage) {
      post.publishedLanguage.forEach((lang) => {
        result.push({
          params: { slug: post.slug },
          locale: lang,
        });
      });
    } else {
      result.push({
        params: { slug: post.slug },
      });
    }
    paths.push(...result);
  });
  return {
    paths,
    fallback: true,
  };
};

export async function getStaticProps({ locale, params }: GetServerSidePropsContext) {
  try {
    const fetchAllNew = await (params?.slug && getPostBySlug(params?.slug.toString(), locale as ELanguage));
    const dataDetail: TPost = fetchAllNew?.data;
    const notInds = dataDetail?._id;
    const taxonomyIds = dataDetail?.taxonomies?.map((value) => value._id);
    // listNew by tax
    const fetchAllNewByTax = await getListPostFromDatabase(
      {
        ...baseParams,
        page: 1,
        limit: 3,
        'notInIds[]': [`${notInds}`],
        'taxonomyIds[]': taxonomyIds,
      },
      locale as ELanguage,
    );

    const ListNewsByTax = fetchAllNewByTax?.data;
    const fetchAllNewTopView = await getListPostFromDatabase(
      {
        page: 1,
        limit: 5,
        order: EOrder.DESC,
        orderBy: EOrderBy.VIEWER,
      },
      locale as ELanguage,
    );
    // const listAllNew = decryptedData(fetchAllNew);
    const listAllNewTopView = fetchAllNewTopView?.data;

    return {
      props: {
        ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'news'])),
        // listAllNew,
        dataDetail,
        params,
        ListNewsByTax,
        listAllNewTopView,
      },
      revalidate: 60,
    };
  } catch (err) {
    return { notFound: true };
  }
}

function DetailNews(props: InferGetServerSidePropsType<typeof getStaticProps>) {
  const { dataDetail, ListNewsByTax, listAllNewTopView } = props;

  return (
    <Layout
      ogImage={`${dataDetail?.thumbnail?.location}?v=${getDateNow()}`}
      title={dataDetail?.name || 'Chi tiết công ty'}
      description={dataDetail?.excerpt || ''}
      keywords={`${dataDetail?.keyword}`}
      withSearch
    >
      <DetailNewsScreen dataDetail={dataDetail} ListNewsByTax={ListNewsByTax} listAllNewTopView={listAllNewTopView} />
    </Layout>
  );
}

export default DetailNews;
