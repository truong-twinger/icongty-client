import { GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';
import { dehydrate, QueryClient } from 'react-query';
import { useTranslation } from 'next-i18next';

import Loading from '@/components/Elements/Loading';
import { baseParams, LANGUAGE_DEFAULT } from '@/configs/const.config';
import { EKeyOption, ELanguage } from '@/configs/interface.config';
import { getListPostFromDatabase, getListTaxonomy, getOptionByKey } from '@/queries/apis';
import { LIST_POST } from '@/queries/keys';

const NewsScreen = dynamic(() => import('@/components/Screens/NewsScreen'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

export async function getServerSideProps({ locale, query }: GetServerSidePropsContext) {
  const queryClient = new QueryClient();
  const fetchAllNew = await getListPostFromDatabase(
    {
      ...baseParams,
      limit: -1,
      isHot: 1,
      lang: locale as ELanguage,
    },

    locale as ELanguage,
  );
  // const listAllNew = decryptedData(fetchAllNew);
  const listAllNew = fetchAllNew.data;

  /// taxonomy
  const fetchAllTax = await getListTaxonomy({
    ...baseParams,
    limit: 10,
    lang: locale as ELanguage,
  });
  const listAllTax = fetchAllTax.data;

  const params = {
    ...baseParams,
    ...query,
    limit: 6,
    isHot: 0,
  };
  await queryClient.prefetchQuery([LIST_POST, JSON.stringify(params), locale], () =>
    getListPostFromDatabase(params, locale as ELanguage),
  );

  const fetchOptionPage = await getOptionByKey([EKeyOption.GENERALOPTION], locale as ELanguage);
  const optionPage = fetchOptionPage.data;

  return {
    props: {
      ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'news'])),
      dehydratedState: dehydrate(queryClient),
      listAllNew,
      listAllTax,
      optionPage,
    },
  };
}

function News(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { t } = useTranslation();
  const { listAllNew, listAllTax, optionPage } = props;

  return (
    <Layout
      description={optionPage[0]?.dataSEO?.metaDesciption}
      keywords={optionPage[0]?.dataSEO?.metaKeyword}
      title={t('news') || 'Tin tức'}
      withSearch
    >
      <NewsScreen listAllNew={listAllNew} listAllTax={listAllTax} />
    </Layout>
  );
}

export default News;
