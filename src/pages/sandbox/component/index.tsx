import { InboxOutlined, PlusOutlined, SearchOutlined } from '@ant-design/icons';
import {
  Button,
  Cascader,
  Checkbox,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Rate,
  Row,
  Select,
  Slider,
  Space,
  Switch,
  TreeSelect,
  Typography,
  // eslint-disable-next-line prettier/prettier
  Upload
} from 'antd';
import dynamic from 'next/dynamic';
import { useMemo } from 'react';

import SlideShowCustom from '@/components/Widgets/SlideShowCustom';

const Layout = dynamic(() => import('@components/Layouts'));

const { Title } = Typography;

// const fakeHotNews = [
//   { title: 'Câu chuyện lựa chọn đối tác “đường dài” của các Startup Việt', thumbnail: '/images/home/hotnews.png' },
//   {
//     title: 'Nhìn lại một năm nổi bật của nền kinh tế Việt Nam 2022: Có nhiều điểm sáng',
//     thumbnail: '/images/home/company.png',
//   },
//   { title: 'Câu chuyện lựa chọn đối tác “đường dài” của các Startup Việt', thumbnail: '/images/home/hotnews.png' },
//   { title: 'Câu chuyện lựa chọn đối tác “đường dài” của các Startup Việt', thumbnail: '/images/home/hotnews.png' },
//   { title: 'Câu chuyện lựa chọn đối tác “đường dài” của các Startup Việt', thumbnail: '/images/home/hotnews.png' },
// ];

function Component() {
  const slideElement = useMemo(
    () =>
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map((e, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={i} style={{ height: 200, background: 'rgb(54, 77, 121)' }}>
          {e}
        </div>
      )),
    [],
  );

  // const slideElementHotNews = useMemo(
  //   () =>
  //     fakeHotNews.map((e, i) => (
  //       // eslint-disable-next-line react/no-array-index-key
  //       <HotNews next={fakeHotNews[i + 2]} data={e} key={i} />
  //     )),
  //   [],
  // );
  return (
    <Layout title='Sandbox-component' description='Sandbox component description'>
      <div className='home-container'>
        <div style={{ marginBottom: 200 }} className='home-wrapper'>
          <Row gutter={[30, 30]}>
            <Col span={24}>
              <Title level={2}>Component</Title>
            </Col>
            <Col span={24}>
              <Title level={4}>Button</Title>
            </Col>
            <Col span={24}>
              <Space direction='vertical'>
                <Space>
                  <Button type='default'>Default</Button>
                  <Button type='primary'>Primary</Button>
                  <Button type='ghost'>Ghost</Button>
                  <Button type='dashed'>Dashed</Button>
                  <Button type='link'>Link</Button>
                  <Button type='text'>Text</Button>
                </Space>
              </Space>
              <br />
              <br />
              <Space direction='vertical'>
                <Space>
                  <Button ghost type='primary'>
                    Primary
                  </Button>
                </Space>
                <Space>
                  <Button ghost>Warning</Button>
                </Space>
              </Space>
              <br />
              <br />
              <Title level={5}>Icon Button</Title>
              <Space>
                <Button shape='circle' icon={<SearchOutlined />} />
                <Button icon={<SearchOutlined />}>Search</Button>
                <Button type='primary' icon={<PlusOutlined />}>
                  Add New
                </Button>
              </Space>
              <br />
              <br />
              <Title level={5}>Loading Button</Title>
              <Space>
                <Button loading shape='circle' icon={<SearchOutlined />} />
                <Button loading icon={<SearchOutlined />}>
                  Search
                </Button>
                <Button loading icon={<PlusOutlined />}>
                  Add New
                </Button>
                <Button loading icon={<PlusOutlined />}>
                  Click Me !
                </Button>
              </Space>
              <br />
              <br />
              <Title level={5}>Other</Title>
              <Space>
                <Button icon={<SearchOutlined />}>Search Dashed</Button>
                <Button icon={<PlusOutlined />}>Add New Link</Button>
                <Button size='small'>Small</Button>
                <Button size='middle'>Middle</Button>
                <Button size='large'>Large</Button>
              </Space>
            </Col>
            <Col span={24}>
              <Title level={4}>Input</Title>
            </Col>
            <Col span={24}>
              <Row justify='center'>
                <Col span={8}>
                  <Form layout='vertical'>
                    <Form.Item
                      label='Email'
                      name='email'
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item label='Password'>
                      <Input />
                    </Form.Item>
                    <Form.Item label='Form Size' name='size'>
                      <Radio.Group>
                        <Radio.Button value='small'>Small</Radio.Button>
                        <Radio.Button value='default'>Default</Radio.Button>
                        <Radio.Button value='large'>Large</Radio.Button>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item label='Select'>
                      <Select>
                        <Select.Option value='demo'>Demo</Select.Option>
                      </Select>
                    </Form.Item>
                    <Form.Item label='TreeSelect'>
                      <TreeSelect
                        treeData={[
                          { title: 'Light', value: 'light', children: [{ title: 'Bamboo', value: 'bamboo' }] },
                        ]}
                      />
                    </Form.Item>
                    <Form.Item label='Cascader'>
                      <Cascader
                        options={[
                          {
                            value: 'zhejiang',
                            label: 'Zhejiang',
                            children: [
                              {
                                value: 'hangzhou',
                                label: 'Hangzhou',
                              },
                            ],
                          },
                        ]}
                      />
                    </Form.Item>
                    <Form.Item label='DatePicker'>
                      <DatePicker />
                    </Form.Item>
                    <Form.Item label='InputNumber'>
                      <InputNumber />
                    </Form.Item>
                    <Form.Item label='Switch' valuePropName='checked'>
                      <Switch />
                    </Form.Item>
                    <Form.Item label='Button'>
                      <Button block>Button</Button>
                    </Form.Item>
                    <Form.Item name='slider' label='Slider'>
                      <Slider
                        marks={{
                          0: 'A',
                          20: 'B',
                          40: 'C',
                          60: 'D',
                          80: 'E',
                          100: 'F',
                        }}
                      />
                    </Form.Item>

                    <Form.Item name='radio-group' label='Radio.Group'>
                      <Radio.Group>
                        <Radio value='a'>item 1</Radio>
                        <Radio value='b'>item 2</Radio>
                        <Radio value='c'>item 3</Radio>
                      </Radio.Group>
                    </Form.Item>

                    <Form.Item name='checkbox-group' label='Checkbox.Group'>
                      <Checkbox.Group>
                        <Row>
                          <Col span={8}>
                            <Checkbox value='A' style={{ lineHeight: '32px' }}>
                              A
                            </Checkbox>
                          </Col>
                          <Col span={8}>
                            <Checkbox value='B' style={{ lineHeight: '32px' }} disabled>
                              B
                            </Checkbox>
                          </Col>
                          <Col span={8}>
                            <Checkbox value='C' style={{ lineHeight: '32px' }}>
                              C
                            </Checkbox>
                          </Col>
                          <Col span={8}>
                            <Checkbox value='D' style={{ lineHeight: '32px' }}>
                              D
                            </Checkbox>
                          </Col>
                          <Col span={8}>
                            <Checkbox value='E' style={{ lineHeight: '32px' }}>
                              E
                            </Checkbox>
                          </Col>
                          <Col span={8}>
                            <Checkbox value='F' style={{ lineHeight: '32px' }}>
                              F
                            </Checkbox>
                          </Col>
                        </Row>
                      </Checkbox.Group>
                    </Form.Item>

                    <Form.Item name='rate' label='Rate'>
                      <Rate />
                    </Form.Item>

                    <Form.Item label='Dragger'>
                      <Form.Item name='dragger' valuePropName='fileList' noStyle>
                        <Upload.Dragger name='files' action='/upload.do'>
                          <p className='ant-upload-drag-icon'>
                            <InboxOutlined />
                          </p>
                          <p className='ant-upload-text'>Click or drag file to this area to upload</p>
                          <p className='ant-upload-hint'>Support for a single or bulk upload.</p>
                        </Upload.Dragger>
                      </Form.Item>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
              <Col span={24} style={{ paddingLeft: 100, paddingRight: 100 }}>
                <SlideShowCustom
                  slideElement={slideElement}
                  isNavigation
                  isPagination
                  slidesPerView={4}
                  slideKey='demo'
                  spaceBetween={40}
                />
              </Col>
            </Col>

            <Col span={24}>
              <Title level={3}>Doanh nghiệp mới</Title>
            </Col>
            <Col span={24}>
              <Title level={3}>Danh mục ngành nghề</Title>
            </Col>
            <Col span={24}>
              <Title level={3}>Tin mới</Title>
            </Col>
            <Col span={24} style={{ display: 'flex', flexWrap: 'wrap' }}>
              <div style={{ width: 684, marginRight: 56 }}>a</div>
            </Col>
            <Col span={24}>
              <Title level={3}>Công ty tiêu biểu</Title>
              <Row gutter={[26, 32]}>
                {/* <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <TypicalCompany />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <TypicalCompany />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <TypicalCompany />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <TypicalCompany />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <TypicalCompany />
                </Col> */}
              </Row>
            </Col>
            <Col span={24}>
              <Title level={3}>Doanh nghiệp mới</Title>
              {/* <Row gutter={[26, 32]}>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <NewEnterprise />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <NewEnterprise />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <NewEnterprise />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <NewEnterprise />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <NewEnterprise />
                </Col>
              </Row> */}
            </Col>
            <Col span={24}>
              <Title level={3}>Danh mục ngành nghề</Title>
              {/* <Row gutter={[24, 24]}>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <CareerCategory />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <CareerCategory />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <CareerCategory />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <CareerCategory />
                </Col>
                <Col sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 8 }} span={24}>
                  <CareerCategory />
                </Col>
              </Row> */}
            </Col>
            <Col span={24}>
              <Title level={3}>Tin mới</Title>
            </Col>
            <Col span={24} style={{ display: 'flex', flexWrap: 'wrap' }}>
              <Row gutter={[58, 30]}>
                <Col span={24} lg={{ span: 15 }}>
                  {/* <SlideHotNews
                    slideElement={slideElementHotNews}
                    isNavigation
                    isPagination
                    slidesPerView={1}
                    slideKey='news'
                  /> */}
                </Col>
                <Col span={24} lg={{ span: 9 }}>
                  {/* <Row gutter={[24, 24]}>
                    <Col span={24}>
                      <Post />
                    </Col>
                    <Col span={24}>
                      <Post />
                    </Col>
                    <Col span={24}>
                      <Post />
                    </Col>
                  </Row> */}
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </Layout>
  );
}

export default Component;
