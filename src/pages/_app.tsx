import '@/styles/globals.less';
import { appWithTranslation } from 'next-i18next';
import type { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import nProgress from 'nprogress';
import React, { useEffect } from 'react';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { ToastContainer } from 'react-toastify';

import useFoucFix from '@/hooks/useFoucFix';
import logger from '@libs/logger';

function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  useFoucFix();
  useEffect(() => {
    nProgress.configure({ showSpinner: false });
    const handleStart = (url: string) => {
      logger.debug('🚀 ~ handleStart ~ url', url);
      nProgress.start();
    };
    const handleStop = (url: string) => {
      logger.debug('🚀 ~ handleStop ~ url', url);
      nProgress.done();
    };
    router.events.on('routeChangeStart', handleStart);
    router.events.on('routeChangeComplete', handleStop);
    router.events.on('routeChangeError', handleStop);
    return () => {
      nProgress.remove();
      router.events.off('routeChangeStart', handleStart);
      router.events.off('routeChangeComplete', handleStop);
      router.events.off('routeChangeError', handleStop);
    };
  }, [router]);

  const [queryClient] = React.useState(() => new QueryClient());

  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        <ToastContainer
          position='top-right'
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme='light'
        />
        <ReactQueryDevtools initialIsOpen={false} position='bottom-right' />
      </Hydrate>
      <Component {...pageProps} />
    </QueryClientProvider>
  );
}
export default appWithTranslation(App);
