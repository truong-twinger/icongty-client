import { GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';
import { dehydrate, QueryClient } from 'react-query';

import Loading from '@/components/Elements/Loading';
import { baseParams, LANGUAGE_DEFAULT } from '@/configs/const.config';
import { ELanguage, EOrder, EOrderBy } from '@/configs/interface.config';
import { getListCategory, getListCity, getListCompanyFromDatabase } from '@/queries/apis';
import { GET_LIST_COMPANY } from '@/queries/keys/company';

const SearchScreen = dynamic(() => import('@components/Screens/Search'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

export async function getServerSideProps({ locale, query }: GetServerSidePropsContext) {
  const queryClient = new QueryClient();
  const params = {
    ...baseParams,
    ...query,
    limit: 15,
    lang: (locale as ELanguage) || ELanguage.VI,
  };
  await queryClient.prefetchQuery([GET_LIST_COMPANY, JSON.stringify(params)], () => getListCompanyFromDatabase(params));

  const fetchTakeCareCompany = await getListCompanyFromDatabase({
    ...baseParams,
    limit: 3,
    lang: (locale as ELanguage) || ELanguage.VI,
  });
  const listTakeCareCompany = fetchTakeCareCompany.data;

  const fetchListCategory = await getListCategory({
    ...baseParams,
    limit: 0,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    lang: (locale as ELanguage) || ELanguage.VI,
  });

  const fetchListCity = await getListCity({
    ...baseParams,
    limit: 0,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    'countryCodes[]': ['vn'],
    lang: (locale as ELanguage) || ELanguage.VI,
  });

  return {
    props: {
      ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'company'])),
      dehydratedState: dehydrate(queryClient),
      fetchListCategory,
      fetchListCity,
      listTakeCareCompany,
    },
  };
}

function Search(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { fetchListCity, fetchListCategory, listTakeCareCompany } = props;
  return (
    <Layout>
      <SearchScreen
        fetchListCity={fetchListCity}
        fetchListCategory={fetchListCategory}
        listTakeCareCompany={listTakeCareCompany}
      />
    </Layout>
  );
}

export default Search;
