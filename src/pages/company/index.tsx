import { GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';
import { dehydrate, QueryClient } from 'react-query';
import { useTranslation } from 'next-i18next';

import Loading from '@/components/Elements/Loading';
import { baseParams, LANGUAGE_DEFAULT } from '@/configs/const.config';
import { EKeyOption, ELanguage, EOrder, EOrderBy } from '@/configs/interface.config';
import { getListCategory, getListCity, getListCompanyFromDatabase, getOptionByKey } from '@/queries/apis';
import { GET_LIST_COMPANY } from '@/queries/keys/company';

const CompanyScreen = dynamic(() => import('@components/Screens/CompanyScreen'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

export async function getServerSideProps({ locale, query }: GetServerSidePropsContext) {
  const queryClient = new QueryClient();
  const params = {
    ...baseParams,
    ...query,
    limit: 15,
    lang: (locale as ELanguage) || ELanguage.VI,
  };
  await queryClient.prefetchQuery([GET_LIST_COMPANY, JSON.stringify(params)], () => getListCompanyFromDatabase(params));

  const fetchListCategory = await getListCategory({
    ...baseParams,
    limit: 0,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    lang: (locale as ELanguage) || ELanguage.VI,
  });

  const fetchListCity = await getListCity({
    ...baseParams,
    limit: 0,
    orderBy: EOrderBy.NAME,
    order: EOrder.ASC,
    'countryCodes[]': ['vn'],
    lang: (locale as ELanguage) || ELanguage.VI,
  });

  const fetchOptionPage = await getOptionByKey(
    [EKeyOption.COMPANYOPTION, EKeyOption.GENERALOPTION],
    locale as ELanguage,
  );
  const optionPage = fetchOptionPage.data[0];
  const optionGeneral = fetchOptionPage.data[1];

  return {
    props: {
      ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'company'])),
      dehydratedState: dehydrate(queryClient),
      fetchListCategory,
      fetchListCity,
      optionPage,
      optionGeneral,
    },
  };
}

function CategoryCompany(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { t } = useTranslation();
  const { fetchListCategory, fetchListCity, optionPage, optionGeneral } = props;

  return (
    <Layout
      title={t('company') || 'Danh mục công ty'}
      description={optionGeneral?.dataSEO?.metaDesciption}
      keywords={optionGeneral?.dataSEO?.metaKeyword}
    >
      <CompanyScreen optionPage={optionPage} fetchListCity={fetchListCity} fetchListCategory={fetchListCategory} />
    </Layout>
  );
}

export default CategoryCompany;
