import { GetStaticProps, InferGetServerSidePropsType } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import dynamic from 'next/dynamic';
import { dehydrate, QueryClient } from 'react-query';

import Loading from '@/components/Elements/Loading';
import { baseParams, LANGUAGE_DEFAULT } from '@/configs/const.config';
import { ELanguage, TPaths } from '@/configs/interface.config';
import { getDateNow } from '@/libs/common';
import { TCategory, TCompany } from '@/modules';
import {
  getCompanyBySlug,
  getListCompanyFromDatabase,
  getPortfolioByCompanyID,
  getProductByCompanyID,
  getServiceByCompanyID,
} from '@/queries/apis';
import { LIST_POSTPORTFOLIO, LIST_POSTPRODUCT, LIST_POSTSERVICE } from '@/queries/keys';

const LayOut = dynamic(() => import('@/components/Layouts'));
const DetailCompany = dynamic(() => import('@components/Screens/DetailCompany'), {
  loading: () => <Loading />,
});

export const getStaticProps: GetStaticProps = async ({ locale, params }) => {
  try {
    const queryClient = new QueryClient();
    const fetchDetailCompany = await (params?.slug && getCompanyBySlug(params?.slug.toString(), locale as ELanguage));
    const detailCompany = fetchDetailCompany.data;
    const _idDetail = detailCompany._id;

    const listCategory = detailCompany?.categories?.map((category: TCategory) => category._id) || [];
    const fetchListRelatedCompany = await getListCompanyFromDatabase({
      ...baseParams,
      limit: 5,
      'categoryIds[]': listCategory,
      'notInIds[]': _idDetail,
      lang: (locale as ELanguage) || ELanguage.VI,
    });
    const listRelatedCompany = fetchListRelatedCompany.data;

    const paramsTab = { ...baseParams, limit: 10, companyId: _idDetail };
    await queryClient.prefetchQuery([LIST_POSTPORTFOLIO, JSON.stringify(paramsTab)], () =>
      getPortfolioByCompanyID(paramsTab, locale as ELanguage),
    );
    const fetchPortfolio = await getPortfolioByCompanyID(
      { ...baseParams, limit: 15, companyId: _idDetail },
      locale as ELanguage,
    );
    const listPortfolio = fetchPortfolio.data;

    await queryClient.prefetchQuery([LIST_POSTPRODUCT, JSON.stringify(paramsTab)], () =>
      getProductByCompanyID(paramsTab, locale as ELanguage),
    );
    const fetchProduct = await getProductByCompanyID(
      { ...baseParams, limit: 15, companyId: _idDetail },
      locale as ELanguage,
    );
    const listProduct = fetchProduct.data;

    await queryClient.prefetchQuery([LIST_POSTSERVICE, JSON.stringify(paramsTab)], () =>
      getServiceByCompanyID(paramsTab, locale as ELanguage),
    );
    const fetchService = await getServiceByCompanyID(
      { ...baseParams, limit: 15, companyId: _idDetail },
      locale as ELanguage,
    );
    const listService = fetchService.data;
    return {
      props: {
        ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'company'])),
        dehydratedState: dehydrate(queryClient),
        detailCompany,
        listRelatedCompany,
        listPortfolio,
        listProduct,
        listService,
      },
      revalidate: 60,
    };
  } catch (err) {
    return { notFound: true };
  }
};

export const getStaticPaths = async () => {
  const fetchAllCompany = await getListCompanyFromDatabase({ ...baseParams });

  const { data } = fetchAllCompany;
  const paths: TPaths = [];
  data?.forEach((post: TCompany) => {
    const result = [];
    if (post?.publishedLanguage) {
      post.publishedLanguage.forEach((lang) => {
        result.push({
          params: { slug: post.slug },
          locale: lang,
        });
      });
    } else {
      result.push({
        params: { slug: post.slug },
      });
    }
    paths.push(...result);
  });
  return {
    paths,
    fallback: true,
  };
};

function DetailNews(props: InferGetServerSidePropsType<typeof getStaticProps>) {
  const { detailCompany, listRelatedCompany, listPortfolio, listService, listProduct } = props;

  const { t } = useTranslation();
  return (
    <LayOut
      withSearch
      keywords={`${detailCompany?.keyword}${detailCompany?.taxCode ? ` - ${detailCompany?.taxCode}` : ''}`}
      ogImage={`${detailCompany?.coverPhoto?.location}?v=${getDateNow()}`}
      title={
        `${detailCompany?.taxCode ? `${detailCompany?.taxCode} - ` : ''}${detailCompany?.name}` ||
        t('company:title_page') ||
        'Chi tiết công ty'
      }
      description={detailCompany?.excerpt || ''}
    >
      <DetailCompany
        listRelatedCompany={listRelatedCompany}
        detailCompany={detailCompany}
        listPortfolio={listPortfolio}
        listService={listService}
        listProduct={listProduct}
      />
    </LayOut>
  );
}

export default DetailNews;
