import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

import Loading from '@/components/Elements/Loading';

const ResetPasswordScreen = dynamic(() => import('@/components/Screens/ResetPassword'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

function ResetPassword() {
  const router = useRouter();
  useEffect(() => {
    if (!router.query?.token && router.isReady) {
      router.replace('/');
    }
  }, [router]);
  return (
    <Layout title='Đặt lại mật khẩu'>
      {router.query?.token && <ResetPasswordScreen token={router.query.token.toString()} />}
    </Layout>
  );
}

export async function getStaticProps() {
  // return {
  //   props: {
  //     ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'auth'])),
  //   },
  // };
  return {
    notFound: true,
  };
}

export default ResetPassword;
