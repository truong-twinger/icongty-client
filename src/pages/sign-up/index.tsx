import dynamic from 'next/dynamic';

import Loading from '@/components/Elements/Loading';

const SignUpScreen = dynamic(() => import('@/components/Screens/SignUp'), {
  loading: () => <Loading />,
});
const Layout = dynamic(() => import('@/components/Layouts'));

function SignIn() {
  return (
    <Layout title='Đăng ký'>
      <SignUpScreen />
    </Layout>
  );
}

export async function getStaticProps() {
  // return {
  //   props: {
  //     ...(await serverSideTranslations(locale || LANGUAGE_DEFAULT, ['common', 'home', 'auth'])),
  //   },
  // };
  return {
    notFound: true,
  };
}

export default SignIn;
