import { NextRouter } from 'next/router';

import { ELanguage, EOrderBy, EOrder } from '@/configs/interface.config';

export const handleChangeLanguage = (router: NextRouter) => {
  const { pathname, asPath, query } = router;
  if (router?.locale === ELanguage.EN) {
    router.replace({ pathname, query }, asPath, { locale: ELanguage.VI });
  } else {
    router.replace({ pathname, query }, asPath, { locale: ELanguage.EN });
  }
};

// handleFilter
export const handleFilter = (value: string) => {
  const params = {
    order: EOrder.DESC,
    orderBy: value || EOrderBy.CREATED_DATE,
  };
  if (value?.toString()?.charAt(0) === '-') {
    params.order = EOrder.ASC;
    params.orderBy = value.toString().substr(1, value.length);
  }
  return params;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const chunkArray = (myArray: Array<any>, chunkSize: number) => {
  const tempArray = [];
  if (myArray) {
    for (let index = 0; index < myArray.length; index += chunkSize) {
      const myChunk = myArray.slice(index, index + chunkSize);
      tempArray.push(myChunk);
    }
  }

  return tempArray;
};
