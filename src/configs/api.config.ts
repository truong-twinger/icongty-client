/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosRequestConfig } from 'axios';

import { checkAuth } from '@/libs/localStorage';
import logger from '@/libs/logger';

import { decryptedData, LANGUAGE_DEFAULT } from './const.config';
import { API_ENDPOINT, API_TIMEOUT, BASE_URL } from './env.config';
import { ELanguage } from './interface.config';

const client = axios.create({
  baseURL: API_ENDPOINT,
  timeout: API_TIMEOUT,
  timeoutErrorMessage: '🚧🚧🚧 Server connection time out!',
  headers: {
    Accept: 'application/json',
    xsrfCookieName: 'XSRF-TOKEN',
    xsrfHeaderName: 'X-XSRF-TOKEN',
    responseEncoding: 'utf8',
    responseType: 'json',
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache, no-store, must-revalidate',
    'Access-Control-Allow-Origin': BASE_URL,
    'X-Application': 'web app',
    'X-Version': '1.0.1',
  },
});
export const request = async (
  options: AxiosRequestConfig,
  additional?: { lang?: ELanguage | string; token?: string },
) => {
  logger.debug('🚧🚧🚧 ~ Axios Options:', options);

  if (additional?.token || checkAuth()) {
    client.defaults.headers.common.Authorization = `Bearer ${additional?.token || checkAuth()}`;
  }

  client.defaults.headers.common.lang = additional?.lang || LANGUAGE_DEFAULT;

  const onSuccess = (response: any) => {
    const { data } = response;
    let result = data;
    result = data;
    // if (process.env.APP_ENV === 'production') {
    //   result = JSON.parse(decryptedData(data));
    // }
    result = JSON.parse(decryptedData(data));
    logger.debug('🚀🚀🚀 ~ Response API:', result);
    return result;
  };
  const onError = async (error: any) => {
    let errorD = error;
    if (process.env.APP_ENV === 'production') {
      errorD = JSON.parse(decryptedData(error));
    }
    logger.error('🚨🚨🚨 ~ Axios onError:', errorD);
    await Promise.reject({
      statusCode: errorD?.response?.data?.statusCode,
      message: errorD?.response?.data?.message,
      statusText: errorD?.response?.statusText,
      status: errorD?.response?.status,
      data: errorD?.response?.data?.data || null,
    });
  };
  return client(options).then(onSuccess).catch(onError);
};
