import { request } from '@/configs/api.config';
import { ELanguage } from '@/configs/interface.config';
import { TQueryTaxonomy } from '@/modules';

export const getListTaxonomy = (params: TQueryTaxonomy) => request({ url: 'taxonomy/web', method: 'GET', params });
export const getTaxonomyBySlug = (slug: string, lang: ELanguage) =>
  request({ url: `taxonomy/${slug}/detail`, method: 'GET' }, { lang });
