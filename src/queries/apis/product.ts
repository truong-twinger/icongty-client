import { request } from '@/configs/api.config';
import { ELanguage } from '@/configs/interface.config';
import { TQueryProduct } from '@/modules';

export const getListProduct = (params: TQueryProduct) => request({ url: 'product/web', method: 'GET', params });
export const getProductBySlug = (slug: string, lang: ELanguage) =>
  request({ url: `product/${slug}/detail`, method: 'GET' }, { lang });
