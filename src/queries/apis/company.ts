import { request } from '@/configs/api.config';
import { ELanguage } from '@/configs/interface.config';
import { TCompany, TQueryParamsGetCompany } from '@/modules';

export const postCompany = (data: TCompany, lang: ELanguage) =>
  request({ url: 'company', method: 'POST', data }, { lang });
export const getListCompany = (params: TQueryParamsGetCompany) =>
  request({ url: 'company/search', method: 'GET', params });
export const getListCompanyFromDatabase = (params: TQueryParamsGetCompany) =>
  request({ url: 'company/companies', method: 'GET', params });

export const getSuggestCompany = (params: TQueryParamsGetCompany) =>
  request({ url: 'company/suggest', method: 'GET', params });
export const getCompanyBySlug = (slug: string, lang: ELanguage) =>
  request({ url: `company/${slug}/detail`, method: 'GET' }, { lang });
