import { request } from '@/configs/api.config';
import { EKeyOption, ELanguage } from '@/configs/interface.config';
import { TQueryParamsGetOption } from '@/modules';

export const getListOption = (params: TQueryParamsGetOption) => request({ url: 'option', method: 'GET', params });
export const getOptionByKey = (key: EKeyOption[], lang: ELanguage) =>
  request({ url: 'option/key', method: 'GET', params: { key, lang } });
