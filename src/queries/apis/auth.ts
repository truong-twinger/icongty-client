import { request } from '@/configs/api.config';
import { TPassword, TRegister, TResetPassword, TSignIn } from '@modules/auth';

export const signIn = (body: TSignIn) => request({ url: 'auth/login', method: 'POST', data: body });
export const signOut = (token: string) => request({ url: 'auth/logout', method: 'POST' }, { token });
export const refreshToken = (token: string, refreshToken: string) =>
  request({ url: 'auth/refresh-token', method: 'POST', data: { refreshToken } }, { token });
export const changePassword = (token: string, body: TPassword) =>
  request({ url: 'auth/change-password', method: 'POST', data: body }, { token });
export const register = (body: TRegister) => request({ url: 'auth/register', method: 'POST', data: body });
export const forgotPassword = (email: string) =>
  request({ url: 'auth/forgot-password', method: 'POST', data: { email } });
export const resetPassword = (body: TResetPassword) =>
  request({ url: 'auth/reset-password', method: 'POST', data: body });
export const confirmEmail = (body: { token: string }) =>
  request({ url: 'auth/confirm-email', method: 'POST', data: body });
export const resendVerifyEmail = (body: { email: string }) =>
  request({ url: 'auth/resend-verify-email', method: 'POST', data: body });
