import { request } from '@/configs/api.config';
import { ELanguage } from '@/configs/interface.config';
import { TQueryPost } from '@/modules';

export const getListPost = (params: TQueryPost, lang?: ELanguage) =>
  request({ url: 'post/search', method: 'GET', params: { ...params } }, { lang });
export const getListPostFromDatabase = (params: TQueryPost, lang?: ELanguage) =>
  request({ url: 'post/posts', method: 'GET', params: { ...params } }, { lang });

export const getPostBySlug = (slug: string, lang: ELanguage) =>
  request({ url: `post/${slug}/detail`, method: 'GET' }, { lang });
export const getListAllNew = (params: TQueryPost) => request({ url: 'post', method: 'GET', params });

export const getPortfolioByCompanyID = (params: TQueryPost, lang: ELanguage) =>
  request({ url: 'portfolio/web', method: 'GET', params: { ...params } }, { lang });

export const getServiceByCompanyID = (params: TQueryPost, lang: ELanguage) =>
  request({ url: 'service/web', method: 'GET', params: { ...params } }, { lang });

export const getProductByCompanyID = (params: TQueryPost, lang: ELanguage) =>
  request({ url: 'product/web', method: 'GET', params: { ...params } }, { lang });
