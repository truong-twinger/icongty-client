import { request } from '@/configs/api.config';
import { ELanguage } from '@/configs/interface.config';
import { TQueryParamsGetJob } from '@/modules';

export const getListJob = (params: TQueryParamsGetJob) => request({ url: 'job/web', method: 'GET', params });
export const getJobBySlug = (slug: string, lang: ELanguage) =>
  request({ url: `job/${slug}/detail`, method: 'GET', params: { lang } });
