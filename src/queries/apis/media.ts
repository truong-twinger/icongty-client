import { API_ENDPOINT } from '@/configs/env.config';

export const urlUploadMedia = `${API_ENDPOINT}media/s3/upload`;
