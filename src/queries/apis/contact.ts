import { request } from '@/configs/api.config';
import { TPostContact } from '@/modules';

export const postContact = (data: TPostContact) => request({ url: 'contact-form', method: 'POST', data });
