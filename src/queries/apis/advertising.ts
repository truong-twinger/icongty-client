import { request } from '@/configs/api.config';

export const getCurrentAdvertising = () => request({ url: 'advertising/current', method: 'GET' });
