import { request } from '@/configs/api.config';
import { ELanguage, TQueryParamsGetData } from '@/configs/interface.config';
import { TQueryParamsGetCity } from '@/modules';

// incorporation-type
export const getListIncorporationType = (params: TQueryParamsGetData) =>
  request({ url: 'incorporation-type', method: 'GET', params });
export const getIncorporationTypeById = (id: string, lang: ELanguage) =>
  request({ url: `incorporation-type/${id}`, method: 'GET' }, { lang });
export const getIncorporationTypeByCode = (code: string, lang: ELanguage) =>
  request({ url: `incorporation-type/code/${code}`, method: 'GET' }, { lang });

// category
export const getListCategory = (params: TQueryParamsGetData) => request({ url: 'category', method: 'GET', params });
export const getCategoryById = (id: string, lang: ELanguage) =>
  request({ url: `category/${id}`, method: 'GET' }, { lang });
export const getCategoryByCode = (code: string, lang: ELanguage) =>
  request({ url: `category/code/${code}`, method: 'GET' }, { lang });

// technology
export const getListTechnology = (params: TQueryParamsGetData) => request({ url: 'technology', method: 'GET', params });
export const getTechnologyById = (id: string, lang: ELanguage) =>
  request({ url: `technology/${id}`, method: 'GET' }, { lang });
export const getTechnologyByCode = (code: string, lang: ELanguage) =>
  request({ url: `technology/code/${code}`, method: 'GET' }, { lang });

// country/city
export const getListCountry = (params: TQueryParamsGetData) => request({ url: 'country', method: 'GET', params });
export const getCountryById = (id: string, lang: ELanguage) =>
  request({ url: `country/${id}`, method: 'GET' }, { lang });
export const getCountryByCode = (code: string, lang: ELanguage) =>
  request({ url: `country/code/${code}`, method: 'GET' }, { lang });
export const getListCity = (params: TQueryParamsGetCity) => request({ url: 'city', method: 'GET', params });
export const getCityById = (id: string, lang: ELanguage) => request({ url: `city/${id}`, method: 'GET' }, { lang });
