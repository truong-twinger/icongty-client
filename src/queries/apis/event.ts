import { request } from '@/configs/api.config';
import { ELanguage } from '@/configs/interface.config';
import { TQueryParamsGetEvent } from '@/modules';

export const getListEvent = (params: TQueryParamsGetEvent) => request({ url: 'event/web', method: 'GET', params });
export const getEventBySlug = (slug: string, lang: ELanguage) =>
  request({ url: `event/${slug}/detail`, method: 'GET' }, { lang });
