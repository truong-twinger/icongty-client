import { useInfiniteQuery, useMutation, useQuery, useQueryClient } from 'react-query';

import { TPostDataWithLang, TResApi, TResApiErr, TResDataListApi } from '@/configs/interface.config';
import logger from '@/libs/logger';
import { TAuth, TCompany, TQueryParamsGetCompany } from '@/modules';

import { getListCompany, getListCompanyFromDatabase, getSuggestCompany, postCompany } from '../apis';
import { GET_LIST_SUGGEST_COMPANY } from '../keys/company';

export const usePostCompany = () => {
  const queryClient = useQueryClient();
  return useMutation(({ body, lang }: TPostDataWithLang<TCompany>) => postCompany(body, lang), {
    onSuccess: (data: TResApi<TAuth>) => {
      logger.debug('PostCompany data:', data);
      // Todo
    },
    onError: (error: TResApiErr) => {
      logger.debug('PostCompany error:', error);
    },
    onSettled: (data, error, variables, context) => {
      logger.debug('PostCompany onSettled', data, error, variables, context);
      queryClient.invalidateQueries();
    },
  });
};

export const queryAllCompany = (params: TQueryParamsGetCompany, key: string) =>
  useQuery<TResDataListApi<TCompany[]>>({
    queryKey: [key, JSON.stringify(params)],
    queryFn: () => getListCompany(params),
    refetchOnMount: false,
    keepPreviousData: true,
  });
export const queryAllCompanyFromDatabase = (params: TQueryParamsGetCompany, key: string) =>
  useQuery<TResDataListApi<TCompany[]>>({
    queryKey: [key, JSON.stringify(params)],
    queryFn: () => getListCompanyFromDatabase(params),
    refetchOnMount: false,
    keepPreviousData: true,
  });

export const queryGetSuggestCompany = (params: TQueryParamsGetCompany) =>
  useInfiniteQuery({
    queryKey: [GET_LIST_SUGGEST_COMPANY, JSON.stringify(params)],
    enabled: !!params?.s,
    queryFn: ({ pageParam = 1 }) => getSuggestCompany({ ...params, page: pageParam }),
    getNextPageParam: (lastPage) =>
      lastPage?.page < Math.ceil(lastPage?.total / lastPage?.limit) ? lastPage?.page + 1 : undefined,
  });
