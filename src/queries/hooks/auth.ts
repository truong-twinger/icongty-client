import { useRouter } from 'next/router';
import { useMutation, useQuery, useQueryClient } from 'react-query';

import { urlHeader } from '@/configs/const.config';
import { TAuth, TChangePassword, TRegister, TResetPassword, TRole, TSignature, TUser } from '@/modules';
import { TResApi, TResApiErr } from '@configs/interface.config';
import { checkAuth, clearStoredAuth, getStoredAuth, setStoredAuth } from '@libs/localStorage';
import logger from '@libs/logger';

import {
  changePassword,
  confirmEmail,
  forgotPassword,
  refreshToken,
  register,
  resendVerifyEmail,
  resetPassword,
  signIn,
  signOut,
} from '../apis';
import { getRoleCurrent } from '../apis/role';
import { getProfile } from '../apis/user';
import { USER_CURRENT_ROLE, USER_PROFILE } from '../keys';

/**
 * @method useQuerySignIn
 * @returns
 */
export const useMutationSignIn = () => {
  const queryClient = useQueryClient();
  return useMutation(signIn, {
    onSuccess: async (res: TResApi<TAuth>) => {
      setStoredAuth(res.data.auth);
      // toast.success(res.message);
    },
    onError: () => {
      void clearStoredAuth();
      // toast.error(error.message || error.statusText);
    },
    onSettled: () => {
      queryClient.invalidateQueries();
    },
  });
};

/**
 * @method useMutationSignOut
 * @returns
 */
export const useMutationSignOut = () => {
  const router = useRouter();
  const accessToken = checkAuth();
  return useMutation(() => signOut(accessToken), {
    onSuccess: (data: TResApi) => {
      logger.debug('SignOut data:', data);
      router.push({ pathname: urlHeader.signIn });
      // Todo
    },
    onError: (error: TResApiErr) => {
      logger.error('SignOut error:', error);
      // Todo
    },
    onSettled(data, error, variables, context) {
      logger.debug('signOut onSettled', data, error, variables, context);
      void clearStoredAuth();
    },
  });
};

/**
 * @method useQueryProfile
 * @returns
 */
export const useQueryProfile = (token: string) =>
  useQuery<TResApi<TUser>>([USER_PROFILE], () => getProfile(token), {
    enabled: !!token,
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
    retry: 2,
  });

/**
 * @method useQueryRoleCurrent
 * @returns
 */
export const useQueryRoleCurrent = (token: string) =>
  useQuery<TResApi<TRole[]>>([USER_CURRENT_ROLE], () => getRoleCurrent(token), {
    enabled: !!token,
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
    retry: 2,
  });

/**
 * @method refreshTokenFn
 * @returns
 */
export const refreshTokenFn: () => void = async () => {
  const signature: TSignature | null = getStoredAuth();
  if (signature) {
    const result: TResApi = await refreshToken(signature.accessToken, signature?.refreshToken || '');
    if (result.statusCode === 200) {
      setStoredAuth({
        ...signature,
        accessToken: result.data.accessToken,
        expiredAt: result.data.expiredAt,
      });
    }
    return result;
  }
  // TODO ...
  return false;
};

export const useChangePassword = () => {
  const queryClient = useQueryClient();
  const accessToken = checkAuth();
  return useMutation((body: TChangePassword) => changePassword(accessToken, body), {
    onSuccess: (data: TResApi) => {
      logger.debug('ChangePassword data:', data);
      // Todo
    },
    onError: (error: TResApiErr) => {
      logger.debug('ChangePassword error:', error);
      // Todo
    },
    onSettled(data, error, variables, context) {
      logger.debug('ChangePassword onSettled', data, error, variables, context);
      queryClient.invalidateQueries();
    },
  });
};

export const useRegister = () => {
  const queryClient = useQueryClient();
  return useMutation((body: TRegister) => register(body), {
    onSuccess: (res: TResApi<TAuth>) => {
      // toast.success(res.message);
      logger.debug('Register res:', res);
      // Todo
    },
    onError: (error: TResApiErr) => {
      // toast.error(error.message || error.statusText);
      logger.debug('Register error:', error);
    },
    onSettled: (data, error, variables, context) => {
      logger.debug('Register onSettled', data, error, variables, context);
      queryClient.invalidateQueries();
    },
  });
};

export const useForgotPassword = () => {
  const queryClient = useQueryClient();
  return useMutation((email: string) => forgotPassword(email), {
    onSuccess: (data: TResApi) => {
      logger.debug('ForgotPassword data:', data);
      // Todo
    },
    onError: (error: TResApiErr) => {
      logger.debug('ForgotPassword error:', error);
      // Todo
    },
    onSettled(data, error, variables, context) {
      logger.debug('ForgotPassword onSettled', data, error, variables, context);
      queryClient.invalidateQueries();
    },
  });
};

export const useResetPassword = () => {
  const queryClient = useQueryClient();
  return useMutation((body: TResetPassword) => resetPassword(body), {
    onSuccess: (data: TResApi) => {
      logger.debug('ResetPassword data:', data);
      // Todo
    },
    onError: (error: TResApiErr) => {
      logger.debug('ResetPassword error:', error);
      // Todo
    },
    onSettled(data, error, variables, context) {
      logger.debug('ResetPassword onSettled', data, error, variables, context);
      queryClient.invalidateQueries();
    },
  });
};

export const useConfirmEmail = () => {
  const queryClient = useQueryClient();
  return useMutation((body: { token: string }) => confirmEmail(body), {
    onSuccess: (data: TResApi) => {
      logger.debug('ConfirmEmail data:', data);
      // Todo
    },
    onError: (error: TResApiErr) => {
      logger.debug('ConfirmEmail error:', error);
      // Todo
    },
    onSettled(data, error, variables, context) {
      logger.debug('ConfirmEmail onSettled', data, error, variables, context);
      queryClient.invalidateQueries();
    },
  });
};

export const useResendVerifyEmail = () => {
  const queryClient = useQueryClient();
  return useMutation((body: { email: string }) => resendVerifyEmail(body), {
    onSuccess: (data: TResApi) => {
      logger.debug('ResendVerifyEmail data:', data);
      // Todo
    },
    onError: (error: TResApiErr) => {
      logger.debug('ResendVerifyEmail error:', error);
      // Todo
    },
    onSettled(data, error, variables, context) {
      logger.debug('ResendVerifyEmail onSettled', data, error, variables, context);
      queryClient.invalidateQueries();
    },
  });
};
