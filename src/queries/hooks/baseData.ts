/* eslint-disable no-unsafe-optional-chaining */
import { useInfiniteQuery } from 'react-query';

import { TQueryParamsGetData } from '@/configs/interface.config';

import { GET_LIST_CITY } from '../keys';
import { getListCity } from '../apis';

// Get list city
export const useInfiniteAllCity = (params: TQueryParamsGetData) =>
  useInfiniteQuery({
    queryKey: [GET_LIST_CITY, params],
    queryFn: ({ pageParam = 1 }) => getListCity({ ...params, page: pageParam }),
    getNextPageParam: (lastPage) =>
      // eslint-disable-next-line prettier/prettier
      (lastPage?.page < Math.ceil(lastPage?.total / lastPage?.limit) ? lastPage?.page + 1 : undefined),
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
  });
