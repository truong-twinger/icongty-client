import { useQuery } from 'react-query';

import { getCurrentAdvertising } from '../apis/advertising';
import { GET_CURRENT_ADVERTISING } from '../keys';

export const useQueryAdvertising = () =>
  useQuery([GET_CURRENT_ADVERTISING], getCurrentAdvertising, {
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
    retry: 2,
  });
