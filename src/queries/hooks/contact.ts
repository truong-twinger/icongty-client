import { useMutation } from 'react-query';
import { toast } from 'react-toastify';

import { TResApiErr } from '@/configs/interface.config';
import { TPostContact } from '@/modules';

import { postContact } from '../apis';

export const useMutationPostContact = () =>
  useMutation((data: TPostContact) => postContact(data), {
    onError: (error: TResApiErr) => {
      toast.error(error?.message[0] || error.message || error.statusText);
    },
  });
