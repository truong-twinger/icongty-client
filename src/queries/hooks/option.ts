/* eslint-disable @typescript-eslint/no-explicit-any */
import { useQuery } from 'react-query';

import { EKeyOption, ELanguage, TResApi, TResDataListApi } from '@/configs/interface.config';
import { TQueryParamsGetOption } from '@/modules';

import { getListOption, getOptionByKey } from '../apis';
import { DETAIL_OPTION, LIST_OPTION } from '../keys';

export const useQueryAllOption = (params: TQueryParamsGetOption) =>
  useQuery<TResDataListApi<any>>([LIST_OPTION], () => getListOption(params), {
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
    retry: 2,
  });

export const useQueryOptionByKey = (key: EKeyOption[], lang: ELanguage) =>
  useQuery<TResApi<any>>([DETAIL_OPTION, key, lang], () => getOptionByKey(key, lang), {
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
    retry: 2,
  });
