/* eslint-disable no-unsafe-optional-chaining */
/* eslint-disable no-confusing-arrow */
import { useInfiniteQuery, useQuery } from 'react-query';

import { TQueryParamsGetData, TResDataListApi } from '@/configs/interface.config';
import { TCategory } from '@/modules';

import { getListCategory } from '../apis';
import { GET_ALL_CATEGORY, GET_ALL_CATEGORY_INFINITE } from '../keys';

export const queryAllCategory = (params: TQueryParamsGetData) =>
  useQuery<TResDataListApi<TCategory[]>>({
    queryKey: [GET_ALL_CATEGORY, JSON.stringify(params)],
    queryFn: () => getListCategory(params),
    refetchOnMount: false,
    keepPreviousData: true,
  });

export const useInfiniteAllCategory = (params: TQueryParamsGetData) =>
  useInfiniteQuery({
    queryKey: [GET_ALL_CATEGORY_INFINITE, JSON.stringify(params)],
    queryFn: ({ pageParam = 1 }) => getListCategory({ ...params, page: pageParam }),
    getNextPageParam: (lastPage) =>
      lastPage?.page < Math.ceil(lastPage?.total / lastPage?.limit) ? lastPage?.page + 1 : undefined,
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
  });
