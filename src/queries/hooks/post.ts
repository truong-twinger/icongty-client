import { useQuery } from 'react-query';

import { TPortfolio, TPost, TProduct, TQueryPost } from '@/modules';
import { TService } from '@/modules/services';
import { LANGUAGE_DEFAULT } from '@configs/const.config';
import { ELanguage, TResDataListApi } from '@configs/interface.config';

import {
  getListPost,
  getListPostFromDatabase,
  getPortfolioByCompanyID,
  getProductByCompanyID,
  getServiceByCompanyID,
} from '../apis/post';
import { LIST_POST, LIST_POSTPORTFOLIO, LIST_POSTPRODUCT, LIST_POSTSERVICE } from '../keys';

/**
 * @method useQueryListPost
 * @param {TQueryPost}params
 * @param {string}token
 * @returns
 */

export const queryAllPost = (params: TQueryPost, lang: ELanguage) =>
  useQuery<TResDataListApi<TPost[]>>({
    queryKey: [LIST_POST, JSON.stringify(params), lang],
    queryFn: () => getListPost(params, lang),
    refetchOnMount: false,
    keepPreviousData: true,
  });

export const queryAllPostFromDatabase = (params: TQueryPost, lang: ELanguage) =>
  useQuery<TResDataListApi<TPost[]>>({
    queryKey: [LIST_POST, JSON.stringify(params), lang],
    queryFn: () => getListPostFromDatabase(params, lang),
    refetchOnMount: false,
    keepPreviousData: true,
  });

/**
 * @method useQueryPortfolioByCompanyID
 * @param {string}slug
 * @returns
 */
export const useQueryPortfolioByCompanyID = (params: TQueryPost, lang = LANGUAGE_DEFAULT) =>
  useQuery<TResDataListApi<TPortfolio[]>>({
    queryKey: [LIST_POSTPORTFOLIO, JSON.stringify(params)],
    queryFn: () => getPortfolioByCompanyID(params, lang),
    enabled: !!params?.companyId,
    keepPreviousData: true,
    refetchOnMount: false,
  });
/**
 * @method useQueryProductByCompanyID
 * @param {string}slug
 * @returns
 */
export const useQueryProductByCompanyID = (params: TQueryPost, lang = LANGUAGE_DEFAULT) =>
  useQuery<TResDataListApi<TProduct[]>>({
    queryKey: [LIST_POSTPRODUCT, JSON.stringify(params)],
    queryFn: () => getProductByCompanyID(params, lang),
    enabled: !!params?.companyId,
    keepPreviousData: true,
    refetchOnMount: false,
  });
export const useQueryServiceByCompanyID = (params: TQueryPost, lang = LANGUAGE_DEFAULT) =>
  useQuery<TResDataListApi<TService[]>>({
    queryKey: [LIST_POSTSERVICE, JSON.stringify(params)],
    queryFn: () => getServiceByCompanyID(params, lang),
    enabled: !!params?.companyId,
    refetchOnMount: false,
    keepPreviousData: true,
  });

// /**
//  * @method useQueryServiceByCompanyID
//  * @param {string}slug
//  * @returns
//  */
// export const useQueryServiceByCompanyID = (id: string, lang = LANGUAGE_DEFAULT) =>
//   useQuery<TResApi<TPost>>([DETAIL_POST, slug, lang], () => getServiceCompanyID(id, lang), {
//     enabled: !!slug,
//   });
