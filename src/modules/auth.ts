import { TUser } from './user';

export type TSignIn = {
  username: string;
  password: string;
};

export type TSignature = {
  accessToken: string;
  expiredAt: number;
  refreshToken: string;
  expiredAtRefreshToken: number;
  email: string;
  phone: string;
};

export type TAuth = {
  user: TUser;
  auth: TSignature;
};

export type TChangePassword = {
  newPassword: string;
  confirmPassword: string;
  oldPassword: string;
};

export type TPassword = Omit<TChangePassword, 'oldPassword'>;

export type TRegister = {
  email: string;
  username: string;
  firstName: string;
  lastName: string;
} & TPassword;

export type TResetPassword = TPassword & {
  token: string;
};
