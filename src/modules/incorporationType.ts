import { ELanguage, TBaseData, TQueryParamsGetData } from '@/configs/interface.config';

export type TIncorporationType = TBaseData;

export type TQueryIncorporationType = TQueryParamsGetData<{ lang: ELanguage }>;
