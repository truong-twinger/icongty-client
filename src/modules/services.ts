import { ELanguage, EStatusDoc, TQueryParamsGetData } from '@/configs/interface.config';

import { TFile } from './media';
import { TUser } from './user';

export type TQueryService = TQueryParamsGetData<{
  'categoryIds[]'?: string[];
  'technologyIds[]'?: string[];
  lang?: ELanguage;
  companyId: string;
}>;

export type TService = {
  _id: string;
  author: TUser;
  editedBy: string;
  status: EStatusDoc;
  publishedLanguage: ELanguage[];
  name: string;
  nameSort: string;
  slug: string;
  logo: TFile;
  document: TFile;
  excerpt: string;
  description: string;
  viewer: number;
  createdAt: Date;
  updateAt: Date;
  __v: number;
};
