/* eslint-disable import/no-cycle */
import { ELanguage, EStatusDoc, TQueryParamsGetData } from '@/configs/interface.config';

import { TCategory } from './category';
import { TCompany } from './company';
import { TFile } from './media';
import { TTechnology } from './technology';
import { TUser } from './user';

export type TQueryProduct = TQueryParamsGetData<{
  'categoryIds[]'?: string[];
  'technologyIds[]'?: string[];
  lang?: ELanguage;
  companyId: string;
}>;

export type TProduct = {
  _id: string;
  author: TUser;
  editedBy: string;
  status: EStatusDoc;
  publishedLanguage: ELanguage[];
  name: string;
  slug: string;
  logo: TFile;
  coverPhoto: TFile;
  gallery: TFile[];
  videos: TFile[];
  documents: TFile[];
  excerpt: string;
  description: string;
  categories: TCategory[];
  technologies: TTechnology[];
  customers: TCompany[];
  company: TCompany;
  viewer: number;
  nameSort: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
};
