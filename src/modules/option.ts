import { TQueryParamsGetData } from '@/configs/interface.config';

export type TQueryParamsGetOption = TQueryParamsGetData;

export type TOption<T = unknown> = {
  contact?: {
    address?: string;
    phone?: string;
    email?: string;
  };
  _id: string;
  author: string;
  editedBy: string;
  key: string;
  value: T;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
};
