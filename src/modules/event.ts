import { ELanguage, EStatusDoc, TQueryParamsGetData } from '@/configs/interface.config';

import { TCompany } from './company';
import { TFile } from './media';
import { TUser } from './user';

export enum EventType {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
}

export type TQueryParamsGetEvent = TQueryParamsGetData<{
  status?: EStatusDoc;
  type: EventType;
  lang?: ELanguage;
  companyId: string;
}>;

export type TEvent = {
  _id: string;
  author: TUser;
  editedBy: string;
  status: EStatusDoc;
  publishedLanguage: ELanguage[];
  name: string;
  slug: string;
  startTime: Date;
  endTime: Date;
  scheduleAt: Date;
  link: string;
  type: EventType;
  coverPhoto: TFile;
  address: string;
  excerpt: string;
  description: string;
  attendees: TUser[];
  company: TCompany;
  viewer: number;
  nameSort: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
};
