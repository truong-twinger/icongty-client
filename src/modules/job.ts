import { ELanguage, EStatusDoc, TQueryParamsGetData } from '@/configs/interface.config';

import { TCompany } from './company';
import { TCity, TCountry } from './countryCity';
import { TUser } from './user';

export enum EJobTypeTime {
  FULL_TIME = 'FULL_TIME',
  PART_TIME = 'PART_TIME',
  ON_SITE = 'ON_SITE',
}

export type TQueryParamsGetJob = TQueryParamsGetData<{
  jobTypeTime?: EJobTypeTime;
  countryId?: string;
  cityId?: string;
  lang?: ELanguage;
  companyId: string;
}>;

export type TJob = {
  _id: string;
  author: TUser;
  editedBy: string;
  status: EStatusDoc;
  publishedLanguage: ELanguage[];
  name: string;
  slug: string;
  total: number;
  level: string;
  country: TCountry;
  city: TCity;
  excerpt: string;
  description: string;
  jobTypeTime: EJobTypeTime;
  scheduleAt: Date;
  expiredAt: Date;
  applicants: TUser;
  company: TCompany;
  viewer: number;
  nameSort: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
};
