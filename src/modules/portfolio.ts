/* eslint-disable import/no-cycle */
import { ELanguage, EStatusDoc, TQueryParamsGetData } from '@/configs/interface.config';

import { TCompany } from './company';
import { TFile } from './media';
import { TUser } from './user';

export type TQueryPortfolio = TQueryParamsGetData<{
  'categoryIds[]'?: string[];
  'technologyIds[]'?: string[];
  lang?: ELanguage;
  companyId: string;
}>;

export type TPortfolio = {
  _id: string;
  author: TUser;
  editedBy: string;
  status: EStatusDoc;
  publishedLanguage: ELanguage[];
  name: string;
  nameSort: string;
  slug: string;
  thumbnail: TFile;
  document: TFile;
  company: TCompany;
  excerpt: string;
  description: string;
  viewer: number;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
};
