export type TPostContact = {
  name: string;
  email: string;
  phone: string;
  message: string;
  companyName?: string;
};
