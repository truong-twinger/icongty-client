import { TTypeSizeScreen } from '@/configs/interface.config';

import { TFile } from './media';

type IDetailAds = {
  [key in TTypeSizeScreen]: TFile;
} & {
  show: boolean;
  key: string;
};

export type TAdvertising = {
  _id: string;
  author: string;
  editedBy: string;
  name: string;
  description: string;
  scheduleAt: Date;
  expiredAt: Date;
  isActive: boolean;
  value: IDetailAds;
  key: EADS_KEY;
};
export enum EADS_KEY {
  HOME_HEADER = 'HOME_HEADER',
  HOME_SIDEBAR = 'HOME_SIDEBAR',
  NEWS_LIST = 'NEWS_LIST',
  NEWS_DETAILS = 'NEWS_DETAILS',
  TAX_COMPANY_DETAILS = 'TAX_COMPANY_DETAILS',
}
