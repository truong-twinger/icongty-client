module.exports = {
    apps: [
        {
            name: 'icongty-client',
            script: 'yarn',
            args: 'start',
            instances: 1,
            autorestart: true,
            watch: false,
            max_memory_restart: '10G',
            exec_mode: 'cluster',
            env: {
                NODE_ENV: 'development',
                PORT: 3200,
            },
            env_production: {
                NODE_ENV: 'production',
                PORT: 3200,
            },
        },
    ],

    deploy: {
        development: {
            user: 'root',
            key: './gitlab.key',
            host: '103.68.251.151',
            ref: 'origin/develop',
            repo: 'git@gitlab.com:twinger/icongty/icongty-client.git',
            path: '/home/icongty-2/icongty-client',
            'post-setup': 'yarn; yarn build; cd ..; pm2 start ecosystem.config.js --env development',
            'post-deploy': 'yarn; yarn build; cd ..; pm2 reload ecosystem.config.js --env development',
            ssh_options: ['StrictHostKeyChecking=no', 'PasswordAuthentication=no'],
        },
    },
};
